<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>F:/CocosStudio/Cocos2d-x/cocos2d-x-3.10/projects/SpiceQuestBase/rawResources/InterfaceSprites.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename>../Resources/res/game/InterfaceSprites.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Resources/res/game/InterfaceSprites.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">res/game/interfaceSprites/Amazing.png</key>
            <key type="filename">res/game/interfaceSprites/AmazingCopy.png</key>
            <key type="filename">res/game/interfaceSprites/Back.png</key>
            <key type="filename">res/game/interfaceSprites/CloseClick.png</key>
            <key type="filename">res/game/interfaceSprites/CloseUp.png</key>
            <key type="filename">res/game/interfaceSprites/CompleteCopy.png</key>
            <key type="filename">res/game/interfaceSprites/Congratulations.png</key>
            <key type="filename">res/game/interfaceSprites/Continue.png</key>
            <key type="filename">res/game/interfaceSprites/EndSquare.png</key>
            <key type="filename">res/game/interfaceSprites/FbButtonClick.png</key>
            <key type="filename">res/game/interfaceSprites/FbButtonUp.png</key>
            <key type="filename">res/game/interfaceSprites/Finish.png</key>
            <key type="filename">res/game/interfaceSprites/Forward.png</key>
            <key type="filename">res/game/interfaceSprites/GemRed.png</key>
            <key type="filename">res/game/interfaceSprites/GemSlot.png</key>
            <key type="filename">res/game/interfaceSprites/GemTab.png</key>
            <key type="filename">res/game/interfaceSprites/GoldGem.png</key>
            <key type="filename">res/game/interfaceSprites/GoldPot.png</key>
            <key type="filename">res/game/interfaceSprites/GreatWork.png</key>
            <key type="filename">res/game/interfaceSprites/GreatWorkCopy.png</key>
            <key type="filename">res/game/interfaceSprites/Help.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo1.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo2.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo3.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo4.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo5.png</key>
            <key type="filename">res/game/interfaceSprites/HowTo6.png</key>
            <key type="filename">res/game/interfaceSprites/HowToPlay.png</key>
            <key type="filename">res/game/interfaceSprites/HudBarrel.png</key>
            <key type="filename">res/game/interfaceSprites/HudGem.png</key>
            <key type="filename">res/game/interfaceSprites/HuntFor.png</key>
            <key type="filename">res/game/interfaceSprites/LargeGem.png</key>
            <key type="filename">res/game/interfaceSprites/LargeSquareButtonClick.png</key>
            <key type="filename">res/game/interfaceSprites/LargeSquareButtonNo.png</key>
            <key type="filename">res/game/interfaceSprites/LargeSquareButtonUp.png</key>
            <key type="filename">res/game/interfaceSprites/LevelFrame.png</key>
            <key type="filename">res/game/interfaceSprites/LevelHud.png</key>
            <key type="filename">res/game/interfaceSprites/LevelMenu.png</key>
            <key type="filename">res/game/interfaceSprites/LittleTasters.png</key>
            <key type="filename">res/game/interfaceSprites/LockedFrame.png</key>
            <key type="filename">res/game/interfaceSprites/LongButtonClick.png</key>
            <key type="filename">res/game/interfaceSprites/LongButtonUp.png</key>
            <key type="filename">res/game/interfaceSprites/NoBarrels.png</key>
            <key type="filename">res/game/interfaceSprites/OhNo.png</key>
            <key type="filename">res/game/interfaceSprites/OhNoCopy.png</key>
            <key type="filename">res/game/interfaceSprites/PageMarker.png</key>
            <key type="filename">res/game/interfaceSprites/PaperPanel.png</key>
            <key type="filename">res/game/interfaceSprites/Pause.png</key>
            <key type="filename">res/game/interfaceSprites/Paused.png</key>
            <key type="filename">res/game/interfaceSprites/PerfectCopy.png</key>
            <key type="filename">res/game/interfaceSprites/PerfectFrame.png</key>
            <key type="filename">res/game/interfaceSprites/Play.png</key>
            <key type="filename">res/game/interfaceSprites/PlaySmall.png</key>
            <key type="filename">res/game/interfaceSprites/PotLocked.png</key>
            <key type="filename">res/game/interfaceSprites/PotSlot.png</key>
            <key type="filename">res/game/interfaceSprites/ProductInfo.png</key>
            <key type="filename">res/game/interfaceSprites/Restart.png</key>
            <key type="filename">res/game/interfaceSprites/SelectLevel.png</key>
            <key type="filename">res/game/interfaceSprites/SoundOff.png</key>
            <key type="filename">res/game/interfaceSprites/SoundOn.png</key>
            <key type="filename">res/game/interfaceSprites/Start.png</key>
            <key type="filename">res/game/interfaceSprites/StoreFinder.png</key>
            <key type="filename">res/game/interfaceSprites/Terms.png</key>
            <key type="filename">res/game/interfaceSprites/TwitButtonClick.png</key>
            <key type="filename">res/game/interfaceSprites/TwitButtonUp.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>res/game/interfaceSprites</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>

#ifndef __CBASE_SCENE_H__
#define __CBASE_SCENE_H__

#include "cocos2d.h"

#define CREATE_SCENE(__STAGE_SUB__) \
static cocos2d::Scene* createScene() \
{ \
    auto scene = cocos2d::Scene::create(); \
    std::shared_ptr<__STAGE_SUB__> stage = std::make_shared<__STAGE_SUB__>(); \
    stage->initialize(); \
    scene->addChild(stage->_rootNode); \
    return scene; \
}

class CBaseScene
{
	public:
		CBaseScene() : _rootNode(nullptr) {}
	protected:
		virtual void initialize() {};
		cocos2d::Node* _rootNode;
};

#endif // __CBASE_SCENE_H__

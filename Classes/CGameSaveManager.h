#ifndef __GAMESAVE_MANAGER_H__
#define __GAMESAVE_MANAGER_H__

#include "Constants.h"

struct CGameSave {
	unsigned long totalScore;
	unsigned int levelStates[10];
	unsigned int levelBestScores[10];
	CGameSave() 
		: totalScore(0)
		, levelStates{1, 0, 0, 0, 0, 0, 0, 0, 0, 0} 
		, levelBestScores{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	{
	}
};

class CGameSaveManager
	: public cocos2d::Node
{
	public:
		static CGameSaveManager* getInstance() {
			if (!instanceFlag) {
				single = new CGameSaveManager();
				if (single && single->init()) {
					instanceFlag = true;
					return single;
				}
				CC_SAFE_DELETE(single);
				return nullptr;
			}
			else {
				return single;
			}
		}

		virtual ~CGameSaveManager() {
			CCLOG("destroy CGameSaveManager singleton");
			instanceFlag = false;
			single = NULL;
		}

		void loadGame();
		void loadGameFromLocalStorage();
		bool isPB(unsigned long levelId, unsigned long score);
		unsigned long getPB(unsigned long levelId);
		bool unlockLevel(unsigned long levelId);
		bool saveLevelState(unsigned long levelId, unsigned long levelState);
		bool isPerfectComplete();
		void saveScore(unsigned long levelId, unsigned long score);
		void saveToStorage();

		std::vector<int> mLevelStates;
		std::vector<int> mLevelBestScores;
		int mTotalScore;

	private:
		CGameSaveManager();
		virtual bool init();

		static bool instanceFlag;
		static CGameSaveManager* single;
};

#endif // __GAMESAVE_MANAGER_H__
#include "CGameObject.h"

CGameObject::CGameObject() 
	: mWidth(32)
	, mHeight(32){
}

CGameObject::~CGameObject() {
}

CGameObject* CGameObject::create(const std::string& filename) {
	CGameObject* pRet = new CGameObject();
	if (pRet && pRet->init(filename)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CGameObject::init(const std::string& filename) {
	if (!cocos2d::Sprite::initWithSpriteFrameName(filename)) {
		return false;
	}

	return true;
}

void CGameObject::remove() {
	this->release();
	this->autorelease();
	this->removeFromParent();
}
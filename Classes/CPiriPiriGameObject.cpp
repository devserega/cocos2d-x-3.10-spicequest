#include "CPiriPiriGameObject.h"
#include "gridworld/CPiriPiriGridWorld.h"

CPiriPiriGameObject::CPiriPiriGameObject() 
	: CGameObject()
	//, mId(0)
	, stopsExplosion(true)
	, isDestroyable(true)
	, life(1)
	, hasGem(false)
	, showBlast(true)
	//this.onHit = new Signal,
	, isDead(false)
	, mSprite(nullptr)
	, mSpriteClone(nullptr)
	, aStarBlocker(false){
}

CPiriPiriGameObject::~CPiriPiriGameObject() {
	int t1 = 2121;
}

void CPiriPiriGameObject::onReact() {
	//original this.onHit.dispatch(this)
	if (mOnHitCallbackWithObject != nullptr)
		mOnHitCallbackWithObject(this);
}

void CPiriPiriGameObject::onDestroy(){

}

void CPiriPiriGameObject::setPosition(float x, float y) {
	Sprite::setPosition(x, y);
	/*
		this.position.x = x,
			this.position.y = y,
			this.view.position.x = x,
			this.view.position.y = y
	*/
}

void CPiriPiriGameObject::onHit(TOnHitCallbackWithObject onHitCallbackWithObject) {
	mOnHitCallbackWithObject = onHitCallbackWithObject;
}

void CPiriPiriGameObject::updateState() {
}
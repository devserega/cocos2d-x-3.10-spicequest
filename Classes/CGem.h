#ifndef __GEM_H__
#define __GEM_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

using namespace cocos2d;

class CGem
	: public Sprite
{
	public:
		virtual ~CGem();
		static CGem* create();
		virtual bool init();

		void reset();
		void goBoom();
		void updateTransform(float dt);

		void glisten();
		void glistenDown();
		void rotateRight();
		void rotateLeft();
		void scaleDown();
		void scaleUp();
		void hide(unsigned int x, unsigned int y);
		void update();

		double mGemSpeed;
		double mCount;

	protected:
		CGem();
		virtual void onEnter();
		virtual void onExit();

	private:
		Sprite* mGem;
		Sprite* mGemClone;
};

#endif // __GEM_H__
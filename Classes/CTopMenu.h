#ifndef __TOPMENU_H__
#define __TOPMENU_H__

#include "cocos2d.h"
#include "CPiriPiriButton.h"
#include "media/CSoundManager.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CTopMenu
	: public cocos2d::Layer
{
	public:
		static CTopMenu* create(CPiriPiriBlastApp* app);
		virtual ~CTopMenu();
		void gameMode();
		void normalMode();
		void onButtonPressed(CPiriPiriButton* bt);
		void onMuteToggle();
		void onShown();
		void resize();

	protected:
		CTopMenu();
		virtual bool init(CPiriPiriBlastApp* app);

		CPiriPiriBlastApp* mApp;
		CPiriPiriButton* mMuteButton;
		CPiriPiriButton* mPauseButton;
		CPiriPiriButton* mHelpButton;
		bool mIsMobile;
};

#endif // __TOPMENU_H__
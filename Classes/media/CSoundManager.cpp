#include "CSoundManager.h"  

Howl::Howl(std::vector<std::string> _urls, bool _autoplay, bool _loop, float _volume) {
	autoplay = _autoplay;
	loop = _loop;
	volume = _volume;
	urls = _urls;
}

Howl::Howl() {
	autoplay = false;
	loop = false;
	volume = 1.0f;
}

Howl::~Howl() {
	int t1 = 21;
}

void Howl::play() {
	for (std::vector<std::string>::iterator it = urls.begin(); it != urls.end(); it++) {
		std::string filename = *it;
		soundId = SimpleAudioEngine::getInstance()->playEffect(filename.c_str(), loop, 1.0f, 0.0f, volume);
		//SimpleAudioEngine::getInstance()->playBackgroundMusic(filename.c_str());
	}
}

void Howl::stop() {
	SimpleAudioEngine::getInstance()->stopEffect(soundId);
}

Group::Group() {
}

Group::Group(unsigned int index_, unsigned int type_, std::vector<std::string> ids_) {
	index = index_;
	type = type_;
	sounds = ids_;
}

bool CSoundManager::instanceFlag = false;
CSoundManager* CSoundManager::single = nullptr;

CSoundManager::CSoundManager()
	: mDisabled(false)
	, mReload(true)
	, mGlobalVolume(1)
	, mIsMuted(false)
	, mSounds{}
	, mGroups{} {
	int t1 = 0;
}

bool CSoundManager::init() {
	//mSounds.clear();
	//mGroups.clear();
	music = new CSoundManager();
	sfx = new CSoundManager();

	return true;
}
void CSoundManager::addSound(const std::string& url, const std::string& id, Options& options) {
	TSounds::iterator it = this->mSounds.find(id);
	if (!this->mDisabled && it == this->mSounds.end()) {
		//std::vector<std::string> soundsFiles = { String::createWithFormat("%s.mp3", url)->getCString(),
		//										 String::createWithFormat("%s.ogg", url)->getCString() };
		std::vector<std::string> soundsFiles = { url + ".wav"};//, url + ".ogg" 
		Howl* sound = new Howl(	soundsFiles, options.autoplay || false,  options.loop || false, options.volume || 1.0f);
		sound->realVolume = options.volume || 1;
		this->mSounds[id] = sound;
	}
}

void CSoundManager::addSound(const std::string& url, const std::string& id) {
	addSound(url, id, Options());
}

void CSoundManager::addGroup(std::vector<std::string> ids, const std::string& id) {
	std::map<std::string, Group>::iterator it = this->mGroups.find(id);
	if (!this->mDisabled && it == this->mGroups.end()) {
		auto group = Group(0, 0, ids);
		this->mGroups[id] = group;
	}
}

void CSoundManager::play(const std::string& id) {
	TSounds::iterator it = this->mSounds.find(id);
	if (!this->mDisabled && it != this->mSounds.end()) {
		it->second->play();
	}
}

void CSoundManager::playGroup(const std::string& id) {
	std::map<std::string, Group>::iterator it = this->mGroups.find(id);
	if (!this->mDisabled && it != this->mGroups.end()) {
		auto group = this->mGroups[id];
		int index = std::rand() % group.sounds.size(); // test
		this->mSounds[group.sounds[index]]->play();
	}
}

void CSoundManager::setVolume(const std::string& id, float volume) {
	TSounds::iterator it = this->mSounds.find(id);
	if (!this->mDisabled && it != this->mSounds.end()) {
		auto sound = it->second;
		sound->realVolume = volume;
		sound->volume = volume * this->mGlobalVolume;
	}
}

void CSoundManager::stop(const std::string& id) {
	TSounds::iterator it = this->mSounds.find(id);
	if (!this->mDisabled && it != this->mSounds.end())
		it->second->stop();
}

void CSoundManager::setPlaybackSpeed(const std::string& id/*, speed*/) {
	/*
		if (!this.disabled) {
			var sound = this.sounds[id];
			sound._playbackSpeed = speed;
			var hackId = "music" == id ? 0 : 1;
			sound._webAudio && Howler._howls[hackId]._audioNode[0] && (Howler._howls[hackId]._audioNode[0].bufferSource.playbackRate.value = speed)
		}*/
}

void CSoundManager::getPlaybackSpeed(const std::string& id) {
	/*
		if (!this.disabled) {
			var sound = this.sounds[id];
			return sound._playbackSpeed || 1
		}
		*/
}
		
void CSoundManager::setGlobalVolume(float volume) {
	this->mGlobalVolume = volume;
	for (TSounds::iterator it = this->mSounds.begin(); it != this->mSounds.end(); ++it) {
		Howl* sound = it->second;
		sound->volume = sound->realVolume * volume;
	}
}

void CSoundManager::mute() {
	// my [begin]
	SimpleAudioEngine::getInstance()->pauseAllEffects(); 
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	this->mDisabled = true;
	// my [end]
	this->mIsMuted = true;
	this->setGlobalVolume(0);
	for (TOnMuteToggleCallback::iterator it = mBindings.begin(); it != mBindings.end(); it++) {
		TOnCallback callback = *it;
		callback();
	}
	//if (mOnMuteToggleCallback != nullptr)
	//	mOnMuteToggleCallback(true);
	//ORIGINAL this.onMuteToggle.dispatch(!0);
}

void CSoundManager::unmute() {
	// my [begin]
	SimpleAudioEngine::getInstance()->resumeAllEffects();
	SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	this->mDisabled = false;
	// my [end]
	this->mIsMuted = false;
	this->setGlobalVolume(1);
	for (TOnMuteToggleCallback::iterator it = mBindings.begin(); it != mBindings.end(); it++) {
		TOnCallback callback = *it;
		callback();
	}
	//if (mOnMuteToggleCallback != nullptr)
	//	mOnMuteToggleCallback(false);
	//ORIGINAL this.onMuteToggle.dispatch(!1)
}

void CSoundManager::check() {
	/*
				this.lastSeen = Date.now();
				var loop = function() {
					lastSeen = Date.now(), setTimeout(loop, 50)
				};
				loop();
				var music = document.getElementById("music");
				music.addEventListener("timeupdate", function() {
					Date.now() - exports.lastSeen > 100 && this.pause()
				}, !1)
				*/
}

void CSoundManager::onMuteToggle(TOnCallback onMuteToggleCallback) {
	//mOnMuteToggleCallback = onMuteToggleCallback;
	mBindings.push_back(onMuteToggleCallback);
}
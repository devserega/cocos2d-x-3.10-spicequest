#ifndef __SOUND_MANAGER_H__
#define __SOUND_MANAGER_H__

#include "cocos2d.h"
#include "Constants.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

class Howl;
class Group;
typedef std::map<std::string, Howl*> TSounds;
typedef std::map<std::string, Group> TGroups;

class Howl {
	public:
		Howl(std::vector<std::string> _urls, bool _autoplay, bool _loop, float _volume);
		Howl();
		virtual ~Howl();
		void play();
		void stop();

		std::vector<std::string> urls;
		bool autoplay;
		bool loop;
		float volume;
		float realVolume;

		unsigned int soundId;
};

struct Options {
	bool autoplay;
	bool loop;
	float volume;

	Options() {
		autoplay = false;
		loop = false;
		volume = 1.0f;
	}

	Options(bool _autoplay, bool _loop, float _volume) {
		autoplay = _autoplay;
		loop = _loop;
		volume = _volume;
	}

	Options(bool _loop, float _volume) {
		autoplay = false;
		loop = _loop;
		volume = _volume;
	}

	Options(float _volume) {
		autoplay = false;
		loop = false;
		volume = _volume;
	}

	Options(bool _loop) {
		autoplay = false; 
		loop = _loop;
		volume = 1.0f;
	}
};

class Group {
	public:
		Group();
		Group(unsigned int index, unsigned int type, std::vector<std::string> ids);

		unsigned int index;
		unsigned int type;
		std::vector<std::string> sounds;
};

typedef std::vector<TOnCallback> TOnMuteToggleCallback;
class CSoundManager
	: public cocos2d::Ref
{
	public:
		static CSoundManager* getInstance() {
			if (!instanceFlag) {
				single = new CSoundManager();
				if (single && single->init()) {
					instanceFlag = true;
					return single;
				}
				CC_SAFE_DELETE(single);
				return nullptr;
			}
			else {
				return single;
			}
		}

		virtual ~CSoundManager() {
			CCLOG("destroy CSoundManager singleton");
			instanceFlag = false;
			single = NULL;
		}

		void addSound(const std::string& url, const std::string& id, Options& options);
		void addSound(const std::string& url, const std::string& id);
		void addGroup(std::vector<std::string> ids, const std::string& id);
		void play(const std::string& id);
		void playGroup(const std::string& id);
		void setVolume(const std::string& id, float volume);
		void stop(const std::string& id);
		void setPlaybackSpeed(const std::string& id/*, speed*/);
		void getPlaybackSpeed(const std::string& id);
		void setGlobalVolume(float volume);
		void mute();
		void unmute();
		void check();
		void onMuteToggle(TOnCallback onMuteToggleCallback);

		CSoundManager* music;
		CSoundManager* sfx;
		bool mIsMuted;

	protected:
		CSoundManager();
		virtual bool init();

		static bool instanceFlag;
		static CSoundManager* single;

		bool mDisabled;
		bool mReload;
		TSounds mSounds;
		TGroups mGroups;
		float mGlobalVolume;
		TOnCallbackWithBool mOnMuteToggleCallback;
		TOnMuteToggleCallback mBindings;
};

#endif // __SOUND_MANAGER_H__
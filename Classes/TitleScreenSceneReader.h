#ifndef TitleScreenReader_h
#define TitleScreenReader_h

#include <stdio.h>
#include "cocos2d.h"
#include "cocostudio/CocosStudioExport.h"
#include "cocostudio/WidgetReader/NodeReader/NodeReader.h"

class TitleScreenSceneReader
	: public cocostudio::NodeReader
{
	public:
		TitleScreenSceneReader() {};
		~TitleScreenSceneReader() {};
		static TitleScreenSceneReader* getInstance();
		static void purge();
		cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions);
};

#endif /* TitleScreenReader_h */

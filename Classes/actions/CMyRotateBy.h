#ifndef __CMYROTATEACTION_SCENE_H__
#define __CMYROTATEACTION_SCENE_H__

#include "cocos2d.h"

//#include <vector>

#//include "2d/CCAction.h"
//#include "2d/CCAnimation.h"
//#include "base/CCProtocols.h"
//#include "base/CCVector.h"

//NS_CC_BEGIN

using namespace cocos2d;

class CMyRotateBy
	: public cocos2d::RotateBy
{
	public:
		/**
		* Creates the action.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngle In degreesCW.
		* @return An autoreleased RotateBy object.
		*/
		static CMyRotateBy* create(float duration, float deltaAngle);
		/**
		* Creates the action with separate rotation angles.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngleZ_X In degreesCW.
		* @param deltaAngleZ_Y In degreesCW.
		* @return An autoreleased RotateBy object.
		* @warning The physics body contained in Node doesn't support rotate with different x and y angle.
		*/
		static CMyRotateBy* create(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
		/** Creates the action with 3D rotation angles.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngle3D A Vec3 angle.
		* @return An autoreleased RotateBy object.
		*/
		static CMyRotateBy* create(float duration, const Vec3& deltaAngle3D);

		//
		// Override
		//
		virtual CMyRotateBy* clone() const override;
		virtual CMyRotateBy* reverse(void) const override;
		virtual void startWithTarget(Node *target) override;
		/**
		* @param time In seconds.
		*/
		virtual void update(float time) override;

	CC_CONSTRUCTOR_ACCESS:
		CMyRotateBy();
		virtual ~CMyRotateBy() {}

		/** initializes the action */
		bool initWithDuration(float duration, float deltaAngle);
		/**
		* @warning The physics body contained in Node doesn't support rotate with different x and y angle.
		* @param deltaAngleZ_X in degreesCW
		* @param deltaAngleZ_Y in degreesCW
		*/
		bool initWithDuration(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
		bool initWithDuration(float duration, const Vec3& deltaAngle3D);

	protected:
		bool _is3D;
		Vec3 _deltaAngle;
		Vec3 _startAngle;
		int _count;

	private:
		CC_DISALLOW_COPY_AND_ASSIGN(CMyRotateBy);
};

#endif // __CMYROTATEACTION_SCENE_H__

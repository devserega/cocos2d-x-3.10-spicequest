#include "CFragmentAction.h"

//NS_CC_BEGIN

CFragmentAction* CFragmentAction::create(float duration, float deltaAngle){
	CFragmentAction *rotateBy = new (std::nothrow) CFragmentAction();
	rotateBy->initWithDuration(duration, deltaAngle);
	rotateBy->autorelease();

	return rotateBy;
}

CFragmentAction* CFragmentAction::create(float duration, float deltaAngleX, float deltaAngleY){
	CFragmentAction *rotateBy = new (std::nothrow) CFragmentAction();
	rotateBy->initWithDuration(duration, deltaAngleX, deltaAngleY);
	rotateBy->autorelease();

	return rotateBy;
}

CFragmentAction* CFragmentAction::create(float duration, const Vec3& deltaAngle3D){
	CFragmentAction *rotateBy = new (std::nothrow) CFragmentAction();
	rotateBy->initWithDuration(duration, deltaAngle3D);
	rotateBy->autorelease();

	return rotateBy;
}

CFragmentAction::CFragmentAction()
	: _is3D(false)
	, _count(0){
}

bool CFragmentAction::initWithDuration(float duration, float deltaAngle){
	if (ActionInterval::initWithDuration(duration)){
		_deltaAngle.x = _deltaAngle.y = deltaAngle;
		return true;
	}

	return false;
}

bool CFragmentAction::initWithDuration(float duration, float deltaAngleX, float deltaAngleY){
	if (ActionInterval::initWithDuration(duration)){
		_deltaAngle.x = deltaAngleX;
		_deltaAngle.y = deltaAngleY;
		return true;
	}

	return false;
}

bool CFragmentAction::initWithDuration(float duration, const Vec3& deltaAngle3D){
	if (ActionInterval::initWithDuration(duration)){
		_deltaAngle = deltaAngle3D;
		_is3D = true;
		return true;
	}

	return false;
}


CFragmentAction* CFragmentAction::clone() const{
	// no copy constructor
	auto a = new (std::nothrow) CFragmentAction();
	if (_is3D)
		a->initWithDuration(_duration, _deltaAngle);
	else
		a->initWithDuration(_duration, _deltaAngle.x, _deltaAngle.y);
	a->autorelease();
	return a;
}

void CFragmentAction::startWithTarget(Node *target){
	ActionInterval::startWithTarget(target);
	if (_is3D){
		_startAngle = target->getRotation3D();
	}
	else{
		_startAngle.x = target->getRotationSkewX();
		_startAngle.y = target->getRotationSkewY();
	}
}

void CFragmentAction::update(float time){
	// FIXME: shall I add % 360
	if (_target){
		if (_is3D){
			Vec3 v;
			v.x = _startAngle.x + _deltaAngle.x * time;
			v.y = _startAngle.y + _deltaAngle.y * time;
			v.z = _startAngle.z + _deltaAngle.z * time;
			_target->setRotation3D(v);
		}
		else{
			_count += 1;
			float rotation = 0.04f * sinf(0.2f * _count);// *this.ratio;

			float scale_x = 1 + 0.02f * sinf(0.2f * _count); // * this.ratio, 
			float scale_y = 1 + 0.02f * cosf(0.2f * _count); //* this.ratio

			#if CC_USE_PHYSICS
			if (_startAngle.x == _startAngle.y && _deltaAngle.x == _deltaAngle.y){
				//_target->setRotation(_startAngle.x + _deltaAngle.x * time);
				_target->setRotation(-50 * rotation);
				//CCLOG("%f %f %f %f", _startAngle.x, _deltaAngle.x, time, rotation);
				_target->setScaleX(scale_x);
				_target->setScaleY(scale_y);

			}
			else{
				_target->setRotationSkewX(_startAngle.x + _deltaAngle.x * time);
				_target->setRotationSkewY(_startAngle.y + _deltaAngle.y * time);
			}
			#else
			_target->setRotationSkewX(_startAngle.x + _deltaAngle.x * time);
			_target->setRotationSkewY(_startAngle.y + _deltaAngle.y * time);
			#endif // CC_USE_PHYSICS
		}
	}
}

CFragmentAction* CFragmentAction::reverse() const{
	if (_is3D){
		Vec3 v;
		v.x = -_deltaAngle.x;
		v.y = -_deltaAngle.y;
		v.z = -_deltaAngle.z;
		return CFragmentAction::create(_duration, v);
	}
	else{
		return CFragmentAction::create(_duration, -_deltaAngle.x, -_deltaAngle.y);
	}
}
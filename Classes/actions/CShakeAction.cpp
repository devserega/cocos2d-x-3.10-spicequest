// Code by Francois Guibert
// Contact: www.frozax.com - http://twitter.com/frozax - www.facebook.com/frozax
#include "cocos2d.h"
#include "CShakeAction.h"

// not really useful, but I like clean default constructors
CShakeAction::CShakeAction() 
	: _strength_x(2)
	, _strength_y(2)
	, _initial_x(0)
	, _initial_y(0)
	, mIntensity(0.0f)
	, mDirection(0)
{
}

CShakeAction* CShakeAction::actionWithDuration(float duration, float intensity, int direction)
{
	CShakeAction *p_action = new CShakeAction();
	p_action->initWithDuration(duration, intensity, direction);
	p_action->autorelease();

	return p_action;
}

bool CShakeAction::initWithDuration(float duration, float intensity, int direction)
{
	if (ActionInterval::initWithDuration(duration))
	{
		//_strength_x = strength_x;
		//_strength_y = strength_y;
		mIntensity = intensity;
		mDirection = direction;
		return true;
	}

	return false;
}

// Helper function. I included it here so that you can compile the whole file
// it returns a random value between min and max included
float fgRangeRand(float min, float max)
{
	float rnd = ((float)rand() / (float)RAND_MAX);
	return rnd*(max - min) + min;
}

void CShakeAction::update(float time)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	//this.offset.x = Math.round(Math.random() * this.intensity * this.gameWidth * 2 - this.intensity * this.gameWidth);
	//this.offset.y = Math.round(Math.random() * this.intensity * this.gameHeight * 2 - this.intensity * this.gameHeight);

	float randx = round((float)rand() * this->mIntensity * visibleSize.width * 2 - this->mIntensity * visibleSize.width);
	float randy = round((float)rand() * this->mIntensity * visibleSize.height * 2 - this->mIntensity * visibleSize.height);

	randx = fgRangeRand(-_strength_x, _strength_x);
	randy = fgRangeRand(-_strength_y, _strength_y);

	// move the target to a shaked position
	_target->setPosition(Vec2(randx, randy));
}

void CShakeAction::startWithTarget(Node *pTarget)
{
	CCActionInterval::startWithTarget(pTarget);

	// save the initial position
	_initial_x = pTarget->getPosition().x;
	_initial_y = pTarget->getPosition().y;
}

void CShakeAction::stop(void)
{
	// Action is done, reset clip position
	_target->setPosition(Vec2(_initial_x, _initial_y));

	CCActionInterval::stop();
}

void CShakeAction::addDuration(float duration)
{
	this->_duration += duration;
}
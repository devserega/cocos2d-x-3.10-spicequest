#ifndef __FRAGMENT_ACTION_H__
#define __FRAGMENT_ACTION_H__

#include "cocos2d.h"

using namespace cocos2d;

class CFragmentAction
	: public cocos2d::RotateBy
{
	public:
		/**
		* Creates the action.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngle In degreesCW.
		* @return An autoreleased RotateBy object.
		*/
		static CFragmentAction* create(float duration, float deltaAngle);
		/**
		* Creates the action with separate rotation angles.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngleZ_X In degreesCW.
		* @param deltaAngleZ_Y In degreesCW.
		* @return An autoreleased RotateBy object.
		* @warning The physics body contained in Node doesn't support rotate with different x and y angle.
		*/
		static CFragmentAction* create(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
		/** Creates the action with 3D rotation angles.
		*
		* @param duration Duration time, in seconds.
		* @param deltaAngle3D A Vec3 angle.
		* @return An autoreleased RotateBy object.
		*/
		static CFragmentAction* create(float duration, const Vec3& deltaAngle3D);

		//
		// Override
		//
		virtual CFragmentAction* clone() const override;
		virtual CFragmentAction* reverse(void) const override;
		virtual void startWithTarget(Node *target) override;
		/**
		* @param time In seconds.
		*/
		virtual void update(float time) override;

	CC_CONSTRUCTOR_ACCESS:
		CFragmentAction();
		virtual ~CFragmentAction() {}

		/** initializes the action */
		bool initWithDuration(float duration, float deltaAngle);
		/**
		* @warning The physics body contained in Node doesn't support rotate with different x and y angle.
		* @param deltaAngleZ_X in degreesCW
		* @param deltaAngleZ_Y in degreesCW
		*/
		bool initWithDuration(float duration, float deltaAngleZ_X, float deltaAngleZ_Y);
		bool initWithDuration(float duration, const Vec3& deltaAngle3D);

	protected:
		bool _is3D;
		Vec3 _deltaAngle;
		Vec3 _startAngle;
		int _count;

	private:
		CC_DISALLOW_COPY_AND_ASSIGN(CFragmentAction);
};

#endif // __FRAGMENT_ACTION_H__

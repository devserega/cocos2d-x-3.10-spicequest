#ifndef __SHAKE_ACTION_H__
#define __SHAKE_ACTION_H__

#include "cocos2d.h"
//#include "CCActionInterval.h"
using namespace cocos2d;

class CShakeAction
	: public ActionInterval
{
		// Code by Francois Guibert
		// Contact: www.frozax.com - http://twitter.com/frozax - www.facebook.com/frozax
	public:
		CShakeAction();


		// Create the action with a time and a strength (same in x and y)
		static CShakeAction* actionWithDuration(float duration, float intensity, int direction);
		bool initWithDuration(float duration, float intensity, int direction);

		virtual void startWithTarget(Node *pTarget);
		virtual void update(float time);
		virtual void stop(void);
		virtual void addDuration(float duration);

	protected:
		// Initial position of the shaked node
		float _initial_x, _initial_y;
		// Strength of the action
		float _strength_x, _strength_y;

		float mIntensity;
		int mDirection;
};

#endif //__SHAKE_ACTION_H__
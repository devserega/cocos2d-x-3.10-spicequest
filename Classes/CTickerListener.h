#ifndef __TICKER_LISTENER_H__
#define __TICKER_LISTENER_H__

class CTickerListener
{
	public:
		virtual void update(float dt) = 0;
};

#endif // __TICKER_LISTENER_H__


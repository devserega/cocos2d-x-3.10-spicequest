#include "CPointAndClickController.h"
#include "CPiriPiriBlastGame.h"

CPointAndClickController::CPointAndClickController()
	: interactive(true)
	, buttonMode(true) {
}

CPointAndClickController* CPointAndClickController::create(CPiriPiriBlastGame* game) {
	CPointAndClickController* pRet = new CPointAndClickController();
	if (pRet && pRet->init(game)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

CPointAndClickController::~CPointAndClickController() {

}

bool CPointAndClickController::init(CPiriPiriBlastGame* game) {
	if (!cocos2d::Node::init()) {
		return false;
	}

	mGame = game;

	return true;
}

void CPointAndClickController::disable() {
	CCLOG("controller disable");
	interactive = false;
}

void CPointAndClickController::enable() {
	CCLOG("controller enable");
	interactive = true;
}

bool CPointAndClickController::isInteractive() {
	return interactive;
}
#include "CSignal.h"

CSignal::CSignal()
	: mVERSION("1.0.0")
	, mMemorize(false)
	, mShouldPropagate(true)
	, mActive(true)
{
}

CSignal::~CSignal() {
}

bool CSignal::init() {
	//if (!cocos2d::Ref::init()) {
	//	return false;
	//}

	//mActive = false;
	//mDeltaTime = 1;
	//mTimeElapsed = 0;
	//mLastTime = 0;
	//mSpeed = 1;

	return true;
}

CSignal* CSignal::create() {
	CSignal* pRet = new CSignal();
	if (pRet && pRet->init()) {
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}
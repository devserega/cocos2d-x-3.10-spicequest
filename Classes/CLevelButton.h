#ifndef __LEVEL_BUTTON_H__
#define __LEVEL_BUTTON_H__

#include "Constants.h"
#include "CLevels.h"

//#define CLevelButton_DEBUG

struct TEvent {
	unsigned int mId;
};

typedef std::function<void(TEvent)> TOnPressCallbackWithEventParam;

class CLevelButton
	: public cocos2d::Node
{
	public:
		virtual bool init(CLevel* parameters);
		static CLevelButton* create(CLevel* parameters);
		void onPress(TOnPressCallbackWithEventParam onPressCallback);
		virtual void onMouseOver();
		virtual void onMouseOut();
		virtual void onMouseDown();
		void setState(int state, unsigned int count);
		void setGemCount(unsigned int count);

		unsigned int mId;

	protected:
		bool myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point);
		CLevelButton();
		void addListener();
		void removeListener();
		void onEnter();
		void onExit();

		int mState;
		Sprite* mFrame;
		Sprite* mFrameGold;
		Sprite* mFrameLocked;
		Sprite* mPot;
		Sprite* mPotLocked;
		Sprite* mReadoutFrame;
		Sprite* mReadoutFrameGold;
		Label* mName;
		std::vector<Sprite*> mGems;

		EventListenerTouchOneByOne* mListener;
		TOnPressCallbackWithEventParam mOnPressCallbackWithEventParam;

		unsigned int mWidth;
		unsigned int mHeight;
};

#endif // __LEVEL_BUTTON_H__

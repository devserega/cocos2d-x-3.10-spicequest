#ifndef __TICKER_H__
#define __TICKER_H__

#include "cocos2d.h"
#include "Constants.h"

using namespace cocos2d;

class CTickerListener;
class CTicker
	: public cocos2d::Node
{
	public:
		static CTicker* getInstance() {
			if (!instanceFlag) {
				single = new CTicker();
				if (single && single->init()) {
					instanceFlag = true;
					return single;
				}
				CC_SAFE_DELETE(single);
				return nullptr;
			}
			else {
				return single;
			}
		}

		virtual ~CTicker() {
			CCLOG("destroy CTicker singleton");
			//this->unschedule(schedule_selector(CTicker::update));
			this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CTicker::update));
			instanceFlag = false;
			single = NULL;
		}

		void start();	
		void stop();	
		void add(CTickerListener* listener);
		//void add(SEL_SCHEDULE selector);
		//void add(std::function<void(float val)> listener/*, scope*/);
		//void remove(SEL_SCHEDULE listener/*, scope*/);
		void remove(CTickerListener* listener);
		void remove(std::function<void(float val)> listener/*, scope*/);
		double getDeltaTime();
		void setSpeed(int speed);

	private:
		CTicker();
		virtual bool init();
		void update(float dt);
		long long getCurrentTime();

		static bool instanceFlag;
		static CTicker* single;

		//this.onUpdate = new Signal,
		//this.updateBind = this.update.bind(this),
		bool mActive;
		double mDeltaTime;
		long long mTimeElapsed;
		long long mLastTime;
		int mSpeed;
		//std::vector<SEL_SCHEDULE> mListeners;
		//std::vector<std::function<void(float val)>> mListeners;
		std::vector<CTickerListener*> mListeners;
};

#endif // __TICKER_H__

#include "CLevelButton.h"
#include "media/CSoundManager.h"

CLevelButton::CLevelButton()
	: mOnPressCallbackWithEventParam(nullptr)
	, mListener(nullptr) {
}

bool CLevelButton::init(CLevel* parameters) {
	if (!cocos2d::Node::init()) {
		return false;
	}

	float anchorY = .5; // .5f

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	this->mState = LEVEL_STATES::LOCKED;
	//this.view = new PIXI.DisplayObjectContainer,
	//this.view.hitArea = new PIXI.Rectangle(0, 0, 170, 200),
	this->mFrame = Sprite::createWithSpriteFrameName("LevelFrame.png");
	this->mFrame->setScale(1, 1);
	this->mFrame->setAnchorPoint(Vec2(.5f, anchorY)); // .5, 0
	this->addChild(this->mFrame, 0);

	this->mFrameGold = Sprite::createWithSpriteFrameName("PerfectFrame.png");
	this->mFrameGold->setScale(1, 1);
	this->mFrameGold->setAnchorPoint(Vec2(.5f, anchorY));// .5, 0
	this->addChild(this->mFrameGold, 1);

	this->mFrameLocked = Sprite::createWithSpriteFrameName("LockedFrame.png");
	this->mFrameLocked->setScale(1, 1);
	this->mFrameLocked->setAnchorPoint(Vec2(.5f, anchorY)); // .5, 0
	this->addChild(this->mFrameLocked, 2);

	this->mPot = Sprite::createWithSpriteFrameName(parameters->sprite);
	this->mPot->setScale(1, 1);
	this->mPot->setAnchorPoint(Vec2(.5, anchorY)); // .5, 0
	this->addChild(this->mPot, 3);

	this->mPotLocked = Sprite::createWithSpriteFrameName("PotLocked.png");
	this->mPotLocked->setScale(1, 1);
	this->mPotLocked->setAnchorPoint(Vec2(.5, anchorY)); // .5, 0
	this->mPotLocked->setOpacity(0);
	this->addChild(this->mPotLocked, 4);

	this->mName = Label::createWithBMFont("game/font_number-export.fnt", parameters->name);//
	this->mName->setPositionY(-10);
	this->addChild(this->mName, 5);

	this->mReadoutFrame = Sprite::createWithSpriteFrameName("GemTab.png");
	this->mReadoutFrame->setScale(1, 1);
	this->mReadoutFrame->setAnchorPoint(Vec2(.5, anchorY)); // .5, 0
	this->mReadoutFrame->setPositionY(-100);
	this->addChild(this->mReadoutFrame, 6);

	this->mReadoutFrameGold = Sprite::createWithSpriteFrameName("GemTab.png");
	this->mReadoutFrameGold->setScale(1, 1);
	this->mReadoutFrameGold->setAnchorPoint(Vec2(.5, anchorY)); // .5, 0
	this->mReadoutFrameGold->setPositionY(-100);
	this->addChild(this->mReadoutFrameGold, 7);

	//this.view.mouseover = this.view.touchstart = this.onMouseOver.bind(this),
	//this.view.mouseout = this.view.touchend = this.onMouseOut.bind(this),
	//this.onDown.add(this.onMouseDown, this),

	this->mGems.clear();

	for (unsigned int i = 0; 3 > i; i++) {
		auto gem = Sprite::createWithSpriteFrameName("GoldGem.png");
		gem->setAnchorPoint(Vec2(.5f, anchorY)); // .5, 0
		gem->setScale(.45f);
		gem->setPositionX(-33.0f+(34.0f * i)); // 66 + 34 * i
		gem->setPositionY(-105.0f);
		gem->setOpacity(0);
		this->addChild(gem, 8);
		this->mGems.push_back(gem);
	}	

	mWidth = this->mFrame->getContentSize().width;
	mHeight = this->mFrame->getContentSize().height;

	#ifdef CLevelButton_DEBUG
	auto rectWithBorder = DrawNode::create();
	Vec2 vertices[] = {
		Vec2(-mWidth / 2,-mHeight / 2),
		Vec2(-mWidth / 2, mHeight / 2),
		Vec2(mWidth / 2, mHeight / 2),
		Vec2(mWidth / 2,-mHeight / 2)
	};
	rectWithBorder->drawPolygon(vertices, 4, Color4F(1.0f, 0.3f, 0.3f, 1), 1, Color4F(0.2f, 0.2f, 0.2f, 1));
	addChild(rectWithBorder);
	#endif

	return true;
}

CLevelButton* CLevelButton::create(CLevel* parameters) {
	CLevelButton* pRet = new(std::nothrow) CLevelButton();
	if (pRet && pRet->init(parameters)) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

void CLevelButton::onPress(TOnPressCallbackWithEventParam onPressCallbackWithEventParam) {
	mOnPressCallbackWithEventParam = onPressCallbackWithEventParam;
}

bool CLevelButton::myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point) {
	bool bRet = false;

	if (point.x >= rect.origin.x && point.x <= rect.size.width &&
		point.y >= rect.origin.y && point.y <= rect.size.height) {
		bRet = true;
	}

	return bRet;
}

void CLevelButton::setState(int state, unsigned int count) {
	this->setGemCount(count);
	this->mPot->setOpacity(0);
	this->mPotLocked->setOpacity(0);
	this->mReadoutFrame->setOpacity(0);
	this->mName->setOpacity(0);
	this->mFrameLocked->setOpacity(0);
	this->mFrame->setOpacity(0);
	this->mFrameGold->setOpacity(0);
	switch (this->mState = state) {
		case LEVEL_STATES::LOCKED:
			//this.interactive = !1;
			this->mPotLocked->setOpacity(255);
			this->mFrameLocked->setOpacity(255);
			break;
		case LEVEL_STATES::UNLOCKED:
			this->mName->setOpacity(255);
			this->mPot->setOpacity(255);
			this->mFrame->setOpacity(255);
			break;
		case LEVEL_STATES::COMPLETE:
			this->mName->setOpacity(255);
			this->mPot->setOpacity(255);
			this->mFrame->setOpacity(255);
			break;
		case LEVEL_STATES::COMPLETE_WITH_GEMS:
			this->mName->setOpacity(255);
			this->mPot->setOpacity(255);
			this->mFrameGold->setOpacity(255); 
	}
}

void CLevelButton::setGemCount(unsigned int count) {
	//for (var i = 0; 3 > i; i++) this.gems[i].alpha;
	for (unsigned int i = 0; count > i; i++) {
		std::string texture = (3 == count) ? "GoldGem.png" : "LargeGem.png";
		Sprite* gem = this->mGems[i];
		gem->setSpriteFrame(texture);
		gem->setOpacity(255);
	}
}

void CLevelButton::onMouseOver() {
	if (this->mState > 0) {
		//CCLOG("onMouseOver");
		//runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1.1f, 1.1f), 0.3f));
		double rot = .05f * (((double) std::rand() / RAND_MAX) - .5f) * (180 / PI);
		runAction(EaseElasticOut::create(RotateTo::create(0.5f, rot), 0.3f));
	}

	/* ORIGINAL
	this.state > 0 && (
		TweenLite.to(this.view.scale, .5, {
			x: 1.1,
			y : 1.1,
			ease : Elastic.easeOut
		}), 
		TweenLite.to(this.view, .75, {
			rotation: .05 * (Math.random() - .5),
			ease : Elastic.easeOut
		})
	)
	*/
}

void CLevelButton::onMouseOut() {
	if (this->mState > 0) {
		CCLOG("onMouseOut");
		runAction(EaseQuadraticActionInOut::create(ScaleTo::create(0.25, 1.0f, 1.0f)));
		runAction(EaseElasticOut::create(RotateTo::create(0.75f, 0.0f), 0.3f));
	}

	/* ORIGINAL
	this.state > 0 && (
		TweenLite.to(this.view.scale, .25, {
			x: 1,
			y : 1,
			ease : Quart.easeInOut
		}), 
		TweenLite.to(this.view, .75, {
			rotation: 0,
			ease : Elastic.easeOut
		})
	)*/
}

void CLevelButton::onMouseDown() {
	if (0 != this->mState) {
		CCLOG("onMouseDown");
		CSoundManager::getInstance()->sfx->play("buttonPress"); 	//SoundManager.sfx.play("buttonPress");
		runAction(EaseQuadraticActionOut::create(ScaleTo::create(.1f, 1.2f, 1.2f)));
	}
	
	/* ORIGINAL
	0 != this.state && (
	SoundManager.sfx.play("buttonPress"), 
	TweenLite.to(this.view.scale, .1, {
	x: 1.2,
	   y : 1.2,
		ease : Quad.easeOut
	}))*/
}

void CLevelButton::addListener() {
	if (!mListener) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		mListener = cocos2d::EventListenerTouchOneByOne::create();
		mListener->setSwallowTouches(true);

		mListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			if (!this->isVisible()) 
				return false;

			auto target = static_cast<Sprite*>(e->getCurrentTarget());
			cocos2d::Vec2 p = target->convertToNodeSpace(touch->getLocation());
			cocos2d::Rect rect = Rect(0, 0, mWidth, mHeight);
			if (myContainsPoint(rect, p)) {
				this->onMouseDown();
				this->onMouseOver();
				return true; // to indicate that we have consumed it.
			}

			return false; // we did not consume this event, pass thru.
		};

		mListener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			this->onMouseOut();
			if (mOnPressCallbackWithEventParam != nullptr) {
				TEvent _event;
				_event.mId = mId;
				mOnPressCallbackWithEventParam(_event);
			}
		};
		dispatcher->addEventListenerWithSceneGraphPriority(mListener, this->mFrame);
	}
}

void CLevelButton::removeListener() {
	if (mListener != nullptr) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->removeEventListener(mListener);
		mListener = nullptr;
	}
}

void CLevelButton::onEnter() {
	cocos2d::Node::onEnter();
	addListener();
}

void CLevelButton::onExit() {
	cocos2d::Node::onExit();
	removeListener();
}
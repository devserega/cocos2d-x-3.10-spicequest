/*
define("com/PiriPiriBlast/buttons/TwoFrameButton",
	["require", "exports", "module", "PIXI", "../../fido/buttons/Button"],
	function(require, exports, module) {
	var PIXI = require("PIXI"),
		Button = require("../../fido/buttons/Button"),
		TwoFrameButton = function(upFrame, downFrame) {
		this.upFrame = upFrame,
			this.downFrame = downFrame;
		var view = new PIXI.Sprite.fromFrame(this.upFrame);
		view.anchor.set(.5),
			this.tScale = 1.1,
			this.rScale = 1,
			Button.call(this, view),
			this.view.touchstart = this.onMouseDown.bind(this),
			this.view.touchend = this.view.touchendoutside = this.onMouseUp.bind(this),
			this.onDown.add(this.onMouseDown, this),
			this.onUp.add(this.onMouseUp, this)
	};
	TwoFrameButton.prototype = Object.create(Button.prototype),
		TwoFrameButton.prototype.onMouseDown = function() {
		this.down != = !0 && (
			this.down = !0,
			this.view.setTexture(PIXI.Texture.fromFrame(this.downFrame)),
			TweenLite.to(this.view.scale, .2, {
			x: this.tScale,
			   y : this.tScale,
				ease : Elastic.easeOut
			})
			)
	},
		TwoFrameButton.prototype.onMouseUp = function() {
			this.down = !1,
				this.view.setTexture(PIXI.Texture.fromFrame(this.upFrame)),
				TweenLite.to(this.view.scale, .2, {
				x: this.rScale,
				   y : this.rScale,
					ease : Elastic.easeOut
				})
		},
			module.exports = TwoFrameButton
}),
*/
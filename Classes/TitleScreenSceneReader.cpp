#include "TitleScreenSceneReader.h"
#include "TitleScreenScene.h"

USING_NS_CC;

static TitleScreenSceneReader* _instanceTitleScreenSceneReader = nullptr;

TitleScreenSceneReader* TitleScreenSceneReader::getInstance(){
	if (!_instanceTitleScreenSceneReader){
		_instanceTitleScreenSceneReader = new TitleScreenSceneReader();
	}
	return _instanceTitleScreenSceneReader;
}

void TitleScreenSceneReader::purge(){
	CC_SAFE_DELETE(_instanceTitleScreenSceneReader);
}

Node* TitleScreenSceneReader::createNodeWithFlatBuffers(const flatbuffers::Table *nodeOptions){
	TitleScreenScene* node = TitleScreenScene::create();
	TitleScreenScene::setStaticInstance(node);
	setPropsWithFlatBuffers(node, nodeOptions);
	return node;
}
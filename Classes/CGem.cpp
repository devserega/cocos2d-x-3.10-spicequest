#include "CGem.h"
#include "CTicker.h"

CGem::CGem(){
}

CGem::~CGem(){
}

CGem* CGem::create() {
	CGem* pRet = new CGem();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CGem::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	this->setCascadeOpacityEnabled(true);
	mGem = Sprite::createWithSpriteFrameName("GemRed.png");
	mGem->setAnchorPoint(Vec2(.5, .45)); // .55
	addChild(mGem, 1);

	mGemClone = Sprite::createWithSpriteFrameName("GemRed.png");
	mGemClone->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	mGemClone->setAnchorPoint(Vec2(.5, .45)); // .55
	addChild(mGemClone, 2);

	mGemSpeed = 10;
	mCount = 0.0f;

	return true;
}

void CGem::reset() {
	this->mGem->setScale(1.0f);
	this->mGem->setRotation(0);
	this->mGem->setOpacity(255);
	this->mGemClone->setRotation(0);
	this->mGemClone->setOpacity(0);
	this->mGemClone->setScale(1.0f);

	double rand = ((double) std::rand() / (double) (RAND_MAX));
	this->mCount = 100.0f * rand;
}

void CGem::goBoom() {
	double rand = (((double)std::rand() / (double)RAND_MAX));
	this->mGemSpeed = -(6 + 2 * rand); 
	rand = (((double)std::rand() / (double)RAND_MAX));
	rand > .5 ? this->rotateRight() : this->rotateLeft();
	rand = (((double)std::rand() / (double)RAND_MAX));
	rand > .5 ? this->scaleUp() : this->scaleDown();
}

void CGem::updateTransform(float dt) {
	this->mCount += CTicker::getInstance()->getDeltaTime(); 
	double rand = (((double) std::rand() / (double) RAND_MAX));
	this->mCount > 120 && (
		this->glisten(), 
		this->mCount = 0, 
		this->mCount += 60 * rand
	);
	this->mGem->setPositionY(this->mGem->getPositionY() - this->mGemSpeed * CTicker::getInstance()->getDeltaTime()); // +
	this->mGem->getPositionY() <= 0 && ( // >= 0 
		this->mGem->setPositionY(0), 
		this->mGemSpeed *= -.5
	); 
	this->mGemClone->setPositionY(this->mGem->getPositionY());
	this->mGemSpeed += .5;
	//CCLOG("mGemSpeed=%f %f", this->mGemSpeed, this->mGem->getPositionY());

	/* original
		this.count > 120 && (this.glisten(), this.count = 0, this.count += 60 * Math.random()),
		this.gem.y += this.gemSpeed * Ticker.instance.deltaTime,
		this.gem.y >= 0 && (this.gem.y = 0, this.gemSpeed *= -.5),
		this.gemClone.y = this.gem.y,
		this.gemSpeed += .5,
		PIXI.DisplayObjectContainer.prototype.updateTransform.call(this)
	*/
}

void CGem::glisten() {
	mGemClone->runAction(Sequence::create(	FadeTo::create(0.3f, 255),
											CallFunc::create([this]() {
												this->glistenDown();
											}),
											nullptr));
	/* original
		TweenLite.to(this.gemClone, .3, {
		alpha: 1,
			   onComplete : this.glistenDown.bind(this)
		})
		*/
}

void CGem::glistenDown() {
	mGemClone->runAction(FadeTo::create(0.3f, 0));
	/* original
		TweenLite.to(this.gemClone, .3, {
		alpha: 0
		})
		*/
}

void CGem::rotateRight() {}

void CGem::rotateLeft() {}

void CGem::scaleDown() {}

void CGem::scaleUp() {}

void CGem::hide(unsigned int x, unsigned int y) {
	return;
}

void CGem::update() {}

void CGem::onEnter() {
	Sprite::onEnter();
	this->schedule(schedule_selector(CGem::updateTransform), 0.025f); //0.015f
}

void CGem::onExit() {
	Sprite::onExit();
	this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CGem::updateTransform));
}
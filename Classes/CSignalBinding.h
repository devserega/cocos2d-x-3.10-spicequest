#ifndef __CSIGNAL_BINDING_H__
#define __CSIGNAL_BINDING_H__

#include "cocos2d.h"

using namespace cocos2d;

class CSignalBinding
	: public cocos2d::Ref
{
	public:
		static CSignalBinding* create();
		virtual bool init();
		virtual ~CSignalBinding();

	protected:
		CSignalBinding();

		//std::vector<> mBindings;
		//this._bindings = [],
		//	this._prevParams = null;
		//var self = this;
		//this.dispatch = function() {
		//	Signal.prototype.dispatch.apply(self, arguments)
		//}
};

#endif // __CSIGNAL_BINDING_H__
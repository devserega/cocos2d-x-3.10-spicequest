#include "COverlayManager.h"
#include "CPiriPiriBlastApp.h"

COverlayManager::COverlayManager()
	: mBlack(nullptr)
	, mListener(nullptr){
}

COverlayManager::~COverlayManager() {
}

COverlayManager* COverlayManager::create(CPiriPiriBlastApp* app) {
	COverlayManager* pRet = new COverlayManager();
	if (pRet && pRet->init(app)) {
		pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool COverlayManager::init(CPiriPiriBlastApp* app) {
	if (!CScreenManager::init()) {//100,100
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();

	this->setCascadeOpacityEnabled(true);
	this->mApp = app;
	this->mTutorialScreen = CTutorialScreen::create(app);
	this->mPauseScreen = CPauseScreen::create(app);
	this->mGameoverScreen = CGameoverScreen::create(app);
	//this->mCompleteScreen = CCompleteScreen::create(app);
	//this->mGameoverScreen-hasClose = false;
	this->addScreen(this->mGameoverScreen, "gameover");
	this->addScreen(this->mTutorialScreen, "help");
	this->addScreen(this->mPauseScreen, "pause");
	//this.addScreen(this.completeScreen, "complete");
	this->addScreen(CScreen::create(app), "empty");
	//this.onShow = new Signal;
	//this.onHide = new Signal;

	this->mBlack = LayerColor::create(Color4B(0,0,0,255));
	this->mBlack->setPosition(Vec2(-visibleSize.width / 2, -visibleSize.height / 2)); // my
	this->mBlack->setOpacity(178);
	/*
	this.black = (new PIXI.Graphics).beginFill(1706247).drawRect(0, 0, 100, 100);
	this.black.alpha = .7;
	this.black.interactive = !0;
	this.black.hitArea = new PIXI.Rectangle(0, 0, 1e4, 1e4);
	this.black.mousedown = this.black.touchstart = this.onMouseDown.bind(this);
	this.black.mouseup = this.black.touchend = this.onMouseUp.bind(this);
	this.black.click = this.black.tap = this.onPress.bind(this);*/
	this->setVisible(false); // this.view.visible = !1;
	this->addChild(this->mBlack, 0);
	/*
	this.view.addChild(this.container);
	this.app.screenManager.container.filterArea = new PIXI.Rectangle(0, 0, 1e4, 1e4);
	*/

	return true;
}

void COverlayManager::onMouseUp() {}
void COverlayManager::onMouseDown() {}
void COverlayManager::onPress() {}

void COverlayManager::goBack() {
	//this.history.pop();
	//var prev = this.history.pop();
	//prev ? this.gotoScreen(prev) : this.hide()
}

void COverlayManager::show(const std::string& screenId) {
	this->history.clear();
	this->gotoScreenByID(screenId/*, true*/); // this.gotoScreenByID(screenId, !0)
	if(mOnShowCallback!=nullptr) mOnShowCallback(); // this.onShow.dispatch()
	this->setVisible(true);
	this->setOpacity(0);
	this->runAction(FadeIn::create(.3));
	/* ORIGINAL
	TweenLite.to(this.view, .3, {
	alpha: 1
	})*/

	this->addEventListener();
}

void COverlayManager::gotoScreen(CScreen* screen) {
	CScreenManager::gotoScreen(screen); // ORIGINAL ScreenManager.prototype.gotoScreen.call(this, screen)
}

void COverlayManager::hide() {
	this->history.clear(); // ORIGINAL this.history = [],
	this->gotoScreenByID("empty");
	this->setVisible(false);
	if (mOnHideCallback != nullptr) mOnHideCallback(); // ORIGINAL this.onHide.dispatch()
	removeEventListener(); // my
}

void COverlayManager::resize(unsigned int w, unsigned int h) {
	//this.black.scale.set(w / 100, h / 100),
	//ScreenManager.prototype.resize.call(this, w, h)
}

void COverlayManager::onFadeout() {
	if (this->mCurrentScreen) {
		this->mCurrentScreen->onHidden();
		this->mCurrentScreen->removeFromParent(); // this.container.removeChild(this.currentScreen)
	}
	this->mCurrentScreen = this->mNextScreen; 
	this->mCurrentScreen->setOpacity(0);
	this->mCurrentScreen->onShow();
	this->mCurrentScreen->resize(this->w, this->h);

	this->addChild(this->mCurrentScreen);
	this->mCurrentScreen->runAction(Sequence::create(	FadeIn::create(.4f),
														CallFunc::create([this]() {
															//this->onFadein();
														}),
														nullptr));
	/* ORIGINAL
						this.currentScreen && (
							this.currentScreen.onHidden && this.currentScreen.onHidden(),
							this.container.removeChild(this.currentScreen)
							),
							this.currentScreen = this.nextScreen,
							this.currentScreen.alpha = 0,
							this.currentScreen.onShow && this.currentScreen.onShow(),
							this.currentScreen.resize && this.currentScreen.resize(this.w, this.h),
							TweenLite.to(this.currentScreen, .4, {
							alpha: 1,
								   onComplete : this.onFadein.bind(this)
							}),
							this.container.addChild(this.currentScreen)*/
}

void COverlayManager::addEventListener() {
	//CCLOG("ADD_LISTENER");
	if (mListener == nullptr) {
		Size visibleSize = Director::getInstance()->getVisibleSize();

		//Create a "one by one" touch event listener (processes one touch at a time)
		mListener = EventListenerTouchOneByOne::create();
		// When "swallow touches" is true, then returning 'true' from the onTouchBegan method will "swallow" the touch event, preventing other listeners from using it.
		mListener->setSwallowTouches(true);

		// Example of using a lambda expression to implement onTouchBegan event callback function
		mListener->onTouchBegan = [=](Touch* touch, Event* event) {
			cocos2d::Vec2 p = touch->getLocation();
			cocos2d::Rect rect = cocos2d::Rect(0, 0, visibleSize.width, visibleSize.height);

			if (rect.containsPoint(p)) {
				return true; // to indicate that we have consumed it.
			}

			return false;
		};

		//Trigger when moving touch
		mListener->onTouchMoved = [=](Touch* touch, Event* event) {
		};

		//Process the touch end event
		mListener->onTouchEnded = [=](Touch* touch, Event* event) {
		};

		_eventDispatcher->addEventListenerWithSceneGraphPriority(mListener, this);
	}
}

void COverlayManager::removeEventListener() {
	//CCLOG("REMOVE_LISTENER");
	_eventDispatcher->removeEventListener(mListener);
	mListener = nullptr;
}

void COverlayManager::onShow_add(TOnCallback onCallback) {
	mOnShowCallback = onCallback;
}

void COverlayManager::onHide_add(TOnCallback onCallback) {
	mOnHideCallback = onCallback;
}

void COverlayManager::onShow_remove() {
	mOnShowCallback = nullptr;
}

void COverlayManager::onHide_remove() {
	mOnHideCallback = nullptr;
}
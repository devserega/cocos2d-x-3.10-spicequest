#ifndef __ALPHA_TRANSITION_H__
#define __ALPHA_TRANSITION_H__

#include "cocos2d.h"
#include "screens/CScreenManager.h"
#include "screens/CScreen.h"

using namespace cocos2d;

class CAlphaTransition
	: public cocos2d::Ref
{
	public:
		static CAlphaTransition* create();
		virtual ~CAlphaTransition();
		void begin(CScreenManager* screenManager, CScreen* currentScreen, CScreen* nextScreen);
		void onFadeout();
		void onFadein();
		void resize(unsigned int w, unsigned int h);
		void onResize(unsigned int w, unsigned int h);

	protected:
		CAlphaTransition();
		virtual bool init();

		CScreenManager* mScreenManager;
		CScreen* mCurrentScreen;
		CScreen* mNextScreen;
};

#endif // __ALPHA_TRANSITION_H__

#ifndef __GOLD_POT_H__
#define __GOLD_POT_H__

#include "cocos2d.h"

using namespace cocos2d;

class CGoldPot
	: public cocos2d::Sprite
{
	public:
		static CGoldPot* create();
		bool init();
		virtual ~CGoldPot();

	protected:
		CGoldPot();
};

#endif // __GOLD_POT_H__
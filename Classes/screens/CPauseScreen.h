#ifndef __PAUSE_SCREEN_H__
#define __PAUSE_SCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "CCloseButton.h"
#include "CPiriPiriButton.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CPauseScreen
	: public CScreen
{
	public:
		static CPauseScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CPauseScreen();

		void onShow();
		void onPress(CPiriPiriButton* bt);
		void onMusicMuteToggle();
		void resize(unsigned int w, unsigned int  h);

	protected:
		CPauseScreen();
		Node* mContainer; // my
		Sprite* mBG;
		Sprite* mTitle;
		CPiriPiriButton* mHelpButton;
		CPiriPiriButton* mMusicButton;
		CPiriPiriButton* mPlayButton;
		CPiriPiriButton* mRestartButton;
		CPiriPiriButton* mQuitButton;
		CCloseButton* mCloseButton;
		std::vector<Sprite*> mDots;
};

#endif // __PAUSE_SCREEN_H__
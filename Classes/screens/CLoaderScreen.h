#ifndef __CLOADERSCREEN_H__
#define __CLOADERSCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "Constants.h"

using namespace cocos2d;

class CLoaderScreen
	: public CScreen
{
	public:
		static CLoaderScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CLoaderScreen();

		void onComplete(SEL_CallFunc_onAssetsLoaded functionSelector, cocos2d::Ref* target);
		void onShow();
		void onShown();

	protected:
		CLoaderScreen();
		void onAssetsLoaded();
		void showLoader();

		float sx;
		int index;
		Sprite* bar;
		unsigned int  scaleNumber;
		Node* mContainer;

		// Pointers we need
		cocos2d::Ref* mTarget;
		SEL_CallFunc_onAssetsLoaded mFunctionSelector;
};

#endif // __CLOADERSCREEN_H__

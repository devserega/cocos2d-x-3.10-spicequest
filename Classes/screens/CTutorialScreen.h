#ifndef __TUTORIAL_SCREEN_H__
#define __TUTORIAL_SCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "CCloseButton.h"
#include "com/fido/ui/PixiTrackpad.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CTutorialScreen
	: public CScreen
{
	public:
		static CTutorialScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CTutorialScreen();

		void onShow();
		void onBackPressed();
		void onForwardPressed();
		void onClosePressed();
		void updateTransform(float dt);
		void resize(unsigned int w, unsigned int  h);

	protected:
		CTutorialScreen();
		virtual void onEnter();
		virtual void onExit();
		Node* mContainer; // my
		Sprite* mBG;
		CPiriPiriButton* mHelpButton;
		CPiriPiriButton* mBackButton;
		CPiriPiriButton* mForwardButton;
		CCloseButton* mCloseButton;
		PixiTrackpad* mTrackpad;
		std::vector<Sprite*> mDots;
		std::vector<Sprite*> mPages;
		int mSlot;
		ClippingRectangleNode* mHelpContainer;

		void addListener();
		void removeListener();
		bool myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point);
		EventListenerTouchOneByOne* mListener;
};

#endif // __TUTORIAL_SCREEN_H__
#include "CGameScreen.h"
#include "CLevels.h"
#include "CPiriPiriBlastApp.h"
#include "screens/CGameoverScreen.h"

//#include "ui/CocosGUI.h"
//#include "cocostudio/ActionTimeline/CSLoader.h"

//USING_NS_CC;
//using namespace ui;

CGameScreen::CGameScreen() 
	: mLevels(nullptr)
	, mGameSaveManager(nullptr)
	, mGame(nullptr)
	, mTracking(nullptr)
	//, mListener(nullptr)
	, mApp(nullptr)
	/*
	, mScore(0)
	, mPb(0)
	, mState(0)
	, mNextUnlock(100)
	, mGems(0)
	, mGemsGained(0)
	*/
{
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->addSpriteFramesWithFile("game/GameSprites.plist", "game/GameSprites.png");
}

CGameScreen::~CGameScreen() {
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->removeSpriteFramesFromFile("game/GameSprites.plist");

	//_rootNode->getEventDispatcher()->removeEventListener(mListener);
}

CGameScreen* CGameScreen::create(CPiriPiriBlastApp* app){
	CGameScreen* pRet = new(std::nothrow) CGameScreen();
	if (pRet && pRet->init()){
		pRet->mApp = app;
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CGameScreen::init() {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	mGame = CPiriPiriBlastGame::create();
	mGame->setCascadeOpacityEnabled(true);
	addChild(mGame, 0);

	return true;
}

void CGameScreen::onShow() {
	CTracking::getInstance()->trackEvent("Screen views", "Game screen");
	this->mGame->start();
	this->mApp->mOverlayManager->onShow_add((TOnCallback)std::bind(&CGameScreen::onOverlayShow, this));
	this->mApp->mOverlayManager->onHide_add((TOnCallback)std::bind(&CGameScreen::onOverlayHide, this));
	this->mApp->mTopMenu->gameMode();
	if (this->mApp->mSession.level == 0) this->mApp->mOverlayManager->show("help"); // ORIGINAL 0 === this.app.session.level && this.app.overlayManager.show("help")
}

void CGameScreen::onGameover() {
	CLevel* level = CLevels::getInstance()->get(this->mApp->mSession.level);  // ORIGINAL var level = Levels[this.app.session.level];
	CGameSaveManager::getInstance()->saveScore(level->id, this->mGame->gems);
	CGameSaveManager::getInstance()->saveLevelState(level->id, this->mGame->levelState);

	this->mPb = CGameSaveManager::getInstance()->getPB(level->id);
	this->mIsPB = CGameSaveManager::getInstance()->isPB(level->id, this->mGame->score);
	this->mGems = this->mGame->gemsToGet;
	this->mGemsGained = this->mGame->gems;
	this->mLevelState = this->mGame->levelState;
	this->mNextLevelUnlocked = false;
	
	unsigned long nextLevelId = level->id + 1;
	this->mGame->levelState > 1 && (nextLevelId < CLevels::getInstance()->length() ? this->mNextLevelUnlocked = CGameSaveManager::getInstance()->unlockLevel(nextLevelId) : (this->mNextLevelUnlocked = false, this->mGameComplete = true));
	
	this->mApp->mOverlayManager->show(std::string("gameover"));
}

void CGameScreen::onShown() {
	//this.app.toggleBackground(!1)
}

void CGameScreen::onHide() {
	//this.app.toggleBackground(!0),
	this->mApp->mOverlayManager->onShow_remove(); 	// this.app.overlayManager.onShow.remove(this.onOverlayShow, this),
	this->mApp->mOverlayManager->onHide_remove();	// this.app.overlayManager.onHide.remove(this.onOverlayHide, this),
	//app.view.style["-webkit-filter"] = null
}

void CGameScreen::onHidden() {
	this->mGame->pause();
	this->mApp->mTopMenu->normalMode();
}

void CGameScreen::onOverlayShow() {
	this->mGame->pause();
}

void CGameScreen::onOverlayHide() {
	this->mGame->isGameover ? (this->mGame->reset(), this->mGame->resume()) : this->mGame->resume();
}

void CGameScreen::resize(unsigned int width) {
	//this.game.view.x = w / 2 - 568,
	//	this.game.view.y = 0
}

/*
void CGameScreen::onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto s = Director::getInstance()->getWinSize();
	auto touchLocation = touches[0]->getLocation();
	CCLOG("touchLocation.x=%f touchLocation.y=%f",touchLocation.x, s.height - touchLocation.y);
}

void CGameScreen::onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto touchLocation = touches[0]->getLocation();
}

void CGameScreen::onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto touchLocation = touches[0]->getLocation();
}
*/
#ifndef __OVERLAY_MANAGER_H__
#define __OVERLAY_MANAGER_H__

#include "screens/CScreenManager.h"
#include "screens/CTutorialScreen.h"
#include "screens/CPauseScreen.h"
#include "screens/CGameoverScreen.h"
//#include "screens/CCompleteScreen.h"

class CPiriPiriBlastApp;
class COverlayManager
	: public CScreenManager
{
	public:
		static COverlayManager* create(CPiriPiriBlastApp* app);
		virtual ~COverlayManager();

		void onMouseUp();
		void onMouseDown();
		void onPress();
		void goBack();
		void show(const std::string& screenId);
		void gotoScreen(CScreen* screen);
		void hide();
		void resize(unsigned int w, unsigned int h);
		void onFadeout();

		void onShow_add(TOnCallback onShowCallback);
		void onHide_add(TOnCallback onHideCallback);
		void onShow_remove();
		void onHide_remove();

	protected:
		COverlayManager();
		virtual bool init(CPiriPiriBlastApp* app);
		void addEventListener();
		void removeEventListener();

		CPiriPiriBlastApp* mApp;
		CTutorialScreen* mTutorialScreen;
		CGameoverScreen* mGameoverScreen;
		CPauseScreen* mPauseScreen;

		//CCompleteScreen mCompleteScreen;

		LayerColor* mBlack;
		EventListenerTouchOneByOne* mListener;

		TOnCallback mOnShowCallback;
		TOnCallback mOnHideCallback;
};

#endif // __OVERLAY_MANAGER_H__
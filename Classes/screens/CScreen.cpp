#include "CScreen.h"
#include "CPiriPiriBlastApp.h"

CScreen* CScreen::create(CPiriPiriBlastApp* app) {
	CScreen* pRet = new CScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CScreen::init(CPiriPiriBlastApp* app) {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	mApp = app;

	return true;
}

CScreen::~CScreen() {

}

CScreen::CScreen() 
	: mTransition(nullptr){

}

void CScreen::onHide() {

}

void CScreen::onHidden() {

}

void CScreen::onShow() {

}

void CScreen::onShown() {

}

void CScreen::resize(unsigned int w, unsigned int h) {

}
#include "CPotSlot.h"

CPotSlot::CPotSlot() 
	: pot(nullptr){
}

CPotSlot::~CPotSlot() {
}

CPotSlot* CPotSlot::create() {
	CPotSlot* pRet = new CPotSlot();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		//pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CPotSlot::init() {
	if (!Sprite::initWithSpriteFrameName("PotSlot.png")) {//interfaceSprites/
		return false;
	}

	return true;
}
#include "CLargeGem.h"

CLargeGem::CLargeGem() {
}

CLargeGem::~CLargeGem() {
}

CLargeGem* CLargeGem::create() {
	CLargeGem* pRet = new CLargeGem();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		//pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CLargeGem::init() {
	if (!Sprite::initWithSpriteFrameName("LargeGem.png")) {//interfaceSprites/
		return false;
	}

	return true;
}
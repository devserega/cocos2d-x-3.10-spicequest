#ifndef __POT_SLOT_H__
#define __POT_SLOT_H__

#include "cocos2d.h"
#include "screens/CGoldPot.h"

using namespace cocos2d;

class CPotSlot
	: public cocos2d::Sprite
{
	public:
		static CPotSlot* create();
		bool init();
		virtual ~CPotSlot();

		CGoldPot* pot;

	protected:
		CPotSlot();
};

#endif // __POT_SLOT_H__
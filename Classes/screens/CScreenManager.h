#ifndef __CSCREEN_MANAGER_H__
#define __CSCREEN_MANAGER_H__

#include "cocos2d.h"

using namespace cocos2d;

class CScreen;
class CAlphaTransition;
class CScreenManager
	: public cocos2d::Layer
{
	public:
		static CScreenManager* create();//container, unsigned int width, unsigned int height
		virtual ~CScreenManager();

		void gotoScreenByID(std::string id);//, instant
		void addScreen(CScreen* screen, std::string id);
		void goBack();
		void gotoScreen(CScreen* screen);
		void onTransitionComplete();
		void resize(unsigned int w, unsigned int h);

		unsigned int w;
		unsigned int h;

	protected:
		CScreenManager();
		virtual bool init();//unsigned int width, unsigned int height

		std::map<std::string, CScreen*> screens;

		bool active;
		CScreen* mCurrentScreen;
		CScreen* mNextScreen;
		bool fading;
		std::list<CScreen*> history;
		CAlphaTransition* mTransition;
		CAlphaTransition* mDefaultTransition;
};

#endif // __CSCREEN_MANAGER_H__


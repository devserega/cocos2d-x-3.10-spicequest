#ifndef __GAMESCREEN_H__
#define __GAMESCREEN_H__

#include "cocos2d.h"
#include "CPiriPiriBlastGame.h"
//#include "CLevels.h"
#include "CTracking.h"
#include "CGameSaveManager.h"
#include "screens/CScreen.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CLevels;
//class CTracking;
//class CGameSaveManager;

class CGameScreen
	: public CScreen
{
	public:
		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		// implement the "static create()" method manually
		static CGameScreen* create(CPiriPiriBlastApp* app);
		void onShow();
		void onGameover();
		void onShown();
		void onHide();
		void onHidden();
		void onOverlayShow();	
		void onOverlayHide();
		void resize(unsigned int width);

		//void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
		//void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
		//void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);

	//protected:
		CPiriPiriBlastGame* mGame;
		CGameSaveManager* mGameSaveManager;
		CLevels* mLevels;
		CTracking* mTracking;
		CPiriPiriBlastApp* mApp;

		unsigned long	mScore;
		unsigned long	mPb;
		unsigned long	mState;
		bool			mIsPB;
		unsigned long	mNextUnlock;
		bool			mNextLevelUnlocked;
		unsigned long	mGems;
		unsigned long	mGemsGained;
		unsigned long	mLevelState;
		unsigned long	mGameComplete;

		CGameScreen();
		virtual ~CGameScreen();
};

#endif // __GAMESCREEN_H__
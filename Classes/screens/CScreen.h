#ifndef __SCREEN_H__
#define __SCREEN_H__

#include "cocos2d.h"
#include "Constants.h"
#include "screens/CScreenManager.h"
#include "screens/CAlphaTransition.h"
#include "CPiriPiriButton.h"
#include "CLevelButton.h"
#include "CTracking.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CScreen
	: public cocos2d::Layer
{
	public:
		static CScreen* create(CPiriPiriBlastApp* app);
		virtual ~CScreen();
		virtual bool init(CPiriPiriBlastApp* app);
		virtual void onHide();
		virtual void onHidden();
		virtual void onShow();
		virtual void onShown();
		virtual void resize(unsigned int w, unsigned int h);
		//virtual void onMusicMuteToggle() {};

		CScreenManager* mScreenManager;
		CAlphaTransition* mTransition;

	protected:
		CScreen();
		CPiriPiriBlastApp* mApp;
};

#endif // __SCREEN_H__
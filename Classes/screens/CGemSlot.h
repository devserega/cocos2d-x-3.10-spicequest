#ifndef __GEM_SLOT_H__
#define __GEM_SLOT_H__

#include "cocos2d.h"
#include "screens/CLargeGem.h"

using namespace cocos2d;

class CGemSlot
	: public cocos2d::Sprite
{
	public:
		static CGemSlot* create();
		bool init();
		virtual ~CGemSlot();

		CLargeGem* gem;

	protected:
		CGemSlot();
};

#endif // __GEM_SLOT_H__
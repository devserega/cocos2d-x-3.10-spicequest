#include "CGemSlot.h"

CGemSlot::CGemSlot() 
	: gem(nullptr){
}

CGemSlot::~CGemSlot() {
}

CGemSlot* CGemSlot::create() {
	CGemSlot* pRet = new CGemSlot();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		//pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CGemSlot::init() {
	if (!Sprite::initWithSpriteFrameName("GemSlot.png")) {//interfaceSprites/
		return false;
	}

	return true;
}
#ifndef __LEVEL_SCREEN_H__
#define __LEVEL_SCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "CLevelButton.h"

using namespace cocos2d;

class CLevelScreen
	: public CScreen
{
	public:
		static CLevelScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CLevelScreen();
		void onBackPressed();
		void onButtonPressed(TEvent _event);
		void onShow();
		void resize(unsigned int w, unsigned int  h);

	protected:
		CLevelScreen();
		Node* mLayout; // my
		Sprite* mTitle;
		Node* mContainer;
		CPiriPiriBlastApp* app;
		unsigned int mCellWidth;
		unsigned int mCellSizeX;
		unsigned int mCellSizeY;
		CPiriPiriButton* mBackButton;
		std::vector<CLevelButton*> mLevelPanels;
};

#endif // __LEVEL_SCREEN_H__
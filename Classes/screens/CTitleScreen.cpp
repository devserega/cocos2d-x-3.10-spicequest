#include "CTitleScreen.h"
#include <string>

CTitleScreen* CTitleScreen::create(CPiriPiriBlastApp* app) {
	CTitleScreen* pRet = new CTitleScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CTitleScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setCascadeOpacityEnabled(true);

	BlendFunc tBlendFunc = { GL_SRC_ALPHA, GL_ONE };

	this->mSunBurstFG2 = Sprite::create("game/sunBurstFG.png");
	this->mSunBurstFG2->setScale(2.5f);
	this->mSunBurstFG2->setBlendFunc(tBlendFunc);
	this->mSunBurstFG2->setPosition(visibleSize.width / 2, visibleSize.height / 100 * 72);
	addChild(this->mSunBurstFG2, 1);

	this->mSunBurstFG= Sprite::create("game/sunBurstFG.png");
	this->mSunBurstFG->setScale(1.6f);
	this->mSunBurstFG->setBlendFunc(tBlendFunc);
	this->mSunBurstFG->setPosition(visibleSize.width / 2, visibleSize.height / 100 * 72);
	addChild(this->mSunBurstFG, 2);

	this->mSunBurstBG = Sprite::create("game/sunBurstBG.png");
	this->mSunBurstBG->setScale(2.5f);
	this->mSunBurstBG->setBlendFunc(tBlendFunc);
	this->mSunBurstBG->setPosition(visibleSize.width / 2, visibleSize.height / 100 * 72);
	addChild(this->mSunBurstBG, 3);
		
	this->mLogo = Sprite::createWithSpriteFrameName("SpiceLogo.png");
	this->mLogo->setScale(0.0f);
	this->mLogo->setCascadeOpacityEnabled(true);
	this->mLogo->setPosition(visibleSize.width / 2, visibleSize.height / 100 * 72);
	addChild(this->mLogo, 4);

	this->mPlayBtn = CPlayButton::create();
	this->mPlayBtn->onPress((TOnPressCallbackWithButtonParam) std::bind(&CTitleScreen::onButtonPressed, this, std::placeholders::_1));
	this->mPlayBtn->setScale(0.0f);
	//this->mRestartButton->setPositionX(0);
	this->mPlayBtn->setPosition(visibleSize.width / 2, visibleSize.height / 100 * 30);
	addChild(this->mPlayBtn, 4);

	/*
	mPlayBtnListener = cocos2d::EventListenerTouchOneByOne::create();
	mPlayBtnListener->setSwallowTouches(true);

	mPlayBtnListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* event) {//&
		cocos2d::Vec2 p = touch->getLocation();
		cocos2d::Rect rect = this->getBoundingBox();

		if (rect.containsPoint(p)) {
			//mPlayBtn->onTouchBegan();
			return true; // to indicate that we have consumed it.
		}

		return false; // we did not consume this event, pass thru.
	};

	mPlayBtnListener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* event) {
		//mPlayBtn->onTouchEnded();
		runAction(Sequence::create(	FadeOut::create(0.5f),
									CallFunc::create([this]() {
										auto app = (CPiriPiriBlastApp*) this->getParent();
										auto gameScreen = CGameScreen::create(app);
										app->mGameScreen = gameScreen;
										gameScreen->setCascadeOpacityEnabled(true);
										gameScreen->setOpacity(0.0f);
										app->addChild(gameScreen, 1);
									
										removeFromParentAndCleanup(true);

										gameScreen->runAction(Sequence::create(	FadeIn::create(0.5f),
																				CallFunc::create([this, gameScreen]() {
																					// TODO ...
																					gameScreen->onShow(); //temp solution
																				}),
																				nullptr));
									}),
									nullptr));
	};

	mPlayBtn->getEventDispatcher()->addEventListenerWithFixedPriority(mPlayBtnListener, 1);
	*/

	return true;
}

CTitleScreen::CTitleScreen() 
	: mPlayBtnListener(nullptr)
	, mPlayBtn(nullptr)
	, mSunBurstFG2(nullptr)
	, mSunBurstFG(nullptr)
	, mSunBurstBG(nullptr)
	, mLogo(nullptr)
	, mLogoEaseElasticOutAction(nullptr)
	, mSunBurstBGRotateAction(nullptr)
	, mSunBurstFGRotateAction(nullptr)
	, mSunBurstFG2RotateAction(nullptr)
	, mPlayButtonRotateAction(nullptr)
	, mPlayButtonEaseElasticOutAction(nullptr)
{
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->addSpriteFramesWithFile("game/LoadingSprites.plist", "game/LoadingSprites.png");
	pSFCache->addSpriteFramesWithFile("game/InterfaceSprites.plist", "game/InterfaceSprites.png");
}

CTitleScreen::~CTitleScreen() {
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->removeSpriteFramesFromFile("game/LoadingSprites.plist");
	pSFCache->removeSpriteFramesFromFile("game/InterfaceSprites.plist");

	mPlayBtn->getEventDispatcher()->removeEventListener(mPlayBtnListener);
}

void CTitleScreen::onShow() {
	CTracking::getInstance()->trackEvent("Screen views", "Menu screen");
	this->mLogo->setScale(0);
	this->mLogo->setOpacity(0);
	this->mPlayBtn->setOpacity(0);
	this->mPlayBtn->setScale(0);
	this->mSunBurstBG->setOpacity(0); 
	this->mSunBurstFG->setOpacity(0);
	this->mSunBurstFG2->setOpacity(0);
	//this.twitter.view.alpha = 0,
	//this.twitter.view.scale.set(0),
	//this.fb.view.alpha = 0,
	//this.fb.view.scale.set(0),
	//this.productInfo.view.alpha = 0,
	//this.productInfo.view.scale.set(0),
	//this.storeInfo.view.alpha = 0,
	//this.storeInfo.view.scale.set(0),
	//this.wrapPhoto.scale.set(0),
	//this.textLine.scale.set(0),
	//this.textLine2.scale.set(0)
}

void CTitleScreen::onShown() {
	float wrapDelay = 0; //4

	// stop all previous actions, before start new ones
	// ...

	// run actions
	mLogoEaseElasticOutAction = EaseElasticOut::create(ScaleTo::create(1.0f, 1.0f), 0.3f);
	mLogo->runAction(mLogoEaseElasticOutAction);
	mLogo->runAction(FadeTo::create(.3, 255));
	
	mSunBurstBGRotateAction = RotateBy::create(50.0f, 360);
	mSunBurstBG->runAction(RepeatForever::create(mSunBurstBGRotateAction));
	mSunBurstBG->runAction(FadeTo::create(.8, 255));

	mSunBurstFGRotateAction = RotateBy::create(30.0f, 360);
	mSunBurstFG->runAction(RepeatForever::create(mSunBurstFGRotateAction));
	mSunBurstFG->runAction(FadeTo::create(.8, 255));

	mSunBurstFG2RotateAction = RotateBy::create(60.0f, -360);
	mSunBurstFG2->runAction(RepeatForever::create(mSunBurstFG2RotateAction));
	mSunBurstFG2->runAction(FadeTo::create(.8, 255));

	//mPlayButtonRotateAction = CMyRotateBy::create(60.0f, -360);
	//mPlayButtonEaseElasticOutAction = EaseElasticOut::create(ScaleTo::create(1.0f, 1.0f), 0.3f);
	//mPlayBtn->runAction(RepeatForever::create(mPlayButtonRotateAction));
	//mPlayBtn->runAction(mPlayButtonEaseElasticOutAction);
	mPlayBtn->runAction(Sequence::create(	DelayTime::create(.1 + wrapDelay),
											EaseElasticOut::create(ScaleTo::create(1.0f, 1.0f), 0.3f),
											nullptr));
	mPlayBtn->runAction(Sequence::create(	DelayTime::create(.1 + wrapDelay),
											FadeTo::create(1, 255),
											nullptr));
	mPlayBtn->runAction(RepeatForever::create(CMyRotateBy::create(60.0f, -360)));
}

void CTitleScreen::onButtonPressed(CPiriPiriButton* bt) {
	if (bt == mPlayBtn) {
		//Device.instance.android && (document.body.mozRequestFullScreen ? document.body.mozRequestFullScreen() : document.body.webkitRequestFullScreen && document.body.webkitRequestFullScreen()
		//	), Tracking.trackEvent(["Play"]),
		this->mScreenManager->gotoScreenByID("levelScreen");
		//this->mScreenManager->gotoScreenByID("game");
	}
	/*
	bt == = this.playButton ? (
		Device.instance.android && (document.body.mozRequestFullScreen ? document.body.mozRequestFullScreen() : document.body.webkitRequestFullScreen && document.body.webkitRequestFullScreen()
	), Tracking.trackEvent(["Play"]), 
		this.screenManager.gotoScreenByID("levelScreen")
	) : 
	bt == = this.fb ? Share.postToFacebook({
	method: "feed",
			link : "http://share.mcdonalds.co.uk/littletasters/",
		picture : "http://mcd-littletasters-prod.s3.amazonaws.com/1-0-1/assets/img/Facebook.png",
		name : "Spice Quest – A Peri Peri Snack Wrap® Adventure",
		description : "I’ve found 10 golden Peri Peri pots. How many can you find?"
	},
		function() {
		Tracking.trackEvent(["Facebook"])
	}) : 
	bt == = this.twitter ? (Tracking.trackEvent(["Twitter"]), Share.postToTwitter({
		text: "I just played the McDonald’s Spice Quest – A Peri Peri Snack Wrap® Adventure. Can you find the golden Peri Peri pots?",
			  link : "http://share.mcdonalds.co.uk/littletasters/"
		})) : bt == = this.productInfo ? (Tracking.trackEvent(["Product info clicks"]),
			Device.instance.isMobile ? window.open("http://m.mcdonalds.co.uk/mobile/food/food_details.chicken.100007.200282.peri-peri-snack-wrap.html", "_blank") : window.open("http://www.mcdonalds.co.uk/ukhome/more-food/little-tasters/peri-peri-snack-wrap.html", "_blank")) : bt == = this.storeInfo && (Tracking.trackEvent(["Store finder clicks"]),
				Device.instance.isMobile ? window.open("http://m.mcdonalds.co.uk/mobile/restaurants.html", "_blank") : window.open("http://www.mcdonalds.co.uk/ukhome/Restaurants/restaurant_locator.html", "_blank"))
	*/

	/*
	auto app = (CPiriPiriBlastApp*) this->getParent();
	auto gameScreen = CGameScreen::create(app);
	app->mGameScreen = gameScreen;
	gameScreen->setCascadeOpacityEnabled(true);
	gameScreen->setOpacity(0.0f);
	app->addChild(gameScreen, 1);

	removeFromParentAndCleanup(true);

	gameScreen->runAction(Sequence::create(	FadeIn::create(0.5f),
	CallFunc::create([this, gameScreen]() {
	// TODO ...
	gameScreen->onShow(); //temp solution
	}),
	nullptr));
	*/
}
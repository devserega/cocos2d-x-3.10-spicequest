#include "screens/CTutorialScreen.h"
#include "CPiriPiriBlastApp.h"

CTutorialScreen* CTutorialScreen::create(CPiriPiriBlastApp* app) {
	CTutorialScreen* pRet = new CTutorialScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CTutorialScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->setCascadeOpacityEnabled(true);
	this->mContainer = Node::create();
	this->mContainer->setCascadeOpacityEnabled(true);

	this->mApp = app;
	//this.hitArea = new PIXI.Rectangle(-900, -800, 1800, 1600);
	mBG = Sprite::createWithSpriteFrameName("PaperPanel.png");//interfaceSprites/
	mBG->setAnchorPoint(Vec2(.5, .5));
	mContainer->addChild(mBG, 0);

    //var trackpadParameters = {
	//	target: this
    //};
	this->mTrackpad = PixiTrackpad::create(this);
	this->mTrackpad->snapTo = true;
	this->mTrackpad->size = 600;
	this->mSlot = 0;
	this->mDots.clear();// this.dots = [];
	std::vector<std::string> pagesData = { "HowTo1.png", "HowTo2.png", "HowTo3.png", "HowTo4.png", "HowTo5.png", "HowTo6.png" };
	this->mTrackpad->setSlots(pagesData.size());
	this->mTrackpad->scrollMin = -1e5;
	this->mPages.clear(); // this.pages = [];
	auto helpTitle = Sprite::createWithSpriteFrameName("HowToPlay.png");
	mContainer->addChild(helpTitle, 1);
	helpTitle->setAnchorPoint(Vec2(.5, .5));
	helpTitle->setPositionY(155);
    for (unsigned int pad = 25, j = 0; j < pagesData.size(); j++) {
		auto dot = Sprite::createWithSpriteFrameName("PageMarker.png");
		dot->setAnchorPoint(Vec2(.5f, 1.0f));
		mContainer->addChild(dot, 1);
		float posX = (float)(j * pad) - pagesData.size() * pad / 2.0f;
		dot->setPositionX(posX);
		dot->setPositionY(-145);
		this->mDots.push_back(dot);
    }

	this->mHelpContainer = ClippingRectangleNode::create(); // this.helpContainer = new PIXI.DisplayObjectContainer,
	this->mHelpContainer->setCascadeOpacityEnabled(true);
	this->mHelpContainer->setClippingRegion(Rect(visibleSize.width / 2 - 271, visibleSize.height / 2 - 92, 542, 184));
	this->mHelpContainer->setPosition(-visibleSize.width / 2, -visibleSize.height / 2);
	mContainer->addChild(this->mHelpContainer, 10);
	for (int i = 0; i < pagesData.size(); i++) {
		__String* str = String::createWithFormat("%s", pagesData[i].c_str());
		Sprite* page = Sprite::createWithSpriteFrameName(str->getCString());
		//this.addChild(page);
		page->setAnchorPoint(Vec2(.5,.5));
		page->setPositionY(visibleSize.height / 2 + 10);
		this->mHelpContainer->addChild(page, 20);
		this->mPages.push_back(page);
	}

	this->mHelpButton = CPiriPiriButton::create("Help.png");
	this->mHelpButton->setPositionX(866);
	this->mHelpButton->setPositionY(-102);
	mContainer->addChild(this->mHelpButton, 2);
	/*
       var masky = new PIXI.Graphics;
                masky.beginFill(16763904), 
				masky.drawRect(0, 0, 592, 200), 
				masky.x = -297, 
				masky.y = -100, 
				this.addChild(masky), 
				this.helpContainer.mask = masky;*/

	this->mBackButton = CPiriPiriButton::create("Back.png");
	mContainer->addChild(this->mBackButton, 2),
	this->mBackButton->onPress((TOnPressCallback) std::bind(&CTutorialScreen::onBackPressed, this));
	this->mBackButton->setPositionX(-140);
	this->mBackButton->setPositionY(-158);
	this->mBackButton->setOpacity(0);
	this->mBackButton->setVisible(false);
	this->mBackButton->mInteractive = false;

	this->mForwardButton = CPiriPiriButton::create("Forward.png");
	this->mForwardButton->onPress((TOnPressCallback) std::bind(&CTutorialScreen::onForwardPressed, this));
	this->mForwardButton->setPositionX(140);
	this->mForwardButton->setPositionY(-158);
	mContainer->addChild(this->mForwardButton, 2);

	this->mCloseButton = CCloseButton::create();
	this->mCloseButton->onPress((TOnPressCallback) std::bind(&CTutorialScreen::onClosePressed, this));
	this->mCloseButton->setPositionX(295); // 265
	this->mCloseButton->setPositionY(225); // -255
	mContainer->addChild(this->mCloseButton, 2);

	addChild(mContainer, 1);

	return true;
}

CTutorialScreen::~CTutorialScreen() {
}

CTutorialScreen::CTutorialScreen()
	: mListener(nullptr){
}

void CTutorialScreen::onShow() {
	CTracking::getInstance()->trackEvent("Screen views", "How to play screen");
}

void CTutorialScreen::onBackPressed() {
	this->mTrackpad->previousSlot();
}

void CTutorialScreen::onForwardPressed() {
	this->mTrackpad->nextSlot();
}

void CTutorialScreen::onClosePressed() {
	this->mApp->mOverlayManager->hide(); // ORIGINAL this.app.overlayManager.hide()
}

void CTutorialScreen::updateTransform(float dt) {
	if (this->isVisible()) {
		this->mTrackpad->update();
		for (int i = 0; i < this->mPages.size(); i++) {
			Sprite* page = this->mPages[i];
			if (i==0) {
				//CCLOG("i=%d this.trackpad.value=%f this.trackpad.size=%f", i, this->mTrackpad->value, this->mTrackpad->size);
			}

			page->setPositionX(this->mTrackpad->value + (i+1) * this->mTrackpad->size);
			page->setVisible(page->getPositionX() > -600 && page->getPositionX() < 1500);
			this->mDots[i]->setOpacity(this->mTrackpad->currentSlot == i ? 255 : 127); //alpha = this.trackpad.currentSlot == i ? 1 : .5;
		}
		if (this->mTrackpad->currentSlot != this->mSlot) {
			this->mSlot = this->mTrackpad->currentSlot;
			bool backVisible = 0 != this->mSlot;
			if (this->mBackButton->isVisible() != backVisible) {
				this->mBackButton->mInteractive = backVisible;
				if (this->mBackButton->isVisible() == false)
					mBackButton->setVisible(backVisible);
				auto action = Sequence::create(	EaseSineOut::create(FadeTo::create(.3f, backVisible ? 255 : 0)),
												CallFunc::create([this, backVisible]() {
													mBackButton->setVisible(backVisible);
												}),
												nullptr);
				this->mBackButton->runAction(action);
			}
			bool forwardVisible = this->mSlot < this->mPages.size() - 1;
			if (this->mForwardButton->isVisible() != forwardVisible) {
				this->mForwardButton->mInteractive = forwardVisible;
				if(this->mForwardButton->isVisible()==false)
					mForwardButton->setVisible(forwardVisible);
				auto action = Sequence::create(	EaseSineOut::create(FadeTo::create(.3f, forwardVisible ? 255 : 0)),
												CallFunc::create([this, forwardVisible]() {
													mForwardButton->setVisible(forwardVisible);
												}),
												nullptr);
				this->mForwardButton->runAction(action);
			}
		}
		//PIXI.DisplayObjectContainer.prototype.updateTransform.call(this)
	}
}

void CTutorialScreen::resize(unsigned int w, unsigned int  h) {
	// this.x = w / 2, this.y = h / 2, this.w = w, this.h = h
}

void CTutorialScreen::onEnter() {
	Layer::onEnter();
	addListener();
	this->schedule(schedule_selector(CTutorialScreen::updateTransform), 0.0167f);
}

void CTutorialScreen::onExit() {
	Layer::onExit();
	removeListener();
	this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CTutorialScreen::updateTransform));
}

void CTutorialScreen::addListener() {
	if (!mListener) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		mListener = cocos2d::EventListenerTouchOneByOne::create();
		mListener->setSwallowTouches(true);

		mListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			if (!this->isVisible()) { //this->mEnabled
				return false;
			}

			Size visibleSize = Director::getInstance()->getVisibleSize();

			//auto target = //static_cast<Sprite*>(e->getCurrentTarget());
			cocos2d::Vec2 global = this->getParent()->convertToNodeSpace(touch->getLocation());
			//global.x += visibleSize.width / 2;
			//CCLOG("XXXdata.global.x=%f data.global.y=", global.x, global.y);

			this->mTrackpad->onDown(global);

			return true; // we did not consume this event, pass thru.
		};

		mListener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			this->mTrackpad->onUp();
		};
		mListener->onTouchMoved = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			cocos2d::Vec2 global = this->getParent()->convertToNodeSpace(touch->getLocation());

			this->mTrackpad->onMove(global);
		};
		
		dispatcher->addEventListenerWithSceneGraphPriority(mListener, this);
	}
}

void CTutorialScreen::removeListener() {
	if (mListener != nullptr) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->removeEventListener(mListener);
		mListener = nullptr;
	}
}

bool CTutorialScreen::myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point) {
	bool bRet = false;

	if (point.x >= rect.origin.x && point.x <= rect.size.width &&
		point.y >= rect.origin.y && point.y <= rect.size.height) {
		bRet = true;
	}

	return bRet;
}
#ifndef __LARGE_GEM_H__
#define __LARGE_GEM_H__

#include "cocos2d.h"

using namespace cocos2d;

class CLargeGem
	: public cocos2d::Sprite
{
	public:
		static CLargeGem* create();
		bool init();
		virtual ~CLargeGem();

	protected:
		CLargeGem();
};

#endif // __LARGE_GEM_H__
#ifndef __GAMEOVER_SCREEN_H__
#define __GAMEOVER_SCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "screens/CPotSlot.h"
#include "screens/CLargeGem.h"
#include "screens/CGoldPot.h"
#include "screens/CGemSlot.h"

using namespace cocos2d;

class CPiriPiriBlastApp;
class CGameoverScreen
	: public CScreen
{
	public:
		static CGameoverScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CGameoverScreen();

		void onShow();
		void showResult(unsigned int gemCount);
		void perfect();
		void pushDown();
		void updateTransform(float dt);
		void onPress(CPiriPiriButton* bt);
		void resize(unsigned int w, unsigned int h);

	protected:
		CGameoverScreen();
		virtual void onEnter() override;
		virtual void onExit() override;

		CPiriPiriBlastApp* mApp;
		Sprite* mBg;
		CPiriPiriButton* mRestartButton;
		CPiriPiriButton* mMenuButton;
		CPiriPiriButton* mScoresButton;
		CPiriPiriButton* mMusicButton;
		CPiriPiriButton* mContinueButton;
		Sprite* mTitle;
		Sprite* mCopy;
		Sprite* mLosePot;
		Node* mBurst;
		Sprite* mSunBurstBG;
		Sprite* mSunBurstFG;
		Sprite* mSunBurstFG2;
		Node* mResults;
		CPotSlot* mPotSlot;
		std::vector<CGemSlot*> mGemSlots;
		std::vector<std::string> mTexts;
		Node* mContainer; // my
};

#endif // __GAMEOVER_SCREEN_H__

#ifndef __TITLESCREEN_H__
#define __TITLESCREEN_H__

#include "cocos2d.h"
#include "screens/CScreen.h"
#include "CPlayButton.h"
#include "CPiriPiriButton.h"
#include "actions/CMyRotateBy.h"

using namespace cocos2d;

class CTitleScreen
	: public CScreen
{
	public:
		static CTitleScreen* create(CPiriPiriBlastApp* app);
		virtual bool init(CPiriPiriBlastApp* app);
		virtual ~CTitleScreen();
		virtual void onShow();
		virtual void onShown();
		void onButtonPressed(CPiriPiriButton* bt);

	protected:
		CTitleScreen();

	private:
		EventListenerTouchOneByOne* mPlayBtnListener;
		CPiriPiriButton* mPlayBtn;
		Sprite* mSunBurstFG2;
		Sprite* mSunBurstFG;
		Sprite* mSunBurstBG;
		Sprite* mLogo;

		EaseElasticOut* mLogoEaseElasticOutAction;
		RotateBy* mSunBurstBGRotateAction;
		RotateBy* mSunBurstFGRotateAction;
		RotateBy* mSunBurstFG2RotateAction;
		CMyRotateBy* mPlayButtonRotateAction;
		EaseElasticOut* mPlayButtonEaseElasticOutAction;
};

#endif // __TITLESCREEN_H__
#include "CGoldPot.h"

CGoldPot::CGoldPot() {
}

CGoldPot::~CGoldPot() {
}

CGoldPot* CGoldPot::create() {
	CGoldPot* pRet = new CGoldPot();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		//pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CGoldPot::init() {
	if (!Sprite::initWithSpriteFrameName("GoldPot.png")) {//interfaceSprites/
		return false;
	}

	return true;
}
#include "screens/CPauseScreen.h"
#include "CPiriPiriBlastApp.h"

CPauseScreen* CPauseScreen::create(CPiriPiriBlastApp* app) {
	CPauseScreen* pRet = new CPauseScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CPauseScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->mApp = app;
	this->mContainer = Node::create();

	mBG = Sprite::createWithSpriteFrameName("PaperPanel.png");
	mBG->setAnchorPoint(Vec2(.5, .5));
	mContainer->addChild(mBG, 0);

	mTitle = Sprite::createWithSpriteFrameName("Paused.png");
	mTitle->setAnchorPoint(Vec2(.5, .5));
	mTitle->setPositionX(0);
	mTitle->setPositionY(160);
	mContainer->addChild(mTitle, 1);

	mHelpButton = CPiriPiriButton::create("Help.png");
	mHelpButton->onPress((TOnPressCallbackWithButtonParam)std::bind(&CPauseScreen::onPress, this, std::placeholders::_1));
	mHelpButton->setPositionX(-90);
	mHelpButton->setPositionY(6);
	mContainer->addChild(mHelpButton, 1);

	mMusicButton = CPiriPiriButton::create("SoundOn.png");
	mMusicButton->onPress((TOnPressCallbackWithButtonParam)std::bind(&CPauseScreen::onPress, this, std::placeholders::_1));
	mMusicButton->setPositionX(90);
	mMusicButton->setPositionY(6);
	mContainer->addChild(mMusicButton, 1);

	mPlayButton = CPiriPiriButton::create("PlaySmall.png");
	mPlayButton->onPress((TOnPressCallbackWithButtonParam)std::bind(&CPauseScreen::onPress, this, std::placeholders::_1));
	mPlayButton->setPositionX(115);
	mPlayButton->setPositionY(-155);
	mContainer->addChild(mPlayButton, 1);

	mRestartButton = CPiriPiriButton::create("Restart.png");
	mRestartButton->onPress((TOnPressCallbackWithButtonParam)std::bind(&CPauseScreen::onPress, this, std::placeholders::_1));
	mRestartButton->setPositionX(0);
	mRestartButton->setPositionY(-155);
	mContainer->addChild(mRestartButton, 1),

	mQuitButton = CPiriPiriButton::create("LevelMenu.png");
	mQuitButton->onPress((TOnPressCallbackWithButtonParam)std::bind(&CPauseScreen::onPress, this, std::placeholders::_1));
	mQuitButton->setPositionX(-115);
	mQuitButton->setPositionY(-155);
	mContainer->addChild(mQuitButton, 1),

	//SoundManager.music.onMuteToggle.add(this.onMusicMuteToggle, this);
	CSoundManager::getInstance()->music->onMuteToggle((TOnCallback) std::bind(&CPauseScreen::onMusicMuteToggle, this));
	setOpacity(0);

	addChild(mContainer, 1);

	return true;
}

CPauseScreen::~CPauseScreen() {
}

CPauseScreen::CPauseScreen() {
}

void CPauseScreen::onShow() {
	CTracking::getInstance()->trackEvent("Screen views", "Game pause screen");
}

void CPauseScreen::onPress(CPiriPiriButton* bt) {
	if (bt == this->mRestartButton){
		this->mApp->mGameScreen->mGame->reset();
		this->mApp->mOverlayManager->hide();
	}
	else if (bt == this->mQuitButton) {
		this->mApp->mOverlayManager->hide();
		this->mApp->mScreenManager->gotoScreenByID("levelScreen");
	}
	else if (bt == this->mHelpButton) {
		this->mApp->mOverlayManager->gotoScreenByID("help");
	}
	else if (bt == this->mMusicButton) {
		if (CSoundManager::getInstance()->music->mIsMuted)
			CSoundManager::getInstance()->music->unmute();
		else
			CSoundManager::getInstance()->music->mute();

		if (CSoundManager::getInstance()->sfx->mIsMuted)
			CSoundManager::getInstance()->sfx->unmute();
		else
			CSoundManager::getInstance()->sfx->mute();
	}
	else if (bt == this->mPlayButton) {
		this->mApp->mOverlayManager->hide();
	}
}

void CPauseScreen::onMusicMuteToggle() {
	auto soundManager = CSoundManager::getInstance();
	this->mMusicButton->setIcon(soundManager->music->mIsMuted ? "SoundOff.png" : "SoundOn.png");
}

void CPauseScreen::resize(unsigned int w, unsigned int  h) {
	// this.x = w / 2, this.y = h / 2, this.w = w, this.h = h
}
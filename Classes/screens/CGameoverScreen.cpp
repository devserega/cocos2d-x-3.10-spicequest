#include "screens/CGameoverScreen.h"
#include "CGameScreen.h"
#include "CPiriPiriBlastApp.h"

CGameoverScreen* CGameoverScreen::create(CPiriPiriBlastApp* app){
	CGameoverScreen *pRet = new(std::nothrow) CGameoverScreen();
	if (pRet && pRet->init(app)){
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	} 
	else{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CGameoverScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	this->mContainer = Node::create();
	this->mContainer->setCascadeOpacityEnabled(true);


	mApp = app;
	mBg = Sprite::createWithSpriteFrameName("PaperPanel.png");//interfaceSprites/
	mBg->setAnchorPoint(Vec2(.5,.5));
	mContainer->addChild(mBg, 1);

	this->mRestartButton = CPiriPiriButton::create("Restart.png");//interfaceSprites/
	this->mRestartButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CGameoverScreen::onPress, this, std::placeholders::_1));
	this->mRestartButton->setPositionX(0);
	this->mRestartButton->setPositionY(this->mRestartButton->getContentSize().height-155);
	mContainer->addChild(this->mRestartButton, 2);

	this->mMenuButton = CPiriPiriButton::create("LevelMenu.png");//interfaceSprites/
	this->mMenuButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CGameoverScreen::onPress, this, std::placeholders::_1));
	this->mMenuButton->setPositionX(-115);
	this->mMenuButton->setPositionY(this->mMenuButton->getContentSize().height - 155);
	mContainer->addChild(this->mMenuButton, 2);

	this->mContinueButton = CPiriPiriButton::create("Continue.png");//interfaceSprites/
	this->mContinueButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CGameoverScreen::onPress, this, std::placeholders::_1));
	this->mContinueButton->setPositionX(115);
	this->mContinueButton->setPositionY(this->mContinueButton->getContentSize().height - 155);
	mContainer->addChild(this->mContinueButton, 2);

	/*
		this.resultText = new PIXI.Text("", {
		fill: "white",
			  font : "26px Arial",
			dropShadow : !0,
			wordWrap : !0,
			wordWrapWidth : 360
		}),
		this.resultText.x = -315,
		this.resultText.y = -40*/
	setOpacity(0);

	this->mTitle = Sprite::createWithSpriteFrameName("OhNo.png");//interfaceSprites/
	this->mTitle->setAnchorPoint(Vec2(.5, .5));
	this->mTitle->setPosition(Vec2(0, 160));
	mContainer->addChild(this->mTitle, 2);

	this->mCopy = Sprite::createWithSpriteFrameName("OhNoCopy.png");//interfaceSprites/
	this->mCopy->setAnchorPoint(Vec2(.5, .5));
	this->mCopy->setPositionY(-55);
	mContainer->addChild(this->mCopy, 2);
	
	this->mLosePot = Sprite::createWithSpriteFrameName("NoBarrels.png");//interfaceSprites/
	this->addChild(this->mLosePot, 2);
	this->mLosePot->setAnchorPoint(Vec2(.5, .5));
	this->mLosePot->setPosition(Vec2(0, 35)); 
	this->mResults = Node::create();
	mContainer->addChild(this->mResults, 2);
	this->mResults->setPositionX(-20);
	this->mResults->setPositionY(40);
	CPotSlot* potSlot = CPotSlot::create();
	CGoldPot* goldPot = CGoldPot::create();
	potSlot->pot = goldPot;
	potSlot->addChild(goldPot,1);
	goldPot->setPosition(Vec2(potSlot->getContentSize().width/2, potSlot->getContentSize().height/2)); // my
	goldPot->setAnchorPoint(Vec2(.5f,.5f));
	potSlot->setAnchorPoint(Vec2(.5,.5));
	this->mResults->addChild(potSlot);
	potSlot->setPositionX(-140);
	this->mPotSlot = potSlot;
	this->mGemSlots.clear();
	for (unsigned int i = 0; 3 > i; i++) {
		auto gemSlot = CGemSlot::create();
		gemSlot->setAnchorPoint(Vec2(.5, .5));
		auto gem = CLargeGem::create();
		gem->setAnchorPoint(Vec2(.5, .5));
		gemSlot->gem = gem;
		gemSlot->addChild(gem);
		gem->setPosition(Vec2(gemSlot->getContentSize().width / 2, gemSlot->getContentSize().height / 2)); // my
		this->mResults->addChild(gemSlot);
		this->mGemSlots.push_back(gemSlot);
		gemSlot->setPositionX(90 * i);
	}
	this->mBurst = Node::create();
	this->mBurst->setCascadeOpacityEnabled(true);
	this->mSunBurstBG = Sprite::create("game/SunburstBG.png");
	this->mSunBurstBG->setScale(3.75f);
	this->mSunBurstBG->setAnchorPoint(Vec2(.5f, .5f));
	this->mSunBurstBG->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSunBurstFG = Sprite::create("game/SunburstFG.png");
	this->mSunBurstFG->setScale(1.6f * 1.5f);
	this->mSunBurstFG->setAnchorPoint(Vec2(.5f, .5f));
	this->mSunBurstFG->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSunBurstFG2 = Sprite::create("game/SunburstFG.png");
	this->mSunBurstFG2->setScale(3.75);
	this->mSunBurstFG2->setAnchorPoint(Vec2(.5f, .5f));
	this->mSunBurstFG2->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSunBurstFG2->setOpacity(.6f * 255);
	this->mBurst->addChild(this->mSunBurstBG);
	this->mBurst->addChild(this->mSunBurstFG);
	this->mBurst->addChild(this->mSunBurstFG2);
	this->addChild(this->mBurst, 0);

	addChild(mContainer, 1);

	return true;
}

CGameoverScreen::CGameoverScreen(){
}

CGameoverScreen::~CGameoverScreen() {
}

void CGameoverScreen::onEnter(){
	Layer::onEnter();
	this->schedule(schedule_selector(CGameoverScreen::updateTransform), 0.015f); // 0.005f
}

void CGameoverScreen::onExit() {
	Layer::onExit();
	this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CGameoverScreen::updateTransform));
}

void CGameoverScreen::onShow() {
	CGameScreen* gameDetail = this->mApp->mGameScreen;
	this->mBurst->setVisible(false);
	std::vector<std::string> texts;
	texts.push_back(std::string("So you played the level and its still locked? Are you cheating..."));
	if (gameDetail->mLevelState == LEVEL_STATES::UNLOCKED) {
		texts.push_back(std::string("Failed - You ran out of bombs and didn't find a path"));
		this->mTitle->setSpriteFrame("OhNo.png");
		this->mCopy->setSpriteFrame("OhNoCopy.png");
		this->mCopy->setOpacity(255); // my
		this->mLosePot->setVisible(true);
		this->mResults->setVisible(false);
		CTracking::getInstance()->trackEvent("Screen views", "Try again screen");
	}
	else if (gameDetail->mLevelState == LEVEL_STATES::COMPLETE) {
		texts.push_back(std::string("Success - You found a path to the gate"));
		this->mTitle->setSpriteFrame("GreatWork.png");
		this->mCopy->setSpriteFrame("GreatWorkCopy.png");
		this->mCopy->setOpacity(255); // my
		this->mLosePot->setVisible(false);
		this->mResults->setVisible(true);
		this->showResult(gameDetail->mGemsGained);
		CTracking::getInstance()->trackEvent("Screen views", "Peri peri pot screen");
	}
	else if (gameDetail->mLevelState == LEVEL_STATES::COMPLETE_WITH_GEMS) {
		texts.push_back(std::string("Success - You found a path to the gate and all of the gems!"));
		this->mCopy->setSpriteFrame("AmazingCopy.png");
		this->mCopy->setOpacity(255); // my
		this->mTitle->setSpriteFrame("Amazing.png");
		this->mLosePot->setVisible(false);
		this->mResults->setVisible(true);
		this->showResult(3);
		CTracking::getInstance()->trackEvent("Screen views", "True spice explorer screen");
	}

	std::string randText = texts[rand() % texts.size()];
	//this.resultText.setText(randText),

	//if (gameDetail->mNextLevelUnlocked || this->mApp->mSession.level >= 9)
	//	this->mContinueButton->enable();
	//else
	//	this->mContinueButton->disable();
	//(gameDetail->mNextLevelUnlocked || this->mApp->mSession.level >= 9) ? this->mContinueButton->enable() : this->mContinueButton->disable();
	(gameDetail->mNextLevelUnlocked || this->mApp->mSession.level < 9) ? this->mContinueButton->enable() : this->mContinueButton->disable();
}

void CGameoverScreen::showResult(unsigned int gemCount) {
	auto pot = this->mPotSlot->pot;
	pot->setScale(5);
	pot->setOpacity(0);
	pot->setRotation((((double)std::rand() / (RAND_MAX)) - .5f) * (180 / PI)); //pot.rotation = Math.random() - .5,
	this->mCopy->setOpacity(0);

	pot->runAction(Sequence::create(DelayTime::create(.7f),
									EaseElasticOut::create(ScaleTo::create(.4f, 1), 0.3f),
									nullptr));
	
	pot->runAction(Sequence::create(DelayTime::create(.7f),
									EaseElasticOut::create(RotateTo::create(.8f, 0), 0.3f),
									nullptr));

	pot->runAction(Sequence::create(DelayTime::create(.7f),
									FadeIn::create(.1f),
									nullptr));

	/*
			TweenLite.to(pot.scale, .4, {
			x: 1,
			   y : 1,
				ease : Bounce.easeOut,
				delay : .7
			}),
			TweenLite.to(pot, .8, {
			rotation: 0,
					  ease : Bounce.easeOut,
				delay : .7
			}),
			TweenLite.to(pot, .1, {
			alpha: 1,
				   ease : Sine.easeOut,
				delay : .7
			}),
			TweenLite.delayedCall(.85, this.pushDown.bind(this));
	*/

	for (unsigned int i = 0; i < this->mGemSlots.size(); i++) {
		auto gem = this->mGemSlots[i]->gem;
		gem->setSpriteFrame("LargeGem.png");
		gem->setScale(5);
		gem->setOpacity(0);
		gem->setRotation((((double)std::rand() / (RAND_MAX)) - .5) * (180 / PI)); // rotation = Math.random() - .5;

		double delay = 1 + .2 * (i + 1);
		if (gemCount > i) {
			gem->runAction(Sequence::create(DelayTime::create(delay),
											EaseElasticOut::create(ScaleTo::create(.4f, 1), 0.3f),
											nullptr));
			gem->runAction(Sequence::create(DelayTime::create(delay),
											EaseElasticOut::create(RotateTo::create(.8f, 0), 0.3f),
											nullptr));

			gem->runAction(Sequence::create(DelayTime::create(delay),
											EaseElasticOut::create(FadeIn::create(.1f), 0.3f),
											nullptr));

			runAction(Sequence::create(	DelayTime::create(1 + .2 * (i + 1) + .15),
										CallFunc::create([this]() {
											this->pushDown();
										}),
										nullptr));

			/* original
			TweenLite.to(gem.scale, .4, {
				x: 1,
				y: 1,
				ease : Bounce.easeOut,
				delay : delay
			}),
			TweenLite.to(gem, .8, {
				rotation: 0,
				ease : Bounce.easeOut,
				delay : delay
			}),
			TweenLite.to(gem, .1, {
				alpha: 1,
				ease : Sine.easeOut,
				delay : delay
			}),
			TweenLite.delayedCall(1 + .2 * (i + 1) + .15, this.pushDown.bind(this)))
			*/
		}
		mCopy->runAction(Sequence::create(	DelayTime::create(1.8f + .1f),
											EaseSineOut::create(FadeIn::create(.1f)),
											nullptr));
		if (3 == gemCount) {
			runAction(Sequence::create(	DelayTime::create(1.95f),
										CallFunc::create([this]() {
											this->perfect();
										}),
										nullptr));
		}

		/* original
		TweenLite.to(this.copy, .3, {
		alpha: 1,
			   ease : Sine.easeOut,
			delay : 1.8 + .1
		}), 
		3 == = gemCount && TweenLite.delayedCall(1.95, this.perfect.bind(this))
		*/
	}
}

void CGameoverScreen::perfect() {
	this->mBurst->setVisible(true);
	CSoundManager::getInstance()->sfx->play("perfect");
	//this.app.flash.flash(.1, 1.5);
	for (unsigned int i = 0; i < this->mGemSlots.size(); i++) {
		CLargeGem* gem = this->mGemSlots[i]->gem;
		gem->setSpriteFrame("GoldGem.png");
	}
}

void CGameoverScreen::pushDown() {
	CSoundManager::getInstance()->sfx->play("completeThud");
	mContainer->setScaleX(mContainer->getScaleX() - .1);
	mContainer->setRotation(mContainer->getRotation() + .01);
}

void CGameoverScreen::updateTransform(float dt) {
	//CCLOG("test %f",dt);
	//if (this->visible) {
	float scaleX = mContainer->getScaleX() + .4f*((1 - mContainer->getScaleX()));
	//CCLOG("scaleX=%f", scaleX);
	mContainer->setScaleX(mContainer->getScaleX()+.4f*((1 - mContainer->getScaleX())));
	mContainer->setRotation(mContainer->getRotation()+ .4 * (0 - mContainer->getRotation()));
	mContainer->setScaleY(mContainer->getScaleX());
	//PIXI.DisplayObjectContainer.prototype.updateTransform.call(this);
	mSunBurstBG->setRotation(mSunBurstBG->getRotation()+.0025f*(180 / PI));// * PI /180) 
	mSunBurstFG->setRotation(mSunBurstFG->getRotation()-.005f*(180 / PI));// * PI /180) 
	mSunBurstFG2->setRotation(mSunBurstFG2->getRotation()+.0075f*(180 / PI));// * PI /180) 
	//}
}
				
void CGameoverScreen::onPress(CPiriPiriButton* bt) {
	if (bt == this->mRestartButton) {
		CTracking::getInstance()->trackEvent("Replay");
		this->mApp->mGameScreen->mGame->reset();
		this->mApp->mOverlayManager->hide(); // original this.app.overlayManager.hide()
	}
	else if (bt == this->mMenuButton) {
		CTracking::getInstance()->trackEvent("Back to menu clicks");
		this->mApp->mScreenManager->gotoScreenByID("levelScreen");
		this->mApp->mOverlayManager->hide(); // original this.app.overlayManager.hide()
	}
	else if (bt == this->mScoresButton) {
		//this.app.overlayManager.gotoScreenByID("scores") 
	}
	else if (bt == this->mMusicButton) {
		auto sm = CSoundManager::getInstance();
		sm->music->mIsMuted ? sm->music->unmute() : sm->music->mute();
		//SoundManager.music.isMuted ? SoundManager.music.unmute() : SoundManager.music.mute()
	}
	else if (bt == this->mContinueButton) {
		CTracking::getInstance()->trackEvent("Next level clicks");
		this->mApp->mSession.level++; 
		if (this->mApp->mSession.level >= 10) {
			this->mApp->mOverlayManager->gotoScreenByID("title"); // ORIGINAL this->mApp->mOverlayManager->gotoScreenByID("complete");
		}
		else {
			this->mApp->mGameScreen->mGame->reset();
			this->mApp->mOverlayManager->hide();
		}
		// ORIGINAL app.session.level >= 10 ? this.app.overlayManager.gotoScreenByID("complete") : (this.app.gameScreen.game.reset(), this.app.overlayManager.hide())
	}

	/*
	bt == = this.restartButton ? (Tracking.trackEvent(["Replay"]), this.app.gameScreen.game.reset(), this.app.overlayManager.hide()) : 
	bt == = this.menuButton ? (Tracking.trackEvent(["Back to menu clicks"]), this.app.screenManager.gotoScreenByID("levelScreen"), this.app.overlayManager.hide()) : 
	bt == = this.scoresButton ? this.app.overlayManager.gotoScreenByID("scores") : 
	bt == = this.musicButton ? SoundManager.music.isMuted ? SoundManager.music.unmute() : SoundManager.music.mute() : 
	bt == = this.continueButton && (Tracking.trackEvent(["Next level clicks"]), app.session.level++, app.session.level >= 10 ? this.app.overlayManager.gotoScreenByID("complete") : (this.app.gameScreen.game.reset(), this.app.overlayManager.hide()))
	*/
}
				
void CGameoverScreen::resize(unsigned int w, unsigned int h) {
	// this.w = w, this.h = h, this.x = w / 2, this.y = h / 2
}
#include "CLoaderScreen.h"
#include "CPiriPiriBlastApp.h"
#include <string>

CLoaderScreen* CLoaderScreen::create(CPiriPiriBlastApp* app) {
	CLoaderScreen* pRet = new CLoaderScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

CLoaderScreen::~CLoaderScreen() {
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->removeSpriteFramesFromFile("game/LoadingSprites.plist");
}

CLoaderScreen::CLoaderScreen() {
	SpriteFrameCache* pSFCache = SpriteFrameCache::getInstance();
	pSFCache->addSpriteFramesWithFile("game/LoadingSprites.plist", "game/LoadingSprites.png");
}

bool CLoaderScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	setCascadeOpacityEnabled(true);

	this->mContainer = Node::create();
	this->mContainer->setCascadeOpacityEnabled(true);
	mContainer->setPosition(visibleSize.width / 2, visibleSize.height / 2);

	auto loadingFrame = Sprite::createWithSpriteFrameName("LoadingFrame.png");
	loadingFrame->setAnchorPoint(Vec2(0.0f,1.0f));
	loadingFrame->setPosition(-194, -42);
	this->mContainer->addChild(loadingFrame, 1);

	bar = Sprite::createWithSpriteFrameName("LoadingBar.png");
	bar->setAnchorPoint(Vec2(0.0f, 1.0f));
	bar->setPosition(-177, -71);
	bar->setScaleX(0.0f);

	auto text = Sprite::createWithSpriteFrameName("Loading.png");
	text->setAnchorPoint(Vec2(0.0f, 1.0f));
	text->setPosition(-96, -134);

	float scale = 0.62f;
	auto logo = Sprite::createWithSpriteFrameName("SpiceLogo.png");
	logo->setAnchorPoint(Vec2(0.0f, 1.0f));
	logo->setScale(scale);
	logo->setPosition(-500 * scale * .5, 150);

	this->mContainer->addChild(bar,2);
	this->mContainer->addChild(text, 3);
	this->mContainer->addChild(logo, 4);

	//Ticker.instance.add(this.update, this),
	addChild(this->mContainer, 1); // original this.addChild(this.container),
	//this.resize(this.w, this.h),
	this->mContainer->setOpacity(0);  // original this.container.alpha = 0,

	this->scaleNumber = 351;
	
	return true;
}

void CLoaderScreen::onComplete(SEL_CallFunc_onAssetsLoaded functionSelector, cocos2d::Ref* target) {
	mTarget = target;
	mFunctionSelector = functionSelector;
}

void CLoaderScreen::onShow() {}

void CLoaderScreen::onShown() {
	// ORIGINAL this.prepreloader.load()
	mContainer->runAction(	Sequence::create(	//DelayTime::create(1.0f),
												FadeIn::create(0.2f),
												//DelayTime::create(1.0f),
												CallFunc::create([this]() {
													showLoader();
												}),
												nullptr));
}

void CLoaderScreen::showLoader() {
	std::vector<std::string> files;
	files.push_back("game/WireframeGrid.jpg");
	files.push_back("game/GameSprites.png");

	/*
	files.push_back("game/LargeGem.png");
	files.push_back("game/NoBarrels.png");
	files.push_back("game/OhNo.png");
	files.push_back("game/SpicePotB.png");
	files.push_back("game/SpicePotA.png");

	files.push_back("game/Amazing.png");
	files.push_back("game/AmazingCopy.png");
	files.push_back("game/Back.png");
	files.push_back("game/Barrel.png");
	files.push_back("game/BrickWall.png");
	files.push_back("game/CloseClick.png");
	files.push_back("game/CloseUp.png");
	files.push_back("game/CompleteCopy.png");
	files.push_back("game/Congratulations.png");
	*/

	index = 0;
	for (std::vector<std::string>::iterator it = files.begin(); it != files.end(); ++it) {
		cocos2d::Director::getInstance()->getTextureCache()->addImageAsync((*it), [=](Texture2D* texture) {
			index++;
			sx = (index*7.08f) / files.size();
			bar->setScaleX(sx);
			if (index == files.size()) {
				onAssetsLoaded();
				return;
			}
		});
	}
}

void CLoaderScreen::onAssetsLoaded() {
	if (mTarget != nullptr && mFunctionSelector != nullptr) {
		(mTarget->*mFunctionSelector)();
		this->mApp->mScreenManager->gotoScreenByID("title");
	}

	/* original
	"true" == = Utils.get_query("fastplay") ? Utils.get_query("level") != = !1 ? (
	app.session.level = Utils.get_query("level") - 1,
	this.screenManager.gotoScreenByID("game")
	) : this.app.screenManager.gotoScreenByID("levelScreen") : this.app.screenManager.gotoScreenByID("title")
	*/
}
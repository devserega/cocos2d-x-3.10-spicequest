#include "CAlphaTransition.h"

CAlphaTransition* CAlphaTransition::create() {
	CAlphaTransition* pRet = new(std::nothrow) CAlphaTransition();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

CAlphaTransition::~CAlphaTransition() {
}

CAlphaTransition::CAlphaTransition()
	: mScreenManager(nullptr)
	, mCurrentScreen(nullptr)
	, mNextScreen(nullptr){
}

bool CAlphaTransition::init() {

	return true;
}

void CAlphaTransition::begin(CScreenManager* screenManager, CScreen* currentScreen, CScreen* nextScreen) {
	mScreenManager = screenManager;
	mCurrentScreen = currentScreen;
	mNextScreen = nextScreen;
	if (mCurrentScreen) {
		this->mCurrentScreen->onHide();
		mCurrentScreen->runAction(Sequence::create(	FadeOut::create(0.4f), // 0.4f
													CallFunc::create([this]() {
														this->onFadeout();
													}),
													nullptr));
	}
	else {
		this->onFadeout();
	}
}

void CAlphaTransition::onFadeout() {
	if (mCurrentScreen) {
		mCurrentScreen->onHidden();
		mCurrentScreen->removeFromParent();
		mCurrentScreen->setOpacity(255);
	}
	mNextScreen->setOpacity(0);
	mNextScreen->onShow();
	mNextScreen->resize(mScreenManager->w, mScreenManager->h);
	mScreenManager->addChild(mNextScreen, 1);
	mNextScreen->runAction(Sequence::create( FadeIn::create(0.4), // 0.4f
											 CallFunc::create([this]() {
												this->onFadein();
											 }),
											 nullptr));
}

void CAlphaTransition::onFadein() {
	mNextScreen->onShown();
	mScreenManager->onTransitionComplete();
}

void CAlphaTransition::resize(unsigned int w, unsigned int h) {
	//    this.w = w, this.h = h
}

void CAlphaTransition::onResize(unsigned int w, unsigned int h) {

}
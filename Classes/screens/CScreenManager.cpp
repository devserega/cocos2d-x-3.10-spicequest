#include "CScreenManager.h"
#include "screens/CScreen.h"
#include "screens/CAlphaTransition.h"

CScreenManager::CScreenManager() 
	: mCurrentScreen(nullptr)
	, mNextScreen(nullptr)
	, mTransition(nullptr)
	, mDefaultTransition(nullptr)
	, fading(false)
	, active(false){
}

CScreenManager::~CScreenManager() {

}

CScreenManager* CScreenManager::create() {// unsigned int width, unsigned int height
	CScreenManager* pRet = new CScreenManager();
	if (pRet && pRet->init()) {//width, height
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CScreenManager::init() {//unsigned int width, unsigned int height
	if (!cocos2d::Node::init()) {
		return false;
	}

	//this.container = container || new PIXI.DisplayObjectContainer,
	this->screens.clear();
	//this->currentScreen;
	this->fading = false;
	//this->w = width || 400;
	//this->h = height || 400;
	this->history.clear(); 	// ORIGINAL this.history = [],
	this->mDefaultTransition = CAlphaTransition::create();
	this->mTransition = this->mDefaultTransition;
	this->active = false;

	return true;
}

void CScreenManager::gotoScreenByID(std::string id/*, instant*/) {
	this->gotoScreen(this->screens[id]/*, instant*/);
}

void CScreenManager::addScreen(CScreen* screen, std::string id) {
	this->screens[id] = screen;
	screen->mScreenManager = this;
}

void CScreenManager::goBack() {
	//sum += this->history.back();
	//mylist.pop_back();
	//CScreen* prev = this->history.pop_back();

	//this.history.pop();
	//var prev = this.history.pop();
	//prev && this.gotoScreen(prev);
}

void CScreenManager::gotoScreen(CScreen* screen) {
	if (this->mCurrentScreen != screen && screen!=nullptr) {
		this->history.push_back(screen);
		mNextScreen = screen;
		if (!this->active) {
			this->active = true;
			screen->mTransition ? this->mTransition = screen->mTransition : this->mTransition = this->mDefaultTransition;
			this->mTransition->onResize(w, h);
			this->mTransition->begin(this, mCurrentScreen, mNextScreen);
			this->mCurrentScreen = screen;
		}
	}
	/* ORIGINAL
	this.currentScreen != = screen && (
		this.history.push(screen),
		this.nextScreen = screen,
		this.active || (this.active = !0,
			this.transition = screen.transition || this.defaultTransition,
			this.transition.onResize && this.transition.onResize(this.w, this.h),
			this.transition.begin(this, this.currentScreen, this.nextScreen), this.currentScreen = screen))
			*/
}

void CScreenManager::onTransitionComplete() {
	this->active = false;
	if(mCurrentScreen != mNextScreen) 
		this->gotoScreen(mNextScreen);
}

void CScreenManager::resize(unsigned int w, unsigned int h) {
	this->w = w;
	this->h = h;
	/*
	this.transition.onResize && this.transition.onResize(w, h),
	this.currentScreen && this.currentScreen.resize && this.currentScreen.resize(w, h)
	*/
}
#include "screens/CLevelScreen.h"
#include "CGameSaveManager.h"
#include "CLevels.h"
#include "CPiriPiriBlastApp.h"

CLevelScreen* CLevelScreen::create(CPiriPiriBlastApp* app) {
	CLevelScreen* pRet = new CLevelScreen();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CLevelScreen::init(CPiriPiriBlastApp* app) {
	if (!CScreen::init(app)) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);
	this->mLayout = Node::create();
	this->mLayout->setCascadeOpacityEnabled(true);
	this->mLayout->setPositionY(visibleSize.height);

	this->mTitle = Sprite::createWithSpriteFrameName("SelectLevel.png");//interfaceSprites/
	this->mTitle->setAnchorPoint(Vec2(0,1));
	this->mTitle->setPositionX(453);
	this->mTitle->setPositionY(visibleSize.height - 76);
	this->addChild(mTitle, 1);

	this->mContainer = Node::create();
	this->mContainer->setCascadeOpacityEnabled(true);
	this->mContainer->setPosition(Vec2(20, -100)); // my -50
	//this->mLayout->addChild(this->mContainer, 2);
	this->addChild(this->mContainer, 2);
	//this.game = app;
	this->mCellWidth = 5;
	this->mCellSizeX = 180;
	this->mCellSizeY = 230;
	this->mLevelPanels.clear();

	auto save = CGameSaveManager::getInstance();
	for (unsigned int i = 0; i < CLevels::getInstance()->length(); i++) {
		CLevelButton* button = CLevelButton::create(CLevels::getInstance()->get(i)); //	var button = (save.levelStates[i], new LevelButton(Levels[i]));
		button->mId = i;
		button->onPress((TOnPressCallbackWithEventParam) std::bind(&CLevelScreen::onButtonPressed, this, std::placeholders::_1));
		unsigned int x = i % this->mCellWidth;
		unsigned int y = i / this->mCellWidth | 0;
		button->setPositionX(100 + x * this->mCellSizeX + .5 * this->mCellSizeX); // !!!! my version
		button->setPositionY(visibleSize.height - y * this->mCellSizeY - .5 * this->mCellSizeY);
		button->setScale(0, 0);
		//button.view.pivot.x = .5 * this.cellSizeX,
		//button.view.pivot.y = .5 * this.cellSizeY,
		button->setAnchorPoint(Vec2(.5, .5));
		this->mContainer->addChild(button);
		this->mLevelPanels.push_back(button);
	}
	this->mBackButton = CPiriPiriButton::create("Back.png");
	this->addChild(this->mBackButton, 1);
	this->mBackButton->onPress((TOnPressCallback)std::bind(&CLevelScreen::onBackPressed, this));
	this->mBackButton->setPositionX(55);
	this->mBackButton->setPositionY(visibleSize.height - 580);

	addChild(this->mLayout, 0);

	return true;
}

CLevelScreen::~CLevelScreen() {
}

CLevelScreen::CLevelScreen()
	: mTitle(nullptr)
	, mContainer(nullptr){
}

void CLevelScreen::onBackPressed() {
	this->mScreenManager->gotoScreenByID("title");
}

void CLevelScreen::onButtonPressed(TEvent _event) {
	auto save = CGameSaveManager::getInstance();
	int state = save->mLevelStates[_event.mId];
	if (0 != state) {
		this->mApp->mSession.level = _event.mId;
		this->mScreenManager->gotoScreenByID("game");
		CTracking::getInstance()->trackEvent("Level", _event.mId + 1);
	}
	/* ORIGINAL
	var save = GameSaveManager.instance.save,
			state = save.levelStates[event.id];
		0 == = state || (
			app.session.level = event.id, this.screenManager.gotoScreenByID("game"), 
			Tracking.trackEvent(["Level", event.id + 1])
		)
	*/
}

void CLevelScreen::onShow() {
	float delayTime = .12;
	auto save = CGameSaveManager::getInstance();
	for (unsigned int i = 0; i < this->mLevelPanels.size(); i++) {
		CLevelButton* panel = this->mLevelPanels[i];
		int state = save->mLevelStates[i];
		panel->setState(state, save->mLevelBestScores[i]);
		panel->setAnchorPoint(Vec2(.5, .5));
		panel->runAction(Sequence::create(	DelayTime::create(i * delayTime),
											EaseElasticOut::create(ScaleTo::create(.5f, 1.0f), 0.3f),
											CallFunc::create([this]() {}),
											nullptr));

		/* ORIGINAL 
			TweenLite.to(panel.view.scale, .5, {
				x: 1,
				y : 1,
				delay : i * delayTime,
				ease : Elastic.easeOut,
				data : {
					id: i
				},
				onStart : function() {}
			})
		*/
	}
}

void CLevelScreen::resize(unsigned int w, unsigned int  h) {
	//this.container.position.x = .5 * w - 510 + 50,
	//	this.container.position.y = .5 * h - 160
}
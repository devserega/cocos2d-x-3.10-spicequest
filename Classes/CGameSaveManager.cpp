#include "CGameSavemanager.h"

//cache hear: C:\Users\serega\AppData\Local\SpiceQuestBase

bool CGameSaveManager::instanceFlag = false;
CGameSaveManager* CGameSaveManager::single = nullptr;

CGameSaveManager::CGameSaveManager(){
}

bool CGameSaveManager::init() {
	if (!cocos2d::Node::init()) {
		return false;
	}

	//this.onLoaded = new Signal,
	//this.localStorage = new LocalStorage("com.GoodBoyDigital.PiriPiriBlastGame"), 
	//this.save = null

	this->mTotalScore = 0;
	this->mLevelStates = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	this->mLevelBestScores = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	return true;
}

void CGameSaveManager::loadGame() {
	this->loadGameFromLocalStorage();
}

void CGameSaveManager::loadGameFromLocalStorage() {
	UserDefault* ud = UserDefault::getInstance();
	if (ud) {
		mTotalScore = ud->getIntegerForKey("totalScore", 0);
		for (unsigned int i = 0; i < 10; i++) {
			int def = 0;
			if (i == 0) def = 1;
			const char* lsKey = String::createWithFormat("ls%d", i)->getCString();
			int ls = ud->getIntegerForKey(lsKey, def);
			this->mLevelStates[i] = ls;

			def = 0;
			const char* lbsKey = String::createWithFormat("lbs%d", i)->getCString();
			int lbs = ud->getIntegerForKey(lbsKey, def);
			this->mLevelBestScores[i] = lbs;
		}
	}

	//var save = this.localStorage.getObject("save");
	//save || (save = new GameSave, this.localStorage.storeObject("save", save)), 
	//this.save = save, this.onLoaded.dispatch()
}

bool CGameSaveManager::isPB(unsigned long levelId, unsigned long score) {
	int bestScore = this->mLevelBestScores[levelId];
	return score > bestScore;
}

unsigned long CGameSaveManager::getPB(unsigned long levelId) {
	int bestScore = this->mLevelBestScores[levelId];
	return bestScore;
}

bool CGameSaveManager::unlockLevel(unsigned long levelId) {
	if (0 == this->mLevelStates[levelId]) {
		this->mLevelStates[levelId] = 1;
		this->saveToStorage();
	}
	return true;
}

bool CGameSaveManager::saveLevelState(unsigned long levelId, unsigned long levelState) {
	if (levelState > this->mLevelStates[levelId]) {
		this->mLevelStates[levelId] = levelState;
		this->saveToStorage();
	}
	return true;
}

bool CGameSaveManager::isPerfectComplete() {
	for (unsigned int i = 0; i < this->mLevelStates.size(); i++)
		if (3 != this->mLevelStates[i]) return false;
	return true;
}

void CGameSaveManager::saveScore(unsigned long levelId, unsigned long score) {
	int bestScore = this->mLevelBestScores[levelId];
	this->mTotalScore = this->mTotalScore || 0;
	this->mTotalScore += score;
	if (score > bestScore) this->mLevelBestScores[levelId] = score;
	this->saveToStorage();
}

void CGameSaveManager::saveToStorage() {
	UserDefault* ud = UserDefault::getInstance();
	if (ud) {
		mTotalScore = ud->getIntegerForKey("totalScore", 0);
		for (unsigned int i = 0; i < 10; i++) {
			const char* lsKey = String::createWithFormat("ls%d", i)->getCString();
			ud->setIntegerForKey(lsKey, this->mLevelStates[i]);
			const char* lbsKey = String::createWithFormat("lbs%d", i)->getCString();
			ud->setIntegerForKey(lbsKey, this->mLevelBestScores[i]);
		}
		ud->flush();
	}

	// original this.localStorage.storeObject("save", this.save)
}
#include "CPiriPiriBlastApp.h"
#include "CTicker.h"
#include "screens/COverlayManager.h"
#include "CGameSaveManager.h"

CPiriPiriBlastApp* CPiriPiriBlastApp::mThis = nullptr;

CPiriPiriBlastApp* CPiriPiriBlastApp::createScene() {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	mThis = new CPiriPiriBlastApp();
	CGameSaveManager::getInstance()->loadGame();
	mThis->mScreenManager = CScreenManager::create();
	mThis->addChild(mThis->mScreenManager, 0);

	mThis->addChild(CTicker::getInstance(), -1);
	CTicker::getInstance()->start();

	//  this.session = new FilmClubSession, 
	mThis->mLoaderScreen = CLoaderScreen::create(mThis);
	mThis->mLoaderScreen->onComplete(callfunc_onAssetsLoaded_selector(CPiriPiriBlastApp::onAssetsLoaded), mThis);
	mThis->mScreenManager->addScreen(mThis->mLoaderScreen, "loader");
	mThis->mScreenManager->gotoScreenByID("loader");

	mThis->mBg = Sprite::create("game/mapBG.jpg");
	mThis->mBg->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	mThis->mScreenManager->addChild(mThis->mBg, 0);

	mThis->mLogo = Sprite::create("MCTag.png");
	mThis->mLogo->setAnchorPoint(Vec2(0.5, 1.0f));
	mThis->mLogo->setPosition(120.0f, visibleSize.height);
	mThis->mLogo->setVisible(false); 
	mThis->addChild(mThis->mLogo, 1);

	// return the scene
	return mThis;
}

CPiriPiriBlastApp::CPiriPiriBlastApp() 
	: mScreenManager(nullptr)
	, mIsMobile(true){

}

CPiriPiriBlastApp::~CPiriPiriBlastApp() {

}

void CPiriPiriBlastApp::onAssetsLoaded() {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->mTitleScreen = CTitleScreen::create(this);
	this->mGameScreen =  CGameScreen::create(this);
	this->mLevelScreen = CLevelScreen::create(this);
	this->mScreenManager->addScreen(this->mTitleScreen, "title");
	this->mScreenManager->addScreen(this->mGameScreen, "game");
	this->mScreenManager->addScreen(this->mLevelScreen, "levelScreen");
	this->mTopMenu = CTopMenu::create(this);
	this->addChild(this->mTopMenu, 3);
	this->mOverlayManager = COverlayManager::create(this);
	this->mOverlayManager->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2)); // my
	this->addChild(this->mOverlayManager, 4);
	//this.flash = new Flash(1e3, 1e3);
	//this.stage.addChild(this.flash);
	//this.resize(this.w, this.h);
	//this.stage.addChild(this.logo);

	this->initSound();
}

void CPiriPiriBlastApp::initSound() {
	//std::string ASSET_URL_STR(ASSET_URL);
	auto sm = CSoundManager::getInstance();
	//sm->music->addSound(String::createWithFormat("%s%s", ASSET_URL, "snd//music")->getCString(), "music", Options(true, .5f));
	sm->music->addSound("snd/music", "music", Options(true, .5f));
	sm->music->play("music");
	sm->sfx->addSound("snd/button-roll", "buttonRoll");
	sm->sfx->addSound("snd/button-click", "buttonPress", Options(.5f));
	sm->sfx->addSound("snd/barrel-place", "placeBarrel");
	sm->sfx->addSound("snd/barrel-blast", "explodeBarrel1");
	sm->sfx->addSound("snd/pot-blast", "explodePot1");
	sm->sfx->addSound("snd/pot-blast-2", "explodePot2");
	sm->sfx->addSound("snd/level-perfect", "perfect");
	sm->sfx->addGroup({"explodePot1", "explodePot2"}, "explodePot");
	sm->sfx->addGroup({"explodeBarrel1"}, "explodeBarrel");
	sm->sfx->addSound("snd/gem-drop", "gemDrop");
	sm->sfx->addSound("snd/icon-thud", "completeThud");
	sm->sfx->addSound("snd/gold-pot-get", "levelClear");
	sm->sfx->addSound("snd/boots-walk", "bootsWalk", Options(true));
}

void CPiriPiriBlastApp::resize(unsigned int w, unsigned int h) {
	/*
	 this.w = w, this.h = h;
            var scale = 1;
            if (this.isMobile) {
                scale = window.devicePixelRatio ? window.devicePixelRatio : window.screen.deviceXDPI / window.screen.logicalXDPI;
                var isPortrate = h > w;
                void 0 === this.orientation ? this.orientation = isPortrate : this.orientation != isPortrate && (this.orientation = isPortrate)
            }
            var ratio = w / this.safeSize.width < h / this.safeSize.height ? w / this.safeSize.width : h / this.safeSize.height,
                w2 = Math.min(this.maxSize.width * ratio, w),
                h2 = Math.min(this.maxSize.height * ratio, h);
            if (h > w) {
                Ticker.instance.stop();
                var ratioImage = w / 288 > h / 446 ? w / 288 : h / 446;
                this.rotateScreen.width = 288 * ratioImage, this.rotateScreen.height = 446 * ratioImage, this.rotateScreen.style.left = w / 2 - 288 * ratioImage / 2 + "px", this.rotateScreen.style.top = h / 2 - 446 * ratioImage / 2 + "px", this.rotateScreen.style.display = "block"
            } else Ticker.instance.start(), this.rotateScreen.style.display = "none";
            this.renderer.resize(w2 * scale | 0, h2 * scale | 0), this.view.style.width = w2 + "px", this.view.style.height = h2 + "px", this.view.style.left = w / 2 - w2 / 2 + "px", this.view.style.top = h / 2 - h2 / 2 + "px", this.screenManager.resize(w2 / ratio, h2 / ratio), this.overlayManager && (this.overlayManager.resize(w2 / ratio, h2 / ratio), this.overlayManager.view.scale.set(ratio * scale), this.topMenu.scale.set(ratio * scale), this.topMenu.resize(w2 / ratio, h2 / ratio), this.flash.resize(w2 * scale, h2 * scale)), this.logo.scale.set(ratio * scale), this.screenManager.container.scale.set(ratio * scale)
	*/
}
#ifndef __PIRIPIRIBLASTGAME_H__
#define __PIRIPIRIBLASTGAME_H__

#include "Constants.h"
#include "CHud.h"
#include "gridworld/CPiriPiriGridWorld.h"
#include "CBombManager.h"
#include "CExplosionManager.h"
#include "CTickerListener.h"
#include "effects/CEffectsManager.h"
#include "footsteps/CFootsteps.h"
#include "CPointAndClickController.h"

using namespace cocos2d;

//class CGridCell;
class CPiriPiriBlastGame
	: public cocos2d::Layer
	, public CTickerListener
{
	public:
		static CPiriPiriBlastGame* create();
		virtual bool init();
		virtual ~CPiriPiriBlastGame();

		void reset();
		void addToScore(/*points, target*/);
		void start(); //config
		void pause();
		void resume();
		void update(float dt);
		void takeBomb();
		void addGem();
		void onStepsComplete(std::vector<CGridCell*> result, CGridCell* startCell, CGridCell* endCell, float delay);
		void gameBoardCleared();
		void runGameBoardClear();
		void levelComplete();
		void onLevelComplete();
		void gameover();

		void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
		void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
		void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);

	public:
		CPiriPiriBlastGame();

		bool isGameover;
		bool paused;
		unsigned long bombs;
		//config = config || this.config,
		//this.config = config,
		unsigned long baseSpeed;
		double gameTime;
		unsigned long score;
		unsigned long gems;
		unsigned long gemsToGet;
		unsigned long levelState;
		unsigned long scoreMultiplyer;

		//private:
		Sprite* mfloor;
		CHud* mHud;
		CPointAndClickController* mController;
		Layer* mExplosionLayer;
		CPiriPiriGridWorld* mGridWorld;
		std::vector<int> mLevels;
		CLevel* mLevel;
		CBombManager* mBombManager;
		CExplosionManager* mExplosionManager;
		CEffectsManager* mEffectsManager;
		Sprite* mSunBurst1;
		Sprite* mSunBurst2;
		CFootsteps* mFootsteps;
};

#endif // __PIRIPIRIBLASTGAME_H__
#include "CSignalBinding.h"

CSignalBinding::CSignalBinding() {
}

CSignalBinding::~CSignalBinding() {
}

bool CSignalBinding::init() {
	//if (!cocos2d::Ref::init()) {
	//	return false;
	//}

	//mActive = false;
	//mDeltaTime = 1;
	//mTimeElapsed = 0;
	//mLastTime = 0;
	//mSpeed = 1;

	return true;
}

CSignalBinding* CSignalBinding::create() {
	CSignalBinding* pRet = new CSignalBinding();
	if (pRet && pRet->init()) {
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}
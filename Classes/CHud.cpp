#include "CHud.h"
#include "CPiriPiriBlastGame.h"
#include "CGem.h"
#include "CSpiceExplode.h"
#include "screens/CGameScreen.h"
#include "media/CSoundManager.h"

CHud::CHud() {
}

CHud::~CHud() {
	int t1 = 212;
}

bool CHud::init() {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	int nudge = 70;

	mLevelLabel = Sprite::createWithSpriteFrameName("LevelHud.png");//interfaceSprites/
	mLevelLabel->setCascadeOpacityEnabled(true);
	mLevelLabel->setAnchorPoint(Vec2(1.0f, .5f));
	mLevelLabel->setPositionX(310 + nudge);
	mLevelLabel->setPositionY(visibleSize.height - 67);
	addChild(mLevelLabel, 1);

	mLevelNumber = Label::createWithBMFont("game/font_number-export.fnt", "22");
	mLevelNumber->setPositionX(120);
	mLevelNumber->setPositionY(0);
	mLevelNumber->setAnchorPoint(Vec2(0.0f, 0.0f));
	mLevelLabel->addChild(mLevelNumber, 1);

	mHudBomb = Sprite::createWithSpriteFrameName("HudBarrel.png");//interfaceSprites/
	mHudBomb->setAnchorPoint(Vec2(.5f, .5)); 
	mHudBomb->setPositionX(422 + nudge);
	mHudBomb->setPositionY(visibleSize.height - 56);
	addChild(mHudBomb);

	//mLives = LabelBMFont::create("x30", "game/font_number-export.fnt");
	//mLives->setAnchorPoint(Vec2(0.0f, 0.0f));
	//mLives->setPositionX(520);
	//mLives->setPositionY(visibleSize.height - 86);
	//addChild(mLives, 1);

	mBombs = Label::createWithBMFont("game/font_number-export.fnt", "x30");
	mBombs->setAnchorPoint(Vec2(0.0f, 0.0f));
	mBombs->setPositionX(466 + nudge);
	mBombs->setPositionY(visibleSize.height - 86);//66
	addChild(mBombs, 1);

	/*
	this.addChild(this.bombs),
	this.score = new PIXI.Text("SCORE : 0000", format),
	this.score.anchor.x = 1,
	this.score.position.y = 520,
	this.score.position.x = 960 + nudge,*/

	mHudGem = Sprite::createWithSpriteFrameName("HudGem.png");//interfaceSprites/
	mHudGem->setAnchorPoint(Vec2(.5,.5)); 
	mHudGem->setPositionX(610 + nudge);
	mHudGem->setPositionY(visibleSize.height - 56);
	addChild(mHudGem, 1);

	mGems = Label::createWithBMFont("game/font_number-export.fnt", "0/0");
	mGems->setAnchorPoint(Vec2(0.0f, 0.0f));
	mGems->setPositionX(657 + nudge);
	mGems->setPositionY(visibleSize.height - 86);
	addChild(mGems, 1);

	/*
	this.gems = new TextElement(0),
	this.gems.position.y = 66,
	this.gems.position.x = 657 + nudge,
	this.addChild(this.gems),
	this.detailPool = new GameObjectPool(DetailText),
	this.spiceExplode = new GameObjectPool(SpiceExplode),
	this.gemPool = new GameObjectPool(Gem),
	this.activeSpiceExplodes = [],*/
	mActiveGems.clear(); // this.activeGems = {},
	/*
	this.gameoverText = new Pop,
	this.gameoverText.onComplete.add(this.onGameoverComplete, this),
	this.levelCompleteText = new Pop,
	this.levelCompleteText.onComplete.add(this.onLevelComplete, this),*/
	mStartArrow = Sprite::createWithSpriteFrameName("Start.png");//interfaceSprites/
	mStartArrow->setAnchorPoint(Vec2(.5, .5));
	mStartArrow->setVisible(false);
	mEndArrow = Sprite::createWithSpriteFrameName("Finish.png");//interfaceSprites/
	mEndArrow->setAnchorPoint(Vec2(.5, .5));
	mEndArrow->setVisible(false);
	addChild(mStartArrow, 1);
	addChild(mEndArrow, 1);

	return true;
}

CHud* CHud::create(CPiriPiriBlastGame* game) {
	CHud* pRet = new(std::nothrow) CHud();
	if (pRet && pRet->init()) {
		pRet->mGame=game;
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

void CHud::onPausePressed() {
	this->mGame->reset();
}

void CHud::reset() {
	this->mBombs->setString(String::createWithFormat("x%lu", this->mGame->bombs)->getCString());
	this->mGems->setString(String::createWithFormat("%lu/%lu", this->mGame->gems, this->mGame->gemsToGet)->getCString());
	
	// not tested
	for (unsigned int i = this->mActiveSpiceExplodes.size(); i--;) {
		CSpiceExplode* spiceExplosion = this->mActiveSpiceExplodes[i];
		this->removeChild(spiceExplosion);
		//this.spiceExplode.returnObject(spiceExplosion);
		this->mActiveSpiceExplodes.erase(mActiveSpiceExplodes.begin() + i, mActiveSpiceExplodes.begin() + i + 1);
		//this.activeSpiceExplodes.splice(i, 1)
	}

	for (const auto& obj : this->mActiveGems) {
		auto key = obj.first;
		auto gem = this->mActiveGems[key];
		this->removeChild(gem);
		//this.gemPool.returnObject(gem), 
		delete this->mActiveGems[key];
		this->mActiveGems.erase(key); // my
	}
	this->mLevelNumber->setString(String::createWithFormat("%s", this->mGame->mLevel->name.c_str())->getCString());
}

void CHud::updateBombs() {
	if (this->mGame->bombs < 10)
		this->mBombs->setString(String::createWithFormat("x0%lu", this->mGame->bombs)->getCString());
	else
		this->mBombs->setString(String::createWithFormat("x%lu", this->mGame->bombs)->getCString());
}

void CHud::updateGems() {
	this->mGems->setString(String::createWithFormat("%lu/%lu", this->mGame->gems, this->mGame->gemsToGet)->getCString());
}

void CHud::updateScore(/*score, target*/) {
	/*
	            var detailText = this.detailPool.getObject();
            this.score.setText("SCORE : " + Utils.formatScore(this.game.score)), 
			detailText.setup(target.position.x, target.position.y - 80, score, target.multiplyer), 
			this.addChild(detailText), 
			detailText.visible = !0, 
			detailText.onFinish.addOnce(this.onDetailFinish, this)
	*/
}

void CHud::onDetailFinish(/*detailText*/) {
	//detailText.visible = !1, this.detailPool.returnObject(detailText)
}

void CHud::update() {
	for (unsigned int i = this->mActiveSpiceExplodes.size(); i--;) this->mActiveSpiceExplodes[i]->update();
	for (std::map<std::string, CGem*>::reverse_iterator it = this->mActiveGems.rbegin(); it != this->mActiveGems.rend(); ++it) {
		it->second->update();
	}
}

void CHud::showGem(unsigned int x, unsigned int y) {
	CSoundManager::getInstance()->sfx->play("gemDrop");
	CGem* gem = CGem::create();
	gem->reset();
	//gem->onComplete.addOnce(this.gemFinish, this);
	gem->setPositionX(x + OFFSETS::x);
	gem->setPositionY(y - OFFSETS::y);
	const char* key = String::createWithFormat("%d-%d", x, y)->getCString();
	this->mActiveGems[key] = gem;
	addChild(gem);
	this->mGame->addGem();
	gem->goBoom();
}

void CHud::clearGem(unsigned int x, unsigned int y) {
	const char* key = String::createWithFormat("%d-%d", x, y)->getCString();
	std::map<std::string, CGem*>::iterator it = mActiveGems.find(key);
	if (it != mActiveGems.end()) {
		it->second->hide(x, y);
	}
}

void CHud::gemFinish(CGem* gem, unsigned int x, unsigned int y) {
	const char* key = String::createWithFormat("%d-%d", x, y)->getCString();
	this->removeChild(gem);
	//this.gemPool.returnObject(gem);
	std::map<std::string, CGem*>::iterator it = mActiveGems.find(key);
	if (it != mActiveGems.end()) {
		delete it->second;
	}
	// original delete this.activeGems[x + "-" + y]
}

void CHud::showSpiceExplode(unsigned int x, unsigned int y) {
	//var spiceExplode = this.spiceExplode.getObject();
	CSpiceExplode* spiceExplode = CSpiceExplode::create();
	spiceExplode->onComplete_addOnce((TOnCompleteCallbackWithSEParam) std::bind(&CHud::onSpiceExplodeFinish, this, std::placeholders::_1));
	this->mActiveSpiceExplodes.push_back(spiceExplode);
	this->addChild(spiceExplode);
	//spiceExplode->mDisc->mDepthOffset = -1e3;
	//this->mGame->addChild(spiceExplode.disc),//this.game.world.view.addChild(spiceExplode.disc),
	spiceExplode->mDisc->setPosition(spiceExplode->getPosition());
	spiceExplode->setPositionX(x);
	spiceExplode->setPositionY(y);
	spiceExplode->goBoom();
}

void CHud::onSpiceExplodeFinish(CSpiceExplode* spiceExplosion) {
	//	this.spiceExplode.returnObject(spiceExplosion),
	//	this.activeSpiceExplodes.splice(this.activeSpiceExplodes.indexOf(spiceExplosion), 1)
	std::vector<CSpiceExplode*>::iterator foundElement = std::find_if(	mActiveSpiceExplodes.begin(), mActiveSpiceExplodes.end(),
																		[spiceExplosion](CSpiceExplode* vecElement) {return vecElement == spiceExplosion; });
	mActiveSpiceExplodes.erase(foundElement); // original this.bombs.splice(this.bombs.indexOf(bomb), 1);
	this->removeChild(spiceExplosion);
}

void CHud::showLevelComplete() {
	this->mGame->onLevelComplete();
}

void CHud::showGameover() {
	CGameScreen* gameScreen = dynamic_cast<CGameScreen*>(this->getParent()->getParent());
	if (gameScreen) 
		gameScreen->onGameover();
	// original this.game.onGameover.dispatch()
}

void CHud::onGameoverComplete() {
	CGameScreen* gameScreen = dynamic_cast<CGameScreen*>(this->getParent()->getParent());
	if (gameScreen)
		gameScreen->onGameover();
	// original this.game.onGameover.dispatch()
}

void CHud::onLevelComplete() {
	this->mGame->onLevelComplete();
}

void CHud::showStartAndFinish() {
	mStartArrow->setVisible(true);
	mStartArrow->setPositionX(mGame->mGridWorld->getStartCell()->centerX + 25);
	mStartArrow->setPositionY(mGame->mGridWorld->getStartCell()->centerY - 45 - 120 - 23);
	mStartArrow->setOpacity(255); // my
	mStartArrow->setScale(0);

	mEndArrow->setVisible(true);
	mEndArrow->setPositionX(mGame->mGridWorld->getEndCell()->centerX + 28); // getPositionX()
	mEndArrow->setPositionY(mGame->mGridWorld->getEndCell()->centerY + 16); // getPositionY()
	mEndArrow->setOpacity(255); // my
	mEndArrow->setScale(0);

	float bigDelay = 1.0f;

	mStartArrow->runAction(Sequence::create(DelayTime::create(bigDelay),
											EaseElasticOut::create(ScaleTo::create(1.0f, 1.0f), 0.3f),
											nullptr));

	mStartArrow->runAction(Sequence::create(DelayTime::create(bigDelay + 2),
											EaseElasticOut::create(ScaleTo::create(0.3f, 0.0f), 0.3f),
											nullptr));

	mStartArrow->runAction(Sequence::create(DelayTime::create(bigDelay + 4),
											FadeTo::create(0.3f, 0),
											nullptr));

	mEndArrow->runAction(Sequence::create(	DelayTime::create(bigDelay + 1),
											EaseElasticOut::create(ScaleTo::create(1.0f, 1.0f), 0.3f),
											nullptr));

	mEndArrow->runAction(Sequence::create(	DelayTime::create(bigDelay + 3),
											EaseElasticOut::create(ScaleTo::create(0.3f, 0.0f), 0.3f),
											nullptr));

	mEndArrow->runAction(Sequence::create(	DelayTime::create(bigDelay + 5),
											FadeTo::create(0.3f, 0),
											nullptr));
}

void CHud::resize(unsigned int w, unsigned int h) {
	/*
	this.score.position.y = h - 90,
		this.score.position.x = 512 + w / 2 - 32 - 10,
		this.gameoverText.x = w / 2 + 32 + 10,
		this.gameoverText.y = h / 2
	*/
}
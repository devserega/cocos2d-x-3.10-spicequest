#ifndef __PIRI_PIRI_GAME_OBJECT_H__
#define __PIRI_PIRI_GAME_OBJECT_H__

#include "CGameObject.h"

class CGridCell;
class CPiriPiriGameObject;
typedef std::function<void(CPiriPiriGameObject*)> TOnHitCallbackWithObject;

class CPiriPiriGameObject
	: public CGameObject
{
	public:
		virtual ~CPiriPiriGameObject();

		virtual void onReact();
		virtual void onDestroy();
		void setPosition(float x, float y);
		virtual void onHit(TOnHitCallbackWithObject onHitCallbackWithObject);
		virtual void updateState();

	//private:
		//this.game = game,
		bool stopsExplosion;
		bool isDestroyable;
		//this.onHit = new Signal,
		int life;
		bool hasGem;
		bool showBlast;
		bool isDead;

		int mId;
		bool aStarBlocker; // my
		CGridCell* cell; //my
		std::string mIdent;

	protected:
		CPiriPiriGameObject();
		TOnHitCallbackWithObject mOnHitCallbackWithObject;

		Sprite* mSprite;
		Sprite* mSpriteClone;
};

#endif // __PIRI_PIRI_GAME_OBJECT_H__
#include "CSpiceExplode.h"
#include <cstdlib>
#include <iostream>
#include <ctime>

CSpiceExplode::CSpiceExplode(){
	//CCLOG("bomb created %d", mBombId);
}

CSpiceExplode::~CSpiceExplode(){
	CCLOG("soice explode");
}

CSpiceExplode* CSpiceExplode::create() {
	CSpiceExplode* pRet = new CSpiceExplode();
	if (pRet && pRet->init()){
		pRet->autorelease(); //pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CSpiceExplode::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	this->mDisc = CGameObject::create("Swirl.png");
	addChild(this->mDisc, 1);
	this->mDisc->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mDisc->setAnchorPoint(Vec2(0.5f,0.5f));
	double rand = (((double) std::rand() / (double) RAND_MAX));
	this->mDisc->mToRotate = rand > .5 ? 1 : 0;
	rand = (((double) std::rand() / (double) RAND_MAX));
	this->mDisc->mSpeed.x = 2 + 10 * rand;
	rand = (((double) std::rand() / (double) RAND_MAX));
	this->mDisc->mSpeed.y = 2 + 10 * rand;
	this->mParticles.clear();
	this->mTotal = 3;
	this->setScale(.5); // 1.0
	this->mRatio = 0;

	return true;
}

void CSpiceExplode::goBoom() {
	for (unsigned int i = 0; i < this->mTotal; i++) {
		auto sprite = CGameObject::create("spiceExplosionParticle.png");
		sprite->setAnchorPoint(Vec2(.5,.5));
		sprite->setScale(3);
		this->addChild(sprite, 2);
		this->mParticles.push_back(sprite);
	}
	this->mLife = .6;
	this->mRatio = 0;
	for (unsigned int i = 0; i < this->mTotal; i++) {
		double dir = 2 * PI / this->mTotal * i;
		double rand = (((double)std::rand() / (double)RAND_MAX));
		double speed = 3 + 20 * rand;
		auto sprite = this->mParticles[i];
		sprite->mSpeed.x = sin(dir) * speed;
		sprite->mSpeed.y = cos(dir) * speed;
		sprite->setScale(10 * (((double)std::rand() / (double)RAND_MAX)));
		sprite->setVisible(true);
		sprite->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
		sprite->setPositionX(0);
		sprite->setPositionY(0);
		sprite->setOpacity(255); 
	}
	this->mDisc->setScale(1);
	this->mDisc->setOpacity(255);
	this->mDisc->setScale(.2);

	this->mDisc->runAction(EaseSineOut::create(ScaleTo::create(.7, 1.3 * 1.5, 1.3 * 1.5)));
	this->mDisc->runAction(Sequence::create(DelayTime::create(.33333),
											EaseSineOut::create(FadeTo::create(.1665, 0)),
											nullptr));
}

void CSpiceExplode::update() {
	this->mLife -= .03;
	if (this->mLife < 0) {
		if (mOnCompleteCallbackWithSEParam != nullptr)
			mOnCompleteCallbackWithSEParam(this);
		// original if(this->mLife < 0) this->onComplete.dispatch(this);
	}

	for (unsigned int i = 0; i < this->mParticles.size(); i++){ //for (unsigned int i = 0; i < this->mTotal; i++) {
		auto sprite = this->mParticles[i];
		sprite->setPositionX(sprite->getPositionX() + sprite->mSpeed.x);
		sprite->setPositionY(sprite->getPositionY() + sprite->mSpeed.y);
		sprite->setScaleX(sprite->getScaleX() * .98);
		sprite->setScaleY(sprite->getScaleY() * .98);
		sprite->setRotation(sprite->getRotation() + .1);
		if (this->mLife < .5) {
			sprite->mSpeed.x *= .9;
			sprite->mSpeed.y *= .9;
			sprite->setOpacity(sprite->getOpacity() * .8);
		}
	}
	1 == this->mDisc->mToRotate ? (
		this->mDisc->setPositionX(this->mDisc->getPositionX() + .5 * this->mDisc->mSpeed.x),
		this->mDisc->setPositionY(this->mDisc->getPositionY() + .5 * this->mDisc->mSpeed.y)
	) : (
		this->mDisc->setPositionX(this->mDisc->getPositionX() - .5 * this->mDisc->mSpeed.x),
		this->mDisc->setPositionY(this->mDisc->getPositionY() - .5 * this->mDisc->mSpeed.y)
	);
	/* need fix Swirl.png
	this->mDisc->setScaleX(this->mDisc->getScaleX() + .1); // this.disc.scale.x += .1, 
	this->mDisc->setScaleY(this->mDisc->getScaleY() + .1); 	// this.disc.scale.y += .1, */
	this->mDisc->setRotation(this->mDisc->getRotation() + .1); // this.disc.rotation += .1, 
	/*
	this->mDisc->setOpacity(this->mDisc->getOpacity() * 255 * .95); // this.disc.alpha *= .95
	*/
}

void CSpiceExplode::onComplete_addOnce(TOnCompleteCallbackWithSEParam onCompleteCallbackWithSEParam) {
	mOnCompleteCallbackWithSEParam = onCompleteCallbackWithSEParam;
}
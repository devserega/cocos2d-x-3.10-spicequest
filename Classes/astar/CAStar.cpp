#include "CAStar.h"
#include "gridworld/CGridWorld.h"
#include "gridworld/CGridCell.h"

CAStar::CAStar(CGridWorld* gridWorld) {
}

CAStar::~CAStar() {
}

void CAStar::resetGrid(CGridWorld* grid) {
	for (unsigned int i = 0; i < grid->totalCells; i++) {
		CGridCell* cell = grid->cells[i];
		cell->f = 0;
		cell->g = 0;
		cell->h = 0; 
		//cell->cost = cell->id;
		cell->visited = !1;
		cell->closed = !1;
		cell->parent = nullptr;
	}
}

// problem, can not find path from star to finish. Todo : try destroy all brick in first few seconds before Start and Finish label appear.
std::vector<CGridCell*> CAStar::search(CGridWorld* grid, CGridCell* start, CGridCell* end) {
	//CCLOG("start=%s end=%s", start->aStarBlocker ? "true" : "false", end->aStarBlocker ? "true" : "false");
	int k = 0;
	this->resetGrid(grid);
	//heuristic = heuristic || this->manhattan;
	//std::stack<CGridCell*> openHeap; //var openHeap = this.heap();
	std::list<CGridCell*> openHeap;
	std::vector<CGridCell*> ret;
	ret.clear();
	for (openHeap.push_back(start); openHeap.size() > 0;) {
		// show open heap
		//CCLOG("%d === ",k++);
		for (std::list<CGridCell*>::iterator it = openHeap.begin(); it != openHeap.end(); it++) {
			CGridCell* t = *it;
			//CCLOG("[%d,%d]", t->x, t->y);
			//if (t->x==4 && t->y == 2) {
			//	int t43 = 43;
			//}
		}

		CGridCell* currentNode = openHeap.back();
		openHeap.pop_back();
		if (currentNode == end) {
			for (CGridCell* curr = currentNode; curr->parent;) {
				ret.push_back(curr);
				curr = curr->parent;
			}
			std::reverse(ret.begin(), ret.end());

			//test
			//for (std::vector<CGridCell*>::iterator it = ret.begin(); it != ret.end(); it++) {
			//	CGridCell* t = *it;
			//	CCLOG("{%d,%d}", t->x, t->y);
			//}

			return ret;
		}

		currentNode->closed = true;
		std::vector<CGridCell*> neighbors = currentNode->neighbors;
		for (unsigned int i = 0, il = neighbors.size(); il > i; i++) {
			CGridCell* neighbor = neighbors[i];
			if (!neighbor->closed && neighbor->aStarBlocker != true) {
				float gScore = currentNode->g + neighbor->cost;
				bool beenVisited = neighbor->visited;
				if (!beenVisited || gScore < neighbor->g) {
					neighbor->visited = true;
					neighbor->parent = currentNode;
					neighbor->h = neighbor->h || manhattan(neighbor, end); //heuristic
					neighbor->g = gScore;
					neighbor->f = neighbor->g + neighbor->h;
					if (beenVisited) {
						//openHeap.rescoreElement(neighbor)
						std::list<CGridCell*>::iterator foundElement = std::find_if(	openHeap.begin(), openHeap.end(),
																						[neighbor](CGridCell* vecElement) {
																							return vecElement == neighbor; 
																						});
						if(foundElement!= openHeap.end())
							openHeap.erase(foundElement);
					}
					else {
						openHeap.push_back(neighbor);
					}
				}
			}
		}
		int t1 = 212;
	}
	return ret;
}

/*
AStar.prototype.heap = function() {
	return new BinaryHeap(function(node) {
		return node.f
	})
}*/

float CAStar::manhattan(CGridCell* pos0, CGridCell* pos1) {
	float d1 = abs(pos1->x - pos0->x), d2 = abs(pos1->y - pos0->y);
	return d1 + d2;
}
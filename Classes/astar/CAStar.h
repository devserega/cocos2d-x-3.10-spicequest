#ifndef __ASTAR_H__
#define __ASTAR_H__

#include "cocos2d.h"

using namespace cocos2d;

class CGridWorld;
class CGridCell;
class CAStar
	//: public cocos2d::Ref
{
	public:
		CAStar(CGridWorld* gridWorld);
		virtual ~CAStar();
		void resetGrid(CGridWorld* gridWorld);
		std::vector<CGridCell*> search(CGridWorld* gridWorld, CGridCell* startCell, CGridCell* endCell);
		float manhattan(CGridCell* pos0, CGridCell* pos1);
};

#endif // __ASTAR_H__

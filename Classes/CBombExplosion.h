#ifndef __BOMBEXPLOSION_H__
#define __BOMBEXPLOSION_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

using namespace cocos2d;

class CExplosionManager;
class CPiriPiriGridWorld;
class CBomb;
class CGridCell;
class CBombExplosion
	: public CPiriPiriGameObject
{
	public:
		virtual ~CBombExplosion();
		static CBombExplosion* create(CExplosionManager* explosionManager);
		virtual bool init(CExplosionManager* explosionManager);

		void update();
		void complete();
		void reset(CPiriPiriGridWorld* gridWorld, CBomb* bomb, CExplosionManager* explosionManager);
		void start();

		//private:
		CBombExplosion();

		int mStrength;
		float mExplosionLevel;
		CPiriPiriGridWorld* mGridWorld;
		CExplosionManager* mExplosionManager;
		//CBomb* mBomb;
		float mCountdown;
		float mCountdownTimer;
		// this.onComplete = new Signal

		CGridCell* mStartCell;
		std::vector<std::string> mToProcess;
		std::map<std::string, CGridCell*> mSDirection;

	private:
	//	Sprite* mFlashLight;
	//	Sprite* mClone;
		std::string mIdent; // my
	
};

#endif // __BOMBEXPLOSION_H__

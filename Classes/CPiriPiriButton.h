#ifndef __PIRIPIRIBUTTON_H__
#define __PIRIPIRIBUTTON_H__

// not finished

#include "cocos2d.h"
//#include "ui/UIWidget.h"
//#include "ui/GUIExport.h"

//#define CPiriPiriButton_DEBUG

using namespace cocos2d;

class CPiriPiriButton;
typedef std::function<void(CPiriPiriButton*)> TOnPressCallbackWithButtonParam;
typedef std::function<void()> TOnPressCallback;

class CPiriPiriButton
	: public cocos2d::Node
	//, public ui::Widget
{
	public:
		virtual bool init(const std::string& icon, bool wide);
		static CPiriPiriButton* create(const std::string& icon, bool wide = false);
		void onPress(TOnPressCallbackWithButtonParam onPressCallback);
		void onPress(TOnPressCallback onPressCallback);
		void enable();
		void disable();
		virtual void onMouseOver();
		virtual void onMouseOut();
		void setIcon(const std::string& filename);

		bool  mInteractive;

	protected:
		bool myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point);
		CPiriPiriButton();
		void onEnter();
		void onExit();
		void addListener();
		void removeListener();

		EventListenerTouchOneByOne* mListener;
		TOnPressCallbackWithButtonParam mOnPressCallbackWithButtonParam;
		TOnPressCallback mOnPressCallback;
		Sprite* mBg;
		Sprite* mIcon;
		bool mEnabled;
		bool mWide;
		bool mDown;

		float mWidth;
		float mHeight;
		float mOffsetY;
		float mIconOriginalY;
		float mRScale;
		float mTScale;
};

#endif // __PIRIPIRIBUTTON_H__

#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__

#include "cocos2d.h"

using namespace cocos2d;

class CGameObject
	: public cocos2d::Sprite
{
	public:
		static CGameObject* create(const std::string& filename);
		virtual bool init(const std::string& filename);
		virtual ~CGameObject();
		virtual void remove();

	//private:
		Vec2 mPosition;
		Vec2 mSpeed;
		int mWidth;
		int mHeight;
		int mToRotate;
		//this.world = null

	protected:
		CGameObject();
};

#endif // __GAME_OBJECT_H__
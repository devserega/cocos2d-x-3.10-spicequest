#ifndef __CPIRI_PIRI_GRID_WORLD_H__
#define __CPIRI_PIRI_GRID_WORLD_H__

#include "cocos2d.h"
#include "Constants.h"
#include "CLevels.h"
#include "CGridWorld.h"

using namespace cocos2d;

class CPiriPiriBlastGame;
class CGridCell;
class CPiriPiriGridWorld
	: public CGridWorld
{
	public:
		virtual bool init(CPiriPiriBlastGame* game);
		static CPiriPiriGridWorld* create(CPiriPiriBlastGame* game);
		void loadMap(TMap& map);
		CGridCell* getStartCell();
		CGridCell* getEndCell();
		void update();
		void resetMap();
		void setCell(CPiriPiriGameObject* cellType, unsigned int xId, unsigned int yId, bool aStarBlocker = false); //my
		void onHit(CPiriPiriGameObject* object);


		CPiriPiriBlastGame* mGame;

	protected:
		CPiriPiriGridWorld();
		virtual ~CPiriPiriGridWorld();

		bool onTouchesBegan(Touch* touch, Event* e);
		void onTouchesMoved(Touch* touch, Event* e);
		void onTouchesEnded(Touch* touch, Event* e);

		CPiriPiriGameObject* mStartCell;
		CPiriPiriGameObject* mEndCell;
};

#endif // __CPIRI_PIRI_GRID_WORLD_H__

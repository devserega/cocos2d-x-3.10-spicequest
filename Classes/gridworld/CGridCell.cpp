#include "CGridCell.h"

CGridCell::CGridCell()
	: id("")
	, f(0)
	, g(0)
	, h(0)
	, cost(0.1f)
	, visited(false)
	, closed(false)
	, aStarBlocker(false)
	, item(nullptr)
	//, up(nullptr)
	//, down(nullptr)
	//, left(nullptr)
	//, right(nullptr)
{

}

CGridCell::~CGridCell() {

}
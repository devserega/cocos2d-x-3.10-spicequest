#include "CGridWorld.h"

CGridWorld::CGridWorld()
	: cellSize(80) {
	astar = new CAStar(this);
}

CGridWorld::~CGridWorld() {
}

void CGridWorld::build(TMap* map) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	TMap* tempMap = map;
	this->cellWidth = 12;
	this->cellHeight = 6;
	this->totalCells = cellWidth * cellHeight;
	if (tempMap==nullptr) {
		tempMap = new TMap();
		for (unsigned int i = 0; i < this->totalCells; i++) {
			(*tempMap).push_back(std::string(""));
		}
		//tempMap->reserve(this->totalCells);
	}

	if (tempMap->size() == totalCells) {
		cells.clear();
		cells.resize(totalCells);
		for (unsigned int i = 0; i < totalCells; i++) {
			cells.insert(cells.begin()+i, new CGridCell());
		}
		for (unsigned int i = 0; i < totalCells; i++) {
			CGridCell* cell = cells[i];
			unsigned int x = i % cellWidth;
			unsigned int y = i / cellWidth | 0;
			cell->x = x;
			cell->y = y;
			cell->realX = x * cellSize;
			cell->realY = (visibleSize.height - cellSize) - y * cellSize;
			cell->centerX = cell->realX + cellSize / 2;
			cell->centerY = cell->realY + cellSize / 2;
			cell->id = (*tempMap)[i];
			cell->owner = this;
			cell->neighbors.clear();

			if (x != 0) {
				cell->mSDirection["left"] = cells[i - 1];
				cell->neighbors.push_back(cell->mSDirection["left"]);
			}
			if (x != cellWidth - 1) {
				cell->mSDirection["right"] = cells[i + 1];
				cell->neighbors.push_back(cell->mSDirection["right"]);
			}
			if (y != cellHeight - 1) {
				cell->mSDirection["down"] = cells[i + cellWidth];
				cell->neighbors.push_back(cell->mSDirection["down"]);
			}
			if (0 != y) {
				cell->mSDirection["up"] = cells[i - cellWidth];
				cell->neighbors.push_back(cell->mSDirection["up"]);
			}
			if (y != cellHeight - 1) {
				cell->mSDirection["down"] = cells[i + cellWidth];
				cell->neighbors.push_back(cell->mSDirection["down"]);
			}
		}
	}
}

CGridCell* CGridWorld::getCell(unsigned int x, unsigned int y) {
	unsigned int xId = x / cellSize | 0;
	unsigned int yId = y / cellSize | 0;
	return cells[yId * cellWidth + xId];
}

void CGridWorld::setCell(CPiriPiriGameObject* cellType, unsigned int xId, unsigned int yId) {
	CGridCell* cell = cells[yId * cellWidth + xId];

	if (cell->item!=nullptr) {
		// serega [begin]
		CPiriPiriGameObject* ci = cell->item;
		std::vector<CPiriPiriGameObject*>::iterator foundElement =
			std::find_if(myCells.begin(), myCells.end(), [ci](CPiriPiriGameObject* vecElement) {return vecElement == ci; });
		if (foundElement != myCells.end()) {
			//myCells.erase(foundElement); //!!!
			*foundElement = nullptr;
		}
		// serega [end]

		cell->item->remove(); //removeChild(cell->item); //this.view.removeChild(cell.item.view);
		cell->item->cell = nullptr;
	}

	if (cellType != nullptr) {
		if (cell->x == 5 && cell->y == 2) {
			CCLOG("CGridWorld::setCell x=%d y=%d aStarBlocker=%d", cell->x, cell->y, cellType->aStarBlocker);
		}

		cell->aStarBlocker = cellType->aStarBlocker;
		cell->hasGem = cellType->hasGem;
		cellType->cell = cell;
		cell->item = cellType;
		cell->item->setPosition(cell->centerX, cell->centerY);
		this->addChild(cell->item,5);
	}
	else {
		cell->item = nullptr;
	}
}

void CGridWorld::setCellFromPoint(CPiriPiriGameObject* cellType, unsigned int x, unsigned int y) {
	unsigned int xId = x / cellSize | 0;
	unsigned int yId = y / cellSize | 0;
	setCell(cellType, xId, yId);
}

void CGridWorld::removeObject(CPiriPiriGameObject* object) {
	if (object->cell) {
		CGridCell* cell = object->cell;
		// my [begin]
		// at first remove object from myCells
		std::vector<CPiriPiriGameObject*>::iterator foundElement =
			std::find_if(myCells.begin(), myCells.end(), [object](CPiriPiriGameObject* vecElement) {return vecElement == object; });
		if (foundElement!= myCells.end()) {
			//myCells.erase(foundElement); //!!!
			*foundElement = nullptr;
		}
		// my [end]

		object->remove();//removeChild(object); //this.view.removeChild(object.view),
		cell->item = nullptr;
		cell->aStarBlocker = false;

		if (cell->x == 2 && cell->y == 5) {
			CCLOG("CGridWorld::removeObject x=%d y=%d aStarBlocker=%d", cell->x, cell->y, cell->aStarBlocker);
		}

		object->cell = nullptr;
	}
}
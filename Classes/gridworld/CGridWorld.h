#ifndef __GRID_WORLD_H__
#define __GRID_WORLD_H__

#include "cocos2d.h"
#include "astar/CAStar.h"
#include "CPiriPiriGameObject.h"
#include "gridworld/CGridCell.h"
#include "CLevels.h"

class CGridWorld 
	: public cocos2d::Layer {
	public:
		CGridWorld();
		virtual ~CGridWorld();

		void build(TMap* map = nullptr);
		CGridCell* getCell(unsigned int x, unsigned int y);
		void setCell(CPiriPiriGameObject* cellType, unsigned int xId, unsigned int yId);
		void setCellFromPoint(CPiriPiriGameObject* cellType, unsigned int x, unsigned int y);
		void removeObject(CPiriPiriGameObject* piriPiriObject);

	//protected:
		unsigned int cellSize;
		std::vector<CGridCell*> cells;
		unsigned int cellWidth;
		unsigned int cellHeight;
		unsigned int totalCells;
		//this.view = new PIXI.DisplayObjectContainer,
		CAStar* astar;
		std::vector<CPiriPiriGameObject*> myCells;
};

#endif // __GRID_WORLD_H__



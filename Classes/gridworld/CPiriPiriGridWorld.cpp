#include "CPiriPiriGridWorld.h"
#include "blocks/CIndestructable.h"
#include "blocks/CStartBlock.h"
#include "blocks/CEndBlock.h"
#include "blocks/CDestructable.h"
#include "blocks/CDoubleBlock.h"
#include "blocks/CSoftBlock.h"
#include "blocks/CDoubleBlock.h"
#include "blocks/CEmptyBlock.h"
#include "blocks/CBarrel.h"
#include "blocks/CCrackBlock.h"
#include "blocks/CUpDownBlock.h"
#include "CBombManager.h"
#include "CPiriPiriBlastGame.h"
#include "CHud.h"
#include "gridworld/CGridCell.h"

CPiriPiriGridWorld::CPiriPiriGridWorld(){
}

CPiriPiriGridWorld::~CPiriPiriGridWorld() {
}

bool CPiriPiriGridWorld::init(CPiriPiriBlastGame* game) {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);
	this->setPosition(OFFSETS::x, -OFFSETS::y);

	cellSize = 90;
	myCells.clear();
	mStartCell = nullptr;
	mEndCell = nullptr;
	mGame = game;
	// this.cellClasses = [CellEmpty, CellDestructable, CellIndestructable, CellBarrel, CellBomb, CellCrack, CellDoubleHit, CellStart, CellEnd, CellUpDown, CellSoft]

	//  make layer touchable
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(CPiriPiriGridWorld::onTouchesBegan, this); 
	listener->onTouchMoved = CC_CALLBACK_2(CPiriPiriGridWorld::onTouchesMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(CPiriPiriGridWorld::onTouchesEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

CPiriPiriGridWorld* CPiriPiriGridWorld::create(CPiriPiriBlastGame* game) {
	CPiriPiriGridWorld* pRet = new(std::nothrow) CPiriPiriGridWorld();
	if (pRet && pRet->init(game)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

void CPiriPiriGridWorld::loadMap(TMap& map) {
	unsigned int gems = 0;
	this->resetMap();
	for (unsigned int i = 0; i < map.size(); i++) {
		std::string mapID = map[i];
		bool params = false;
		unsigned int x = i % this->cellWidth;
		unsigned int y = i / this->cellWidth | 0;

		/*
		if ("string" == typeof mapID) {
			var params = mapID.split("-");
			mapID = parseInt(params[0]), params.shift()
		}*/
		std::vector<std::string> tempParams = split(mapID, '-');
		CPiriPiriGameObject* object = nullptr; //var object = new this.cellClasses[mapID](params)
		if (tempParams[0].compare("0")==0) { // CellEmpty
			object = CEmptyBlock::create();
		}
		else if (tempParams[0].compare("1") == 0) { // CellDestructable
			object = CDestructable::create();
		}
		else if (tempParams[0].compare("2") == 0) { // CellIndestructable
			object = CIndestructable::create();
		}
		else if (tempParams[0].compare("3") == 0) { // CellBarrel
			object = CBarrel::create(tempParams, this->mGame->mBombManager);
		}
		else if (tempParams[0].compare("4") == 0) { // CellBomb

		}
		else if (tempParams[0].compare("5") == 0) { // CellCrack
			object = CCrackBlock::create();
		}
		else if (tempParams[0].compare("6") == 0) { // CellDoubleHit
			object = CDoubleBlock::create();
		}
		else if (tempParams[0].compare("7") == 0) { // CellStart
			object = CStartBlock::create();
		}
		else if (tempParams[0].compare("8") == 0) { // CellEnd
			object = CEndBlock::create();
		}
		else if (tempParams[0].compare("9") == 0) { // CellUpDown
			object = CUpDownBlock::create(tempParams);
		}
		else if (tempParams[0].compare("10") == 0) { // CellSoft
			object = CSoftBlock::create();
		}

		if (tempParams.size() == 2 && tempParams[1].compare("*") == 0) {
			object->hasGem = true;
			gems++;
			if (mapID.compare("0")==0) {
				this->mGame->mHud->showGem(x * this->cellSize + .5 * this->cellSize, y * this->cellSize + .5 * this->cellSize);
			}
		}
		
		//for (var object = new this.cellClasses[mapID](params), j = 0; j < params.length; j++) 
		//	"*" == = params[j] && (object.hasGem = !0, gems++, 0 == = mapID && this.game.hud.showGem(x * this.cellSize + .5 * this.cellSize, y * this.cellSize + .5 * this.cellSize));

		if (object!=nullptr) { //temp, remove
			if (object->mId == BLOCKS::BARREL) {
				this->mGame->mBombManager->addBarrel((CBarrel*) object, x, y);
			}
			else {
				switch (object->mId) {
				case BLOCKS::START:
					this->mStartCell = object;
					break;
				case BLOCKS::END:
					this->mEndCell = object;
					break;
				}
				// my 
				this->myCells.push_back(object);
				object->onHit((TOnHitCallbackWithObject)std::bind(&CPiriPiriGridWorld::onHit, this, std::placeholders::_1));
				this->setCell(object, x, y);
			}
		}
	}
	this->mGame->gemsToGet = gems;
	this->mGame->mHud->updateGems();
}

CGridCell* CPiriPiriGridWorld::getStartCell() {
	return this->mStartCell->cell;
}

CGridCell* CPiriPiriGridWorld::getEndCell() {
	return this->mEndCell->cell;
}

void CPiriPiriGridWorld::update() {
	CPiriPiriGameObject* cell = nullptr;
	for (unsigned int i = this->myCells.size(); i--;) {
		cell = this->myCells[i];
		if (cell!=nullptr) { // my_need_fix  && cell->mId == 9
			cell->updateState();
		}

		//if (cell!=nullptr) { // my
		//	cell->updateState();
		//}
		
		//"function" == typeof cell.updateState && cell.updateState()
	}
}

void CPiriPiriGridWorld::resetMap() {}

void CPiriPiriGridWorld::setCell(CPiriPiriGameObject* cellType, unsigned int xId, unsigned int yId, bool aStarBlocker) {
	CGridWorld::setCell(cellType, xId, yId); //original GridWorld.prototype.setCell.call(this, cellType, xId, yId, aStarBlocker)
}

bool CPiriPiriGridWorld::onTouchesBegan(Touch* touch, Event* e) {
	if (!mGame->mController->isInteractive())
		return false;

	auto target = static_cast<Layer*>(e->getCurrentTarget());

	Point locationInNode = target->convertToNodeSpace(touch->getLocation());
	//log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
	Size s = target->getContentSize();
	Rect rect = Rect(0, 0, s.width, s.height);

	if (rect.containsPoint(locationInNode)){
		//log("sprite began2... x = %f, y = %f", touch->getLocation().x, touch->getLocation().y);
		CGridCell* targetCell = getCell(touch->getLocation().x, s.height - touch->getLocation().y - cellSize);
		if (targetCell != nullptr) {
			CCLOG("X touchLocation.x=%f touchLocation.y=%f targetCell=[%d,%d]", touch->getLocation().x, s.height - touch->getLocation().y - cellSize, targetCell->x, targetCell->y);
			if ((targetCell->item == nullptr || 0 == targetCell->item->mId) && mGame->bombs > 0) {
				CCLOG("add");
				this->mGame->mBombManager->add(touch->getLocation().x, s.height - touch->getLocation().y - cellSize);
			}
		}
		return true;
	}
	return false;
}

void CPiriPiriGridWorld::onTouchesMoved(Touch* touch, Event* e) {
	//auto touchLocation = touches[0]->getLocation();
}

void CPiriPiriGridWorld::onTouchesEnded(Touch* touch, Event* e) {
	/*
	auto target = static_cast<Layer*>(e->getCurrentTarget());
	Point locationInNode = touch->getLocation();
	log("sprite began_end... x = %f, y = %f", locationInNode.x, locationInNode.y);
	Size s = target->getContentSize();
	CGridCell* targetCell = getCell(locationInNode.x, s.height - locationInNode.y - cellSize);
	if (targetCell != nullptr) {
		//CCLOG("2XXXX2 %d %d", targetCell->x, targetCell->y);
		if ((targetCell->item == nullptr || 0 == targetCell->item->id) ) {//&& mGame->bombs > 0
			//this->mGame->mBombManager->add(locationInNode.x, s.height - locationInNode.y - cellSize);
		}
	}*/
}

void CPiriPiriGridWorld::onHit(CPiriPiriGameObject* object) {
	switch (object->mId) {
		case 0:
			break;
		case BLOCKS::DESTRUCTABLE:
			if(object->hasGem == true) this->mGame->mHud->showGem(object->cell->centerX, object->cell->centerY);
			removeObject(object);
			break;
		case 2:
			break;
		case BLOCKS::BARREL:
			if (object->hasGem == true) this->mGame->mHud->showGem(object->cell->centerX, object->cell->centerY);
			object->onReact();
			break;
		case BLOCKS::DOUBLE_HIT: {
			CDoubleBlock* doubleBlock = dynamic_cast<CDoubleBlock*>(object);
			if (doubleBlock) {
				doubleBlock->takeLife();
				if (doubleBlock->isAlive() == false) {
					if (doubleBlock->hasGem == true) this->mGame->mHud->showGem(object->cell->centerX, object->cell->centerY);
					removeObject(object);
				}
			}
			break;
		}
		case BLOCKS::SOFT:
			if (object->hasGem == true) this->mGame->mHud->showGem(object->cell->centerX, object->cell->centerY);
			removeObject(object);
	}
}
#ifndef __GRID_CELL_H__
#define __GRID_CELL_H__

#include "cocos2d.h"

using namespace cocos2d;

class CGridWorld;
class CPiriPiriGameObject;
class CGridCell
	: public cocos2d::Ref
{
	public:
		CGridCell();
		virtual ~CGridCell();

		std::string id;
		CGridWorld* owner;
		//this.world = world,
		//CGridCell* up;
		//CGridCell* down;
		//CGridCell* left;
		//CGridCell* right;
		std::map<std::string, CGridCell*> mSDirection;
		std::vector<CGridCell*> neighbors;
		int x; 
		int y;
		unsigned int centerX;
		unsigned int centerY;
		unsigned int realX;
		unsigned int realY;
		//this.nextCell,
		int f;
		int g;
		int h;
		float cost;
		bool visited;
		bool closed;
		CGridCell* parent;
		bool aStarBlocker;
		CPiriPiriGameObject* item; // my
		bool hasGem; // my
};

#endif // __GRID_CELL_H__



#ifndef __TRACKING_H__
#define __TRACKING_H__

#include "cocos2d.h"
#include "Constants.h"

using namespace cocos2d;

class CTracking
	: public cocos2d::Ref
{
	public:
		static CTracking* getInstance() {
			if (!instanceFlag) {
				single = new CTracking();
				if (single && single->init()) {
					instanceFlag = true;
					return single;
				}
				CC_SAFE_DELETE(single);
				return nullptr;
			}
			else {
				return single;
			}
		}

		virtual ~CTracking() {
			CCLOG("Destroy CTracking singleton");
			instanceFlag = false;
			single = NULL;
		}

		virtual void trackEvent(const std::string& eventName);
		virtual void trackEvent(const std::string& eventName, unsigned int value);
		virtual void trackEvent(const std::string& eventName, const std::string& value);

	private:
		CTracking();
		virtual bool init();

		static bool instanceFlag;
		static CTracking* single;
};

#endif // __TRACKING_H__

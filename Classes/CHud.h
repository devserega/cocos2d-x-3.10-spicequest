#ifndef __CHUD_H__
#define __CHUD_H__

#include "cocos2d.h"
#include "Constants.h"

using namespace cocos2d;

class CPiriPiriBlastGame;
class CSpiceExplode;
class CGem;
class CHud
	: public cocos2d::Layer
{
	public:
		static CHud* create(CPiriPiriBlastGame* game);
		virtual bool init();

		void onPausePressed();
		void reset();
		void updateBombs();
		void updateGems();
		void updateScore(/*score, target*/);
		void onDetailFinish(/*detailText*/);
		void update();
		void showGem(unsigned int x, unsigned int y);
		void clearGem(unsigned int x, unsigned int y);
		void gemFinish(CGem* gem, unsigned int x, unsigned int y);
		void showSpiceExplode(unsigned int x, unsigned int y);
		void onSpiceExplodeFinish(CSpiceExplode* spiceExplosion);
		void showLevelComplete();
		void showGameover();
		void onGameoverComplete();
		void onLevelComplete();
		void showStartAndFinish();
		void resize(unsigned int w, unsigned int h);

		std::vector<CSpiceExplode*> mActiveSpiceExplodes;

	protected:
		CHud();
		virtual ~CHud();

		CPiriPiriBlastGame* mGame;
		Sprite* mLevelLabel;
		Label* mLevelNumber;
		Label* mLives;
		Sprite* mHudGem;
		Label* mGems;
		Label* mBombs;
		Sprite* mHudBomb;
		Sprite* mStartArrow;
		Sprite* mEndArrow;
		std::map<std::string, CGem*> mActiveGems;
};

#endif // __CHUD_H__

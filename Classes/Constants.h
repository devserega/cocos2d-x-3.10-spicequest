#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "cocos2d.h"

using namespace cocos2d;

enum LEVEL_STATES {
	LOCKED = 0,
	UNLOCKED = 1,
	COMPLETE = 2,
	COMPLETE_WITH_GEMS = 3
};

#define BOMB_BLAST_RADIUS 2
	
#define FLOOROFFSET_x -82
#define FLOOROFFSET_y -12

enum OFFSETS {
	x = 28,
	y = 100
};

enum BLOCKS {
	EMPTY = 0,
	DESTRUCTABLE = 1,
	INDESTRUCTABLE = 2,
	BARREL = 3,
	BOMB = 4,
	CRACKED = 5,
	DOUBLE_HIT = 6,
	START = 7,
	END = 8,
	UP_DOWN = 9,
	SOFT = 10
};

enum BARREL {
	blastRadiusA = 3,
	blastRadiusB = 5
};

/*
UPDOWNPATTERNS: {
	A: [1, 0, 0, 0, 0, 0, 0],
	B : [1, 1, 1, 0, 0, 0, 0, 0],
	C : [0, 1, 1, 1, 0, 0, 0, 0],
	D : [0, 0, 1, 1, 1, 0, 0, 0],
	E : [0, 0, 0, 0, 0, 0, 1, 1],
	F : [0, 0, 0, 0, 1, 1, 1, 1],
	G : [0, 0, 0, 1, 1, 1, 1, 1],
	H : [1, 1, 0, 0, 0, 0, 1, 1]
},*/

//extern std::map<std::string, int> UPDOWNPATTERNS;
//UPDOWNPATTERNS[std::string("0")] = 5;// { 1, 0, 0, 0, 0, 0, 0 };

enum AXIS {
	NONE = 0,
	HORIZONTAL = 1,
	VERTICAL = 2,
	BOTH = 3
};

// You could also take an existing vector as a parameter.
std::vector<std::string> split(std::string str, char delimiter);

//typedef std::function<void()> ccListnerCallback;
//typedef void (Ref::*SEL_CallFunc)();
//typedef void static_cast<cocos2d::SEL_SCHEDULE>(&_SELECTOR);

class CGridCell;
typedef void (cocos2d::Ref::*SEL_CallFunc_onStepsComplete)(std::vector<CGridCell*> path, CGridCell* startCell, CGridCell* endCell, float delay);
#define CC_CALLFUNC_ON_STEPS_COMPLETE_SELECTOR(_SELECTOR) static_cast<SEL_CallFunc_onStepsComplete>(&_SELECTOR)
// Deprecated
#define callfunc_onStepsComplete_selector(_SELECTOR) CC_CALLFUNC_ON_STEPS_COMPLETE_SELECTOR(_SELECTOR)

typedef void (cocos2d::Ref::*SEL_CallFunc_onAssetsLoaded)();
#define CC_CALLFUNC_ON_ASSETS_LOADED_SELECTOR(_SELECTOR) static_cast<SEL_CallFunc_onAssetsLoaded>(&_SELECTOR)
// Deprecated
#define callfunc_onAssetsLoaded_selector(_SELECTOR) CC_CALLFUNC_ON_ASSETS_LOADED_SELECTOR(_SELECTOR)

//typedef void (cocos2d::Ref::*SEL_CallFunc_onClick)();
//#define CC_CALLFUNC_ON_CLICK_SELECTOR(_SELECTOR) static_cast<SEL_CallFunc_onClick>(&_SELECTOR)
// Deprecated
//#define callfunc_onClick_selector(_SELECTOR) CC_CALLFUNC_ON_CLICK_SELECTOR(_SELECTOR)

typedef std::function<void()> TOnCallback;
typedef std::function<void(bool)> TOnCallbackWithBool;

#define PI 3.14159265358979323846  /* pi */ 
#define my_random() ((double) std::rand() / (RAND_MAX))

#endif // __CONSTANTS_H__
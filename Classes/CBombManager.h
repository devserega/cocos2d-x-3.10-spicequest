#ifndef __BOMBMANAGER_H__
#define __BOMBMANAGER_H__

#include "cocos2d.h"

class CPiriPiriBlastGame;
class CBomb;
class CBarrel;
class CBombManager
	: public cocos2d::Ref
{
	public:
		static CBombManager* create(CPiriPiriBlastGame* game);
		virtual bool init();

		void add(unsigned int x, unsigned int y);
		void reset();
		void clearBombs();
		void addBarrel(CBarrel* barrel, unsigned int x, unsigned int y);
		void update();
		bool isActive();
		void onExplode(CBomb* bomb);
		void onExplodeBarrel(CBomb* barrel);

	//protected:
		CBombManager();
		virtual ~CBombManager();

		CPiriPiriBlastGame* mGame;
		std::vector<CBomb*> mBombs;
		std::vector<CBomb*> mBarrels;
		unsigned int mBombCount;
};

#endif // __BOMBMANAGER_H__


#ifndef __CSIGNAL_H__
#define __CSIGNAL_H__

#include "cocos2d.h"

using namespace cocos2d;

class CSignal
	: public cocos2d::Ref
{
	public:
		static CSignal* create();
		virtual bool init();
		virtual ~CSignal();

	protected:
		CSignal();

		std::string mVERSION;
		bool mMemorize;
		bool mShouldPropagate;
		bool mActive;

		//std::vector<> mBindings;
		//this._bindings = [],
		//this._prevParams = null;
		CSignal* mSelf;
		//this.dispatch = function() {
		//	Signal.prototype.dispatch.apply(self, arguments)
		//}
};

#endif // __CSIGNAL_H__
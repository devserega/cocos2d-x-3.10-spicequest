#ifndef __CLOSE_BUTTON_H__
#define __CLOSE_BUTTON_H__

#include "cocos2d.h"
#include "CPiriPiriButton.h"

using namespace cocos2d;

class CCloseButton
	: public cocos2d::Node
{
	public:
		virtual bool init();
		static CCloseButton* create();
		void onPress(TOnPressCallback onPressCloseCallback);
		virtual ~CCloseButton();

	protected:
		CCloseButton();
		bool myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point);
		void addEventListener();
		void removeEventListener();
		EventListenerTouchOneByOne* mListener;
		Sprite* mBg;
		TOnPressCallback mOnPressCloseCallback;
};

#endif // __CLOSE_BUTTON_H__
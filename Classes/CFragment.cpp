#include "CFragment.h"

CFragment::CFragment()
	: mRotationRatio(-0.05)
	, mRotation(0)
	, mAlpha(1)
	, mScaleX(1)
	, mScaleY(1){
}

CFragment::~CFragment() {
}

CFragment* CFragment::create() {
	CFragment* pRet = new(std::nothrow) CFragment();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CFragment::init() {
	if (!cocos2d::Sprite::initWithSpriteFrameName("ExplosionA.png")) {
		return false;
	}

	setAnchorPoint(Vec2(.5,.5));
	mIsDead = false;
	double rnd = ((double) std::rand() / (RAND_MAX));
	mRotationRatio = rnd < .5 ? -.05 : .05;
	return true;
}

void CFragment::reset(std::string& ident) {
	setScale(.4);
	setOpacity(255);
	mIsDead = false;
	if (ident.compare("") == 0) ident = "B";
	setSpriteFrame("Explosion" + ident + ".png");
	//setVisible(false); // test
}

// ������� ������� FragmentAction

void CFragment::update() {
	this->mRotation += this->mRotationRatio*20;
	if (mScaleX > 3 && mAlpha < .1) {
		mIsDead = true;
	}
	else if(mScaleX > 1){
		mAlpha *= .85;
		mScaleX += .05;
		mScaleY += .05;
	}
	else {
		mScaleX += .2;
		mScaleY += .2; 
		mAlpha *= .99;
	}
	setScaleX(mScaleX);
	setScaleY(mScaleY);
	setOpacity(mAlpha*255);
	setRotation(this->mRotation);
}
#ifndef __PLAYBUTTON_H__
#define __PLAYBUTTON_H__

#include "cocos2d.h"
#include "CPiriPiriButton.h"

using namespace cocos2d;

class CPlayButton
	: public CPiriPiriButton
{
	public:
		virtual bool init();
		static CPlayButton* create();

		void onTouchBegan();
		void onTouchEnded();

		void onMouseOver() override;
		void onMouseOut() override;

	private:
		Sprite* mPlaySprite;
};

#endif // __PLAYBUTTON_H__
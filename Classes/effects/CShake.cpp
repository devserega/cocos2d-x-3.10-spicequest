#include "CShake.h"

CShake* CShake::create() {
	CShake* pRet = new CShake();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CShake::init() {
	runOnce = true;
	return true;
}

CShake::~CShake() {

}

CShake::CShake(){

}

void CShake::start(float intensity, float duration, int direction) {
	/*
	this.intensity = parameters.intensity || .01,
		this.duration = parameters.duration || 1e3,
		this.direction = parameters.direction || Constants.AXIS.BOTH,
		this.offset.x = this.offset.y = 0,
		this.camera = parameters.camera
	*/
}

void CShake::addDuration(float duration) {
	//this.duration < duration && (this.duration += duration - this.duration)
}

void CShake::end() {
	/*
			this.done = !0,
				this.camera.pivot.x = 0,
				this.camera.pivot.y = 0,
				this.onComplete.dispatch(this, "shake")
				*/
}

void CShake::update() {
	/*
				this.done || (
					this.duration -= Ticker.instance.deltaTime,
					this.offset.x = -this.offset.x,
					this.offset.y = -this.offset.y,
					this.camera.pivot.x = this.offset.x,
					this.camera.pivot.y = this.offset.y,
					this.duration <= 0 ? this.end() : (
						this.gameWidth = 1366,
						this.gameHeight = 640,
						this.direction && Constants.AXIS.HORIZONTAL && (this.offset.x = Math.round(Math.random() * this.intensity * this.gameWidth * 2 - this.intensity * this.gameWidth)),
						this.direction && Constants.AXIS.VERTICAL && (this.offset.y = Math.round(Math.random() * this.intensity * this.gameHeight * 2 - this.intensity * this.gameHeight)),
						this.camera.pivot.x = this.offset.x,
						this.camera.pivot.y = this.offset.y
						)
					)
		*/
}

void CShake::reset() {
	this->duration = 0;
	this->offset_x = 0;
	this->offset_y = 0;
}
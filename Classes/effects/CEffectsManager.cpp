#include "CEffectsManager.h"
#include "CPiriPiriBlastGame.h"
#include "CEffect.h"
#include "CShake.h"
#include "CSunburst.h"
#include "actions/CShakeAction.h"

CEffectsManager* CEffectsManager::create(CPiriPiriBlastGame* game) {
	CEffectsManager* pRet = new(std::nothrow) CEffectsManager();
	if (pRet && pRet->init(game)) {
		pRet->retain();  //pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CEffectsManager::init(CPiriPiriBlastGame* game) {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	mGame = game;

	return true;
}

CEffectsManager::~CEffectsManager() {

}

CEffectsManager::CEffectsManager() {

}

void CEffectsManager::_do(std::string& eventName, float intensity, float duration, int direction) {
	/* original
	// parameters.camera = this.camera,
	CEffect* effectObject = this->_isEffectRunning(eventName);
	if (effectObject != nullptr && effectObject->runOnce == true) {
		effectObject->addDuration(duration);
	}
	else {
		// var effect = this.fxPool[event].pool.getObject();
		CEffect* effect = nullptr;
		if (eventName.compare("shake") == 0) {
			effect = CShake::create();
		}
		else if (eventName.compare("sunburst") == 0) {
			//effect = CSunburst::create();
		}
		//effect->reset(); //this.cameraOriginalPosition
		effect->start(intensity, duration, direction);
		//effect.onComplete.add(this.onEffectComplete, this, event),
		//effect.hasOwnProperty("view") && (this.view.addChild(effect.view),
		effect->setPositionX(50);
		effect->setPositionY(-180);
		effect->setScale(5);
		effect->setOpacity(76);
		//effect.sprite.blendMode = PIXI.blendModes.ADD),
		effects[eventName] = effect;
	}
	*/
	this->mGame->runAction(CShakeAction::actionWithDuration(0.2f, .01, direction));
	//node_to_shake->runAction(CCShake::actionWithDuration(5.0f, 10.0f));
}

CEffect* CEffectsManager::_isEffectRunning(std::string& type) {
	for (std::map<std::string, CEffect*>::iterator it = effects.begin(); it != effects.end(); it++) {
		if (it->second->name == type) return it->second;
	}
	return nullptr;
}

void CEffectsManager::onEffectComplete() { // effect, event
	/*
	this.effects.splice(this.effects.indexOf(effect), 1),
		this.fxPool[event].pool.returnObject()
		*/
}

void CEffectsManager::reset() {
	this->effects.clear();
	/*
	for (var i = this.effects.length; i--;)
		this.effects.splice(i, 1), this.fxPool.shake.pool.returnObject()
		*/
}

void CEffectsManager::update() {
	for (std::map<std::string, CEffect*>::iterator it = effects.begin(); it != effects.end(); it++) {
		it->second->update();
	}
}
#ifndef __EFFECT_H__
#define __EFFECT_H__

#include "cocos2d.h"

class CEffect
	: public cocos2d::Node
{
	public:
		CEffect();
		virtual ~CEffect();
		virtual void update() = 0;
		virtual void reset() = 0;
		virtual void start(float intensity, float duration, int direction) = 0;
		virtual void addDuration(float duration) = 0;

		std::string name;
		float intensity;
		float duration;
		int direction;
		bool runOnce;
};

#endif // __EFFECT_H__

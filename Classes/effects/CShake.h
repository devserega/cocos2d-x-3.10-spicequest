#ifndef __SHAKE_H__
#define __SHAKE_H__

#include "cocos2d.h"
#include "CEffect.h"

class CShake
	: public CEffect
{
	public:
		static CShake* create();
		virtual bool init();
		virtual ~CShake();

		void start(float intensity, float duration, int direction);
		void addDuration(float duration);
		void end();
		void update();
		void reset();

	protected:
		CShake();
		
		float duration;
		float offset_x;
		float offset_y;
};

#endif // __SHAKE_H__

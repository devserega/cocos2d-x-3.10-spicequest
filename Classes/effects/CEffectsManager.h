#ifndef __EFFECTS_MANAGER_H__
#define __EFFECTS_MANAGER_H__

#include "cocos2d.h"

using namespace cocos2d;

struct TParamenters {
	float intensity;
	float duration;
	int direction;
};

class CPiriPiriBlastGame;
class CEffect;
class CEffectsManager
	: public cocos2d::Layer {
	public:
		static CEffectsManager* create(CPiriPiriBlastGame* game);
		virtual bool init(CPiriPiriBlastGame* game);
		virtual ~CEffectsManager();

		void _do(std::string& eventName, float intensity, float duration, int direction); 
		CEffect* _isEffectRunning(std::string& type);
		void onEffectComplete(); // effect, event
		void reset();
		void update();

	protected:
		CEffectsManager();	
		std::map<std::string, CEffect*> effects;
		CPiriPiriBlastGame* mGame;
};

#endif // __EFFECTS_MANAGER_H__

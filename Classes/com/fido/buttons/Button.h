#ifndef __COM_FIDO_BUTTONS_H__
#define __COM_FIDO_BUTTONS_H__

//namespace com {
//namespace fido {
//namespace buttons{

#include "cocos2d.h"
//#define Button_DEBUG

using namespace cocos2d;

class Button;
//typedef std::function<void(CPiriPiriButton*)> TOnPressCallbackWithButtonParam;
//typedef std::function<void()> TOnPressCallback;

class Button
	: public cocos2d::Node
{
	public:
		virtual bool init(const std::string& icon, bool wide);
		static Button* create(const std::string& icon, bool wide = false);
		/*
		void onPress(TOnPressCallbackWithButtonParam onPressCallback);
		void onPress(TOnPressCallback onPressCallback);
		void enable();
		void disable();
		virtual void onMouseOver();
		virtual void onMouseOut();
		void setIcon(const std::string& filename);

		bool  mInteractive;
		*/

	protected:
		//bool myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point);
		Button();
		/*
		void onEnter();
		void onExit();
		void addListener();
		void removeListener();

		EventListenerTouchOneByOne* mListener;
		TOnPressCallbackWithButtonParam mOnPressCallbackWithButtonParam;
		TOnPressCallback mOnPressCallback;
		Sprite* mBg;
		Sprite* mIcon;
		bool mEnabled;
		bool mWide;
		bool mDown;

		float mWidth;
		float mHeight;
		float mOffsetY;
		float mIconOriginalY;
		float mRScale;
		float mTScale;
		*/
};
//}}}
#endif // __COM_FIDO_BUTTONS_H__

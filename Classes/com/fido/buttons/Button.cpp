#include "Button.h"
//#include "media/CSoundManager.h"

//using namespace com::fido::buttons;

Button::Button()
	//: mOnPressCallbackWithButtonParam(nullptr)
	//, mOnPressCallback(nullptr)
	//, mListener(nullptr)
	//, mInteractive(true) 
{

}

bool Button::init(const std::string& icon, bool wide) {
	if (!cocos2d::Node::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);
	/*
	//var view = pView || new PIXI.DisplayObjectContainer;
	this->mWide = wide || false;
	this->mRScale = 1;
	this->mTScale = 1;
	this->mOffsetY = 15;
	this->mDown = false;
	if (this->mWide) {
		this->mBg = Sprite::createWithSpriteFrameName("LongButtonUp.png");//interfaceSprites/
		addChild(this->mBg, 1);
		this->mTScale = 1.1;
	}
	else {
		this->mBg = Sprite::createWithSpriteFrameName("LargeSquareButtonUp.png"); //interfaceSprites/
		addChild(this->mBg, 1);
		this->mRScale = .5;
		this->mTScale = .52;
		this->setScaleX(this->mRScale);
		this->setScaleY(this->mTScale);
	}
	this->mBg->setAnchorPoint(Vec2(.5, .5));

	//view.hasOwnProperty("anchor") == = !0 && view.anchor.set(.5, .5),
	//	Button.call(this, view),
	if (icon.compare("") != 0) {
		this->mIcon = Sprite::createWithSpriteFrameName(icon);
		if (this->mWide) {
			this->mIcon->setScale(1, 1);
			this->mIcon->setAnchorPoint(Vec2(.5, .5));
		}
		else {
			this->mIcon->setScale(2, 2);
			this->mIcon->setAnchorPoint(Vec2(.5, .52));
		}
		addChild(this->mIcon, 2);
	}
	this->mIconOriginalY = this->mIcon->getPositionY();

	//this.view.touchstart = this.onMouseDown.bind(this),
	//this.view.mouseupoutside = this.view.touchend = this.view.touchendoutside = this.onMouseUp.bind(this),
	//this.onDown.add(this.onMouseDown, this),
	//this.onUp.add(this.onMouseUp, this),
	this->mEnabled = true;

	mWidth = this->mBg->getContentSize().width;
	mHeight = this->mBg->getContentSize().height;

#ifdef CPiriPiriButton_DEBUG
	auto rectWithBorder = DrawNode::create();
	Vec2 vertices[] = {
		Vec2(-mWidth / 2,-mHeight / 2),
		Vec2(-mWidth / 2, mHeight / 2),
		Vec2(mWidth / 2, mHeight / 2),
		Vec2(mWidth / 2,-mHeight / 2)
	};
	rectWithBorder->drawPolygon(vertices, 4, Color4F(1.0f, 0.3f, 0.3f, 1), 1, Color4F(0.2f, 0.2f, 0.2f, 1));
	addChild(rectWithBorder);
#endif
	*/
	return true;
}

Button* Button::create(const std::string& icon, bool wide) {
	Button* pRet = new (std::nothrow) Button();
	if (pRet && pRet->init(icon, wide)) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
void CPiriPiriButton::onPress(TOnPressCallbackWithButtonParam onPressCallbackWithButtonParam) {
	mOnPressCallbackWithButtonParam = onPressCallbackWithButtonParam;
}

void CPiriPiriButton::onPress(TOnPressCallback onPressCallback) {
	mOnPressCallback = onPressCallback;
}

bool CPiriPiriButton::myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point) {
	bool bRet = false;

	if (point.x >= rect.origin.x && point.x <= rect.size.width &&
		point.y >= rect.origin.y && point.y <= rect.size.height) {
		bRet = true;
	}

	return bRet;
}

void CPiriPiriButton::enable() {
	if (!this->mEnabled) {
		this->mEnabled = true;
		setOpacity(255);
		this->mInteractive = true;
		this->mBg->setSpriteFrame("LargeSquareButtonUp.png"); //interfaceSprites/
	}
	//this.enabled || (
	//this.enabled = !0,
	//this.view.alpha = 1,
	//this.view.interactive = !0,
	///this.bg.setTexture(PIXI.Texture.fromFrame("LargeSquareButtonUp.png"))
	//)
}

void CPiriPiriButton::disable() {
	if (this->mEnabled) {
		this->mEnabled = false;
		setOpacity((GLubyte)(.6 * 255)); 	//this.view.alpha = .6,
		this->mInteractive = false;
		this->mBg->setSpriteFrame("LargeSquareButtonNo.png");
	}
}

void CPiriPiriButton::onMouseOver() {
	runAction(EaseElasticOut::create(ScaleTo::create(0.2, this->mTScale, this->mTScale), 0.3f));
}

void CPiriPiriButton::onMouseOut() {
	runAction(EaseElasticOut::create(ScaleTo::create(0.2, this->mRScale, this->mRScale), 0.3f));
}

void CPiriPiriButton::setIcon(const std::string& filename) {
	mIcon->setSpriteFrame(filename);
}

void CPiriPiriButton::onEnter() {
	cocos2d::Node::onEnter();
	addListener();
}

void CPiriPiriButton::onExit() {
	cocos2d::Node::onExit();
	removeListener();
}

void CPiriPiriButton::addListener() {
	if (!mListener) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		mListener = cocos2d::EventListenerTouchOneByOne::create();
		mListener->setSwallowTouches(true);

		mListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			if (!this->mInteractive || !this->isVisible()) { //this->mEnabled
				return false;
			}

			auto target = static_cast<Sprite*>(e->getCurrentTarget());
			cocos2d::Vec2 p = target->convertToNodeSpace(touch->getLocation());
			//cocos2d::Vec2 p = touch->getLocation();

			//CCLOG("mp %f %f", p.x, p.y);
		cocos2d:Rect rect = Rect(0, 0, mWidth, mHeight);
			//cocos2d::Rect rect = this->getBoundingBox();

			if (myContainsPoint(rect, p)) {
				CSoundManager::getInstance()->sfx->play("buttonPress");
				this->mBg->setSpriteFrame(this->mWide ? "LongButtonClick.png" : "LargeSquareButtonClick.png");
				this->mIcon->runAction(MoveTo::create(0.05, Vec2(0, this->mIconOriginalY + this->mOffsetY)));
				this->onMouseOver();
				// original
				//TweenLite.to(this.icon, .05, {
				//y: this.iconOriginalY - this.offsetY,
				//ease: Linear.none
				//}),
				//TweenLite.to(this.view.scale, .2, {
				//x: this.tScale,
				//y: this.tScale,
				//ease: Elastic.easeOut
				//}))

				return true; // to indicate that we have consumed it.
			}

			return false; // we did not consume this event, pass throw.
		};

		mListener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
			this->mBg->setSpriteFrame(this->mWide ? "LongButtonUp.png" : "LargeSquareButtonUp.png");
			this->mIcon->runAction(MoveTo::create(0.05, Vec2(0, this->mIconOriginalY)));
			this->onMouseOut();
			//runAction(EaseElasticOut::create(ScaleTo::create(0.2, this->mRScale, this->mRScale),0.3f));
			// original
			//TweenLite.to(this.icon, .05, {
			//y: this.iconOriginalY,
			//ease : Linear.none
			//}),
			//TweenLite.to(this.view.scale, .2, {
			//x: this.rScale,
			//y : this.rScale,
			//ease : Elastic.easeOut
			//})
			
			if (mOnPressCallbackWithButtonParam != nullptr)
				mOnPressCallbackWithButtonParam(this);
			else if (mOnPressCallback != nullptr) {
				mOnPressCallback();
			}
		};
		dispatcher->addEventListenerWithSceneGraphPriority(mListener, this->mBg);
	}
}

void CPiriPiriButton::removeListener() {
	if (mListener != nullptr) {
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->removeEventListener(mListener);
		mListener = nullptr;
	}
}
*/
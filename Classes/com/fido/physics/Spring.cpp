#include "Spring.h"

Spring::Spring() {
}

Spring::~Spring() {
}

Spring* Spring::create() {
	Spring* pRet = new Spring();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool Spring::init() {
	this->x = 0;
	this->ax = 0;
	this->dx = 0;
	this->tx = 0;
	this->max = 160;
	this->damp = .6;
	this->springiness = .1;

	return true;
}

void Spring::update(){
	this->ax = (this->tx - this->x) * this->springiness;
	this->dx += this->ax;
	this->dx *= this->damp;
	this->dx < -this->max ? this->dx = -this->max : this->dx > this->max && (this->dx = this->max);
	this->x += this->dx;
}

void Spring::reset() {
	this->x = 0;
	this->ax = 0;
	this->dx = 0;
	this->tx = 0;
}
#ifndef __SPRING_H__
#define __SPRING_H__

#include "cocos2d.h"

using namespace cocos2d;

class Spring
	: public cocos2d::Ref
{
	public:
		Spring();
		static Spring* create();
		void update();
		void reset();

		float x;
		float ax;
		float dx;
		float tx;
		float max;
		float damp;
		float springiness;

	protected:
		virtual ~Spring();
		virtual bool init();
};

#endif // __SPRING_H__

#include "PixiTrackpad.h"

PixiTrackpad::PixiTrackpad()
	: spring(nullptr)
	//, this.onScrollUpdate = new Signal,
	//this.target = parameters.target,
	, value(0)
	, easingValue(0)
	, dragOffset(0)
	, dragging(false) // !!!
	, speed(0)
	, size(800)
	, maxSlots(1)
	, capMovement(true)
	, prevPosition(0)
	, valueY(0)
	, easingValueY(0)
	, dragOffsetY(0)
	, speedY(0)
	, prevPositionY(0)
	, didMove(true)
	//this.target.interactive = !0,
	, scrollMin(-600)
	, scrollMax(0)
	, scrollMinY(-600)
	, scrollMaxY(0)
	, snapTo(false)
	, currentSlot(0)
	, locked(false){
}

PixiTrackpad::~PixiTrackpad() {
}

PixiTrackpad* PixiTrackpad::create(CTutorialScreen* target) {
	PixiTrackpad* pRet = new PixiTrackpad();
	if (pRet && pRet->init(target)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool PixiTrackpad::init(CTutorialScreen* target) {
	if (!PixiScrollBar::init()) {
		return false;
	}

	this->target = target;
	this->spring = Spring::create();

	//this->mWidth = this->mBg->getContentSize().width;
	//this->mHeight = this->mBg->getContentSize().height;

	return true;
}

void PixiTrackpad::unlock() {
	this->locked = false;
	this->speed = 0;
	this->easingValue = this->value;
}

void PixiTrackpad::lock() {
	this->locked = true;
}

void PixiTrackpad::update() {
	if (this->value = this->easingValue, this->valueY = this->easingValueY, this->dragging) {
		int newSpeed = this->easingValue - this->prevPosition;
		newSpeed *= .7;
		this->speed += .5 * (newSpeed - this->speed);
		this->prevPosition = this->easingValue;
		int newSpeedY = this->easingValueY - this->prevPositionY;
		newSpeedY *= .7;
		this->speedY += .5 * (newSpeedY - this->speedY);
		this->prevPositionY = this->easingValueY;
	}
	else {
		if (this->snapTo) {
			this->spring->update();
			this->easingValue = this->spring->x;
		}
		else {
			this->speed *= .95;
			this->easingValue += this->speed;
			this->speedY *= .95;
			this->easingValueY += this->speedY;
			if (this->capMovement) {
				this->easingValue > this->scrollMax ? this->easingValue += .3 * (this->scrollMax - this->easingValue) : this->easingValue < this->scrollMin && (this->easingValue += .3 * (this->scrollMin - this->easingValue));
				this->easingValueY > this->scrollMaxY ? this->easingValueY += .3 * (this->scrollMaxY - this->easingValueY) : this->easingValueY < this->scrollMinY && (this->easingValueY += .3 * (this->scrollMinY - this->easingValueY));
			}
		}
	}
}

void PixiTrackpad::setPosition(int value, int valueY) {
	this->value = this->easingValue = value;
	this->valueY = this->easingValueY = valueY;
}

void PixiTrackpad::easeToPosition(int value, int valueY) {
	this->easingValue = value;
	this->easingValueY = valueY;
}

void PixiTrackpad::onDown(cocos2d::Vec2& global) { //data
	if (!this->locked) {// 	this.locked ||
		this->didMove = false;
		this->checkX = global.x;
		this->max = 30;
		this->damp = .85;
		this->springiness = .09;
		this->dragging = true; 
		this->dragOffset = global.x - this->value;
		this->dragOffsetY = global.y - this->valueY;
		//this.target.touchend = this.target.touchendoutside = this.target.mouseup = this.target.mouseupoutside = this.onUp.bind(this);
		//this.target.touchmove = this.target.mousemove = this.onMove.bind(this);
	}
}

void PixiTrackpad::onUp() {
	if (!this->locked) {
		if (this->dragging = false,  this->snapTo) {
			if (this->didMove) {
				double target;
				if (this->spring->dx = this->speed && this->speed < 0)
					target = floor(this->easingValue / this->size);
				else
					target = ceil(this->easingValue / this->size);
				target > 0 ? target = 0 : target < -this->maxSlots && (target = -this->maxSlots);
				this->currentSlot = -target;
				this->spring->tx = target * this->size;
			}
			this->spring->x = this->easingValue;
		}
		//this.target.mouseup = nullptr;
		//this.target.mousemove = nullptr;
	}
}
						
void PixiTrackpad::nextSlot() {
	this->currentSlot++;
	this->spring->tx -= this->size;
	this->spring->tx < -(this->maxSlots * this->size) && (this->spring->tx = -(this->maxSlots * this->size));
}

void PixiTrackpad::previousSlot() {
	this->currentSlot--;
	this->spring->tx += this->size;
	this->spring->tx > 0 && (this->spring->tx = 0);
}

void PixiTrackpad::setSize() { //size
	//								this.size = size,
	//									this.scrollbar && this.scrollBar.setSize(size)
}

void PixiTrackpad::setSlots(int slots) {
	this->maxSlots = slots - 1;
	this->scrollMin = this->maxSlots * this->size;
}

void PixiTrackpad::onMove(cocos2d::Vec2& global) {//data
	float dist = abs(this->checkX - global.x);
	if (dist > 2) {
		this->didMove = true;
		this->easingValue = global.x - this->dragOffset;
		this->easingValueY = global.y - this->dragOffsetY;
		if (this->capMovement) {
			this->easingValue > this->scrollMax ? this->easingValue = this->scrollMax + .3 * (this->easingValue - this->scrollMax) : this->easingValue < this->scrollMin && (this->easingValue = this->scrollMin + .3 * (this->easingValue - this->scrollMin));
			this->easingValueY > this->scrollMaxY ? this->easingValueY = this->scrollMaxY + .3 * (this->easingValueY - this->scrollMaxY) : this->easingValueY < this->scrollMinY && (this->easingValueY = this->scrollMinY + .3 * (this->easingValueY - this->scrollMinY));
		}
	}
}
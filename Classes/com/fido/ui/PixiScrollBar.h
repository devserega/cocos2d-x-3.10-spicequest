#ifndef __PIXI_SCROLLBAR_H__
#define __PIXI_SCROLLBAR_H__

//namespace com::fido::ui {

#include "cocos2d.h"

using namespace cocos2d;

class PixiScrollBar
	: public cocos2d::Node
{
	public:
		PixiScrollBar();
		static PixiScrollBar* create();

	protected:
		virtual ~PixiScrollBar();
		virtual bool init();
};

//}

#endif // __PIXI_SCROLLBAR_H__
#include "PixiScrollBar.h"

PixiScrollBar::PixiScrollBar() {
}

PixiScrollBar::~PixiScrollBar() {
}

PixiScrollBar* PixiScrollBar::create() {
	PixiScrollBar* pRet = new PixiScrollBar();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool PixiScrollBar::init() {
	if (!cocos2d::Node::init()) {
		return false;
	}

	return true;
}
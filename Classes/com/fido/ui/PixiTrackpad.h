#ifndef __PIXI_TRACKPAD_H__
#define __PIXI_TRACKPAD_H__

#include "com/fido/ui/PixiScrollBar.h"
#include "com/fido/physics/Spring.h"

using namespace cocos2d;

class CTutorialScreen;
class PixiTrackpad
	: public PixiScrollBar
{
	public:
		PixiTrackpad();
		static PixiTrackpad* create(CTutorialScreen* target);

		void unlock();
		void lock();
		void update();
		void setPosition(int value, int valueY);
		void easeToPosition(int value, int valueY);
		void onDown(cocos2d::Vec2& global);
		void onUp();
		void nextSlot();
		void previousSlot();
		void setSize();
		void setSlots(int slots);
		void onMove(cocos2d::Vec2& global);

		Spring* spring;
		//this.onScrollUpdate = new Signal,
		CTutorialScreen* target;
		int value;
		float easingValue;
		float dragOffset;
		bool dragging;
		int speed;
		int size;
		int maxSlots;
		bool capMovement;
		int prevPosition;
		int valueY;
		float easingValueY;
		float dragOffsetY;
		int speedY;
		int prevPositionY;
		bool didMove;
		//this.target.interactive = !0,
		double scrollMin;
		double scrollMax;
		double scrollMinY;
		double scrollMaxY;
		bool snapTo;
		int currentSlot;
		//"undefined" != typeof parameters.scrollbar && Device.instance.isMobile == = !1 && Device.instance.iPad == = !1;
		//this.target.touchstart = this.target.mousedown = this.onDown.bind(this)

		float checkX;
		float max;
		float damp;
		float springiness;

	protected:
		bool locked;
		virtual ~PixiTrackpad();
		virtual bool init(CTutorialScreen* target);
};

#endif // __PIXI_TRACKPAD_H__
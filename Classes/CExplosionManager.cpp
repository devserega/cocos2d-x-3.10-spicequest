#include "CExplosionManager.h"
#include "CPiriPiriBlastGame.h"
#include "gridworld/CPiriPiriGridWorld.h"
#include "CBombExplosion.h"
#include "blocks/CBarrel.h"
#include "blocks/CBomb.h"
#include "CFragment.h"
#include "CHud.h"
#include "effects/CEffectsManager.h"

CExplosionManager* CExplosionManager::create(CPiriPiriBlastGame* game) {
	CExplosionManager* pRet = new(std::nothrow) CExplosionManager();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->mGame = game;
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CExplosionManager::init() {
	//if (!cocos2d::Ref::init()) {
	//	return false;
	//}

	// ...
	mBombCount = 0;

	return true;
}

CExplosionManager::CExplosionManager() {

}

CExplosionManager::~CExplosionManager() {

}

void CExplosionManager::add(CBomb* bomb) {
	CBombExplosion* explosion = CBombExplosion::create(this);
	explosion->reset(this->mGame->mGridWorld, bomb, this);
	//bombExplosion->onComplete.addOnce(this.onExplosionFinish, this),
	this->mExplosions.push_back(explosion);
	explosion->start();
	if (bomb->cell->hasGem)
		this->mGame->mHud->showGem(bomb->cell->centerX, bomb->cell->centerY);
}

void CExplosionManager::reset() {
	//for (int i = this->mExplosions.size(); i--;) this.explosionPool.returnObject(this.explosions[i]);
	this->mExplosions.clear();
	for (int i = this->mFragments.size(); i--;) {
		mFragments[i]->removeFromParent();
		//this.fragmentPool.returnObject(); !!!!
	}
	this->mFragments.clear();
}

void CExplosionManager::update() {
	for (unsigned int i = this->mExplosions.size(); i--;) 
		this->mExplosions[i]->update();
	for (unsigned int i = this->mFragments.size(); i--;) {
		this->mFragments[i]->update();
		if (this->mFragments[i]->mIsDead) {
			this->mGame->mExplosionLayer->removeChild(this->mFragments[i]);
			this->mFragments.erase(mFragments.begin()+i, mFragments.begin() + i + 1);
			this->checkComplete();
		}
	}
}

void CExplosionManager::checkComplete() {
	for (unsigned int i = 0; i < this->mGame->mBombManager->mBombs.size(); i++)
		if (this->mGame->mBombManager->mBombs[i]->mIsActive == true) return;
	if (0 == this->mFragments.size() && 
		0 == this->mExplosions.size() && 
		this->mGame->mBombManager->mBombCount == 0 && 
		0 == this->mGame->mHud->mActiveSpiceExplodes.size()) 
	{
		mGame->gameBoardCleared(); // original this.onAllComplete.dispatch();
	}
}

void CExplosionManager::addFragment(CGridCell* cell, std::string bombIdent) {
	this->mGame->mEffectsManager->_do(std::string("shake"), .01, 6.0, AXIS::BOTH);
	/* original
	this.game.effectsManager.do("shake", {
	intensity: .01,
			   duration : 6,
		direction : Constants.AXIS.BOTH
	}),*/
	this->mGame->mHud->showSpiceExplode(cell->centerX + OFFSETS::x, cell->centerY - OFFSETS::y);

	std::string ident;
	if (bombIdent.compare("")==0) {
		if (cell->item->mIdent.compare("") == 0)
			ident = "A";
		else
			ident = cell->item->mIdent;
	}
	else
		ident = bombIdent;
	/*
	if (dynamic_cast<CBomb*>(bomb) == nullptr) {
		if (cell->item->mIdent.compare("") == 0) 
			ident = "A";
		else 
			ident = cell->item->mIdent;
	}
	else
		ident = bomb->mIdent;
	*/

	CFragment* fragment = CFragment::create(); 
	fragment->reset(ident);
	fragment->setPositionX(cell->centerX + OFFSETS::x);
	fragment->setPositionY(cell->centerY - OFFSETS::y);
	this->mGame->mExplosionLayer->addChild(fragment);
	this->mFragments.push_back(fragment);
}

void CExplosionManager::onExplosionFinish(CBombExplosion* explosion) {
	//this.explosionPool.returnObject(explosion)

	std::vector<CBombExplosion*>::iterator foundElement = std::find_if(	mExplosions.begin(), mExplosions.end(),
																		[explosion](CBombExplosion* vecElement) {return vecElement == explosion;});
	mExplosions.erase(foundElement); // original this.explosions.splice(this.explosions.indexOf(explosion), 1)
}
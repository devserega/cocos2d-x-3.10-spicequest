#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "cocos2d.h"

using namespace cocos2d;

class CPiriPiriBlastGame;
class CPointAndClickController
	: public cocos2d::Node
{
	public:
		static CPointAndClickController* create(CPiriPiriBlastGame* game);
		virtual ~CPointAndClickController();
		void disable();
		void enable();
		bool isInteractive();

	private:
		CPointAndClickController();
		virtual bool init(CPiriPiriBlastGame* game);

		bool interactive;
		bool buttonMode;
		CPiriPiriBlastGame* mGame;
};

#endif // __CONTROLLER_H__
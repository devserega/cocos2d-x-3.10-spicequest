#include "CDestructable.h"

CDestructable::CDestructable(){
}

CDestructable* CDestructable::create() {
	CDestructable* pRet = new CDestructable();
	if (pRet && pRet->init()){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CDestructable::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	this->mSprite = Sprite::createWithSpriteFrameName("BrickWall.png");
	this->mSprite->setAnchorPoint(Vec2(.5, .5));
	addChild(this->mSprite);
	this->setScale(1.0f);

	this->mId = 1;
	this->aStarBlocker = true;
	this->stopsExplosion = true;
	this->mSpriteClone = Sprite::createWithSpriteFrameName("BrickWall.png");
	this->mSpriteClone->setAnchorPoint(Vec2(.5, .5)),
	this->mSpriteClone->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSpriteClone->setOpacity(0);
	addChild(this->mSpriteClone);

	return true;
}

CDestructable::~CDestructable() {
}
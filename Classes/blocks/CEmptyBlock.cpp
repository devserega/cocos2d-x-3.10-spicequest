#include "CEmptyBlock.h"

CEmptyBlock::CEmptyBlock() {
}

CEmptyBlock* CEmptyBlock::create() {
	CEmptyBlock* pRet = new CEmptyBlock();
	if (pRet && pRet->init()) {
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CEmptyBlock::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	this->mId = 0;
	this->aStarBlocker = false;
	this->stopsExplosion = false;

	return true;
}

CEmptyBlock::~CEmptyBlock() {
}
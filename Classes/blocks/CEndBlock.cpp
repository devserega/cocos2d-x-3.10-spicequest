#include "CEndBlock.h"

CEndBlock::CEndBlock() {
}

CEndBlock::~CEndBlock() {
}

CEndBlock* CEndBlock::create() {
	CEndBlock* pRet = new CEndBlock();
	if (pRet && pRet->init()){
		//pRet->autorelease();//!!!
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CEndBlock::init() {
	if (!cocos2d::Sprite::initWithSpriteFrameName("EndSquare.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setAnchorPoint(Vec2(.5, .5));

	mId = 8;

	return true;
}
#ifndef __CRACKBLOCK_H__
#define __CRACKBLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CCrackBlock
	: public CPiriPiriGameObject
{
	public:
		virtual ~CCrackBlock();
		static CCrackBlock* create();
		virtual bool init();

	private:
		CCrackBlock();
};

#endif // __CRACKBLOCK_H__
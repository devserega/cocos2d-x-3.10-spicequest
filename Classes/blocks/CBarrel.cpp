#include "CBarrel.h"
#include "CTicker.h"

CBarrel::CBarrel(){
}

CBarrel::~CBarrel(){
}

static int i = 1;

void CBarrel::update() {
	//CCLOG("CBarrel %d %d ended=%d", i, this->mIsActive, this->mEnded);
	if ( this->mIsActive && this->mEnded == false) {
		i++;
		float pivot_x = 2 * sin(this->mCountdownTimer) * this->mRatio;
		this->mContainer->setPositionX(pivot_x);
		float rotation = (((double)std::rand() / (RAND_MAX))  * (.01 - .1) + .1);	// (Math.random() * (.01 - .1) + .1).toFixed(4)
		this->mContainer->setRotation(rotation*57.2958);
		//var that = this;
		this->mCountdownTimer -= CTicker::getInstance()->getDeltaTime();
		if ( this->mCountdownTimer <= 0) {
			auto clone = Sprite::createWithSpriteFrameName(this->mSpriteName);
			clone->setAnchorPoint(Vec2(0, 0));
			clone->setPositionX(.5 * -clone->getContentSize().width);
			clone->setPositionY(.5 * -clone->getContentSize().height);
			clone->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
			clone->setOpacity(0);
			this->mContainer->addChild(clone, 2);
			this->mRatio += .01;
			if (this->mRatio > 2) this->mRatio = 2;
			if ((std::rand() / RAND_MAX) > .5) setRotation(getRotation() - 2 * getRotation());

			mFlashLight->runAction(FadeIn::create(.4f));
			clone->runAction(Sequence::create(	DelayTime::create(.1f),
												FadeTo::create(.3f, 204), //.8
												nullptr));
			mFlashLight->runAction(Sequence::create(EaseElasticOut::create(ScaleTo::create(.5f, 2), 0.3f),
													CallFunc::create([this, clone]() {
														this->explode();
														this->mCountdownTimer = this->mCountdown;
														this->removeChild(clone, false);
														this->removeChild(this->mFlashLight, false); 
														//	that.view.removeChild(clone),
														//	that.view.removeChild(that.flashLight)
														
														// my [begin]
														//release(); 
														//autorelease();
														// my [end]
														CGameObject::remove();
													}),
													nullptr));

			/* original
			TweenLite.to(this.flashLight, .4, {
				alpha: 1
			}),
			TweenLite.to(clone, .3, {
				alpha: .8,
				delay : .1
			}),
			TweenLite.to(this.flashLight.scale, .5, {
				x: 2,
				y : 2,
				scale : Expo.easeIn,
				onComplete : function() {
					that.explode(),
					that.countdownTimer = that.countdown,
					that.view.removeChild(clone),
					that.view.removeChild(that.flashLight)
				}
			}),*/
			this->mEnded = true;
		}
	}
}

void CBarrel::onReact(/*direction*/) {
	float rotation = 0;
	float newX = 0;
	float newY = 0;
	float posX = this->getPositionX();
	float posY = this->getPositionY();
	
	/*
	"right" == = direction ? (rotation += .3, newX += 5) : 
	"left" == = direction ? (rotation -= .3, newX -= 5) : 
	"up" == = direction ? (rotation += Math.random() > .5 ? .1 : -.1, newY -= 5) : (rotation += Math.random() > .5 ? .1 : -.1, newY += 5),
	TweenLite.to(this.view, .1, {
	rotation: rotation,
	x : posX + newX,
	y : posY + newY
	}),
	TweenLite.to(this.view, .1, {
	rotation: rotation - 1.3 * rotation,
	x : posX - 1.3 * newX,
	y : posY - 1.3 * newY,
	delay : .1
	}),
	TweenLite.to(this.view, .1, {
	rotation: .4 * rotation,
	x : posX + .4 * newX,
	y : posY + .4 * newY,
	delay : .2
	}),
	TweenLite.to(this.view, .1, {
	rotation: rotation - 1.1 * rotation,
	x : posX - 1.1 * newX,
	y : posY - 1.1 * newY,
	delay : .3
	}),
	TweenLite.to(this.view, .1, {
	rotation: 0,
	x : posX,
	y : posY,
	delay : .4
	}),*/
	this->mIsActive = true;
}

CBarrel* CBarrel::create(std::vector<std::string>& params, CBombManager* bombManager) {
	CBarrel* pRet = new CBarrel();
	if (pRet && pRet->init(params, bombManager)){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CBarrel::init(std::vector<std::string>& params, CBombManager* bombManager) {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	mBombManager = bombManager;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	this->mContainer = Node::create();
	this->mContainer->setCascadeOpacityEnabled(true);
	addChild(this->mContainer, 0);

	setAnchorPoint(Vec2(.5, .5));

	if (params.size() == 1)
		this->mIdent = "A";
	else if(params.size() == 2)
		this->mIdent = params[1];

	this->mSpriteName = "SpicePot" + this->mIdent + ".png";
	mSprite = Sprite::createWithSpriteFrameName(mSpriteName);
	mSprite->setAnchorPoint(Vec2(.5, .5)); 
	this->mContainer->addChild(mSprite,0);
	setScale(1,1);

	this->mExplosionSound = "explodePot";
	this->mId = 3;
	this->mCountdown = 48;
	this->mCountdownTimer = mCountdown;
	this->aStarBlocker = true;
	this->mRatio = .5;
	if (this->mIdent.compare("A") == 0) 
		this->mExplosionSize = BARREL::blastRadiusA;
	else if (this->mIdent.compare("B") == 0) 
		this->mExplosionSize = BARREL::blastRadiusB;
	this->showBlast = false;
	this->mFlashLight = Sprite::createWithSpriteFrameName("Light.png");
	this->mFlashLight->setOpacity(0);
	this->mFlashLight->setScale(.2, .2);
	this->mFlashLight->setAnchorPoint(Vec2(.5, .5));
	this->mFlashLight->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mFlashLight->setPositionX(0);
	this->mFlashLight->setPositionY(0);
	this->mContainer->addChild(this->mFlashLight, 1); // original this.view.addChildAt(this.flashLight, 0);
	this->mEnded = false;
	this->mIsActive = false;

	return true;
}

void CBarrel::remove() {
	if (!this->mIsActive) {
		CGameObject::remove();
	}
}
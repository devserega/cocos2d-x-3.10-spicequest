#include "CDoubleBlock.h"

CDoubleBlock::CDoubleBlock(){
}

CDoubleBlock* CDoubleBlock::create() {
	CDoubleBlock* pRet = new CDoubleBlock();
	if (pRet && pRet->init()) {
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CDoubleBlock::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	this->mSprite = Sprite::createWithSpriteFrameName("EnforcedWall.png");
	this->mSprite->setAnchorPoint(Vec2(.5, .5));
	addChild(this->mSprite);

	this->mId = 6;
	this->life = 2;
	this->aStarBlocker = true;
	this->isDestroyable = false;
	this->mSpriteClone = Sprite::createWithSpriteFrameName("BrickWall.png");
	this->mSpriteClone->setAnchorPoint(Vec2(.5, .5)),
	this->mSpriteClone->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSpriteClone->setOpacity(0);
	addChild(this->mSpriteClone);

	return true;
}

CDoubleBlock::~CDoubleBlock() {
}

void CDoubleBlock::takeLife() {
	auto that = this;
	this->life--;
	this->isDestroyable = true;
	this->mSpriteClone->runAction(EaseExponentialIn::create(FadeTo::create(.2f, 255)));
	this->mSpriteClone->runAction(Sequence::create(	DelayTime::create(.2f),
													EaseBackOut::create(FadeTo::create(.2f, 0)),
													nullptr));
	this->mSpriteClone->runAction(Sequence::create(	DelayTime::create(.4f),
													EaseBackIn::create(FadeTo::create(.2f, 255)),
													CallFunc::create([that]() {
														that->mSprite->setSpriteFrame("BrickWall.png");
													}),
													nullptr));
	this->mSpriteClone->runAction(Sequence::create(	DelayTime::create(.6f),
													EaseBackIn::create(FadeTo::create(.2f, 0)),
													nullptr));

	/* original
	TweenLite.to(this.spriteClone, .2, {
	alpha: 1,
		   delay : 0,
		ease : Expo.easeIn
	}),

		TweenLite.to(this.spriteClone, .2, {
		alpha: 0,
			   delay : .2,
			ease : Expo.easeOut
		}),

		TweenLite.to(this.spriteClone, .2, {
		alpha: 1,
			   delay : .4,
			ease : Expo.easeIn,
			onComplete : function() {
		that.sprite.setTexture(PIXI.Texture.fromFrame("BrickWall.png"))
	}
		}),
		TweenLite.to(this.spriteClone, .2, {
		alpha: 0,
			   delay : .6,
			ease : Expo.easeOut
		})
	*/
}

void CDoubleBlock::onDestroy() {
	this->isDead = true;
	auto that = this;
	//TweenLite.killTweensOf(this.sprite),
	//TweenLite.killTweensOf(this.spriteClone),
	this->mSpriteClone->runAction(FadeTo::create(.3f, 255));
	this->mSprite->runAction(EaseExponentialIn::create(ScaleTo::create(.6f, .6f, .6f)));
	this->mSpriteClone->runAction(EaseExponentialIn::create(ScaleTo::create(.6f, .6f, .6f)));
	this->mSprite->runAction(Sequence::create(	DelayTime::create(.5f),
												FadeTo::create(.2f, 0),
												nullptr));
	this->mSpriteClone->runAction(Sequence::create(	DelayTime::create(.5f),
													FadeTo::create(.2f, 0),
													CallFunc::create([that]() {
														that->onReact();
													}),
													nullptr));

	/* original
	var that = this;
	TweenLite.killTweensOf(this.sprite), 
	TweenLite.killTweensOf(this.spriteClone), 
	TweenLite.to(this.spriteClone, .3, {
	alpha: 1
	}),
		TweenLite.to(this.sprite.scale, .6, {
		x: .6,
		   y : .6,
			ease : Expo.easeIn
		}),
		TweenLite.to(this.spriteClone.scale, .6, {
		x: .6,
		   y : .6,
			ease : Expo.easeIn
		}),
		TweenLite.to(this.sprite, .2, {
		alpha: 0,
			   delay : .5
		}),
		TweenLite.to(this.spriteClone, .2, {
		alpha: 0,
			   delay : .5,
			onComplete : function() {
		that.onReact()
	}
		})
	*/
}

bool CDoubleBlock::isAlive() {
	return this->life > 0;
}
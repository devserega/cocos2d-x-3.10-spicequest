#ifndef __SOFTBLOCK_H__
#define __SOFTBLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CSoftBlock
	: public CPiriPiriGameObject
{
	public:
		static CSoftBlock* create();
		virtual bool init();
		virtual ~CSoftBlock();
		void onDestroy();

	protected:
		CSoftBlock();
};

#endif // __SOFTBLOCK_H__

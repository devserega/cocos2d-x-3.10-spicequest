#ifndef __CELL_DESTRUCTABLE_H__
#define __CELL_DESTRUCTABLE_H__

#include "cocos2d.h"

using namespace cocos2d;

class CCellDestructable
	: public cocos2d::Sprite
{
	public:
		virtual bool init();
		CREATE_FUNC(CCellDestructable);


	protected:
		CCellDestructable();
		virtual ~CCellDestructable();

	private:
		//EventListenerTouchOneByOne* mPlayBtnListener;
};

#endif // __CELL_DESTRUCTABLE_H__

#include "CUpDownBlock.h"
#include "CTicker.h"

CUpDownBlock::CUpDownBlock() {
}

CUpDownBlock::~CUpDownBlock() {
}

CUpDownBlock* CUpDownBlock::create(std::vector<std::string>& params) {
	CUpDownBlock* pRet = new CUpDownBlock();
	if (pRet && pRet->init(params)){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CUpDownBlock::init(std::vector<std::string>& params) {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	setAnchorPoint(Vec2(.5, .5));

	/*
UPDOWNPATTERNS: {
	A : [1, 0, 0, 0, 0, 0, 0],
	B : [1, 1, 1, 0, 0, 0, 0, 0],
	C : [0, 1, 1, 1, 0, 0, 0, 0],
	D : [0, 0, 1, 1, 1, 0, 0, 0],
	E : [0, 0, 0, 0, 0, 0, 1, 1],
	F : [0, 0, 0, 0, 1, 1, 1, 1],
	G : [0, 0, 0, 1, 1, 1, 1, 1],
	H : [1, 1, 0, 0, 0, 0, 1, 1]
	}, */

	// not good solution
	if (params[1].compare("A")==0)
		mPattern = { 1, 0, 0, 0, 0, 0, 0 };
	else if (params[1].compare("B") == 0)
		mPattern = { 1, 1, 1, 0, 0, 0, 0, 0 };
	else if (params[1].compare("C") == 0)
		mPattern = { 0, 1, 1, 1, 0, 0, 0, 0 };
	else if (params[1].compare("D") == 0)
		mPattern = { 0, 0, 1, 1, 1, 0, 0, 0 };
	else if (params[1].compare("E") == 0)
		mPattern = { 0, 0, 0, 0, 0, 0, 1, 1 };
	else if (params[1].compare("F") == 0)
		mPattern = { 0, 0, 0, 0, 1, 1, 1, 1 };
	else if (params[1].compare("G") == 0)
		mPattern = { 0, 0, 0, 1, 1, 1, 1, 1 };
	else if (params[1].compare("H") == 0)
		mPattern = { 1, 1, 0, 0, 0, 0, 1, 1 };
	this->mPatternPosition = 0;
	this->mUp = Sprite::createWithSpriteFrameName("MovingWallUp.png");
	this->mUp->setOpacity(0);
	this->mDown = Sprite::createWithSpriteFrameName("MovingWallDown.png");
	this->mDown->setOpacity(255);
	this->mUp->setAnchorPoint(Vec2(.5, .5));
	this->mDown->setAnchorPoint(Vec2(.5, .5));
	addChild(this->mUp, 1);
	addChild(this->mDown, 1);
	this->mId = 9;
	this->mState = this->mPattern[this->mPatternPosition];
	this->mCountdown = 30;
	this->mCountdownTimer = this->mCountdown;
	this->isDestroyable = false;
	this->mIsActive = true;
	this->setProperties();

	return true;
}

void CUpDownBlock::setDown() {
	this->mState = 0;
	this->mIsActive = false;
	this->setProperties();
}

void CUpDownBlock::updateState() {
	if (this->mIsActive == true && (this->mCountdownTimer -= CTicker::getInstance()->getDeltaTime(), this->mCountdownTimer <= 0)) {
		this->mPatternPosition++;
		this->mPatternPosition >= this->mPattern.size() && (this->mPatternPosition = 0);
		unsigned int wantedState = this->mPattern[this->mPatternPosition];
		if(this->mState != wantedState) this->toggleState();
		this->mCountdownTimer = this->mCountdown;
	}
}

void CUpDownBlock::toggleState() {
	this->mState = (0 == this->mState) ? 1 : 0;
	this->setProperties();
}

void CUpDownBlock::setProperties() {
	1 == this->mState ? (this->stopsExplosion = !0, this->mDown->setOpacity(0), this->mUp->setOpacity(255)) : (this->stopsExplosion = false, this->mUp->setOpacity(0), this->mDown->setOpacity(255));
}
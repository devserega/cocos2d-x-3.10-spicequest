#ifndef __INDESTRUCTABLE_H__
#define __INDESTRUCTABLE_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CIndestructable
	: public CPiriPiriGameObject
{
	public:
		static CIndestructable* create();
		virtual bool init();
		virtual ~CIndestructable();

	private:
		CIndestructable();
};

#endif // __INDESTRUCTABLE_H__
#include "CIndestructable.h"

CIndestructable::CIndestructable(){
}

CIndestructable* CIndestructable::create() {
	CIndestructable* pRet = new CIndestructable();
	if (pRet && pRet->init()){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CIndestructable::init() {
	if (!cocos2d::Sprite::initWithSpriteFrameName("SolidBlock.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setAnchorPoint(Vec2(.5,.5));
	// ...

	mId = 2;
	isDestroyable = false;
	aStarBlocker = true;

	return true;
}

CIndestructable::~CIndestructable() {
}

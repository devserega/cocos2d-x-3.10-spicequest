#include "CSoftBlock.h"

CSoftBlock::CSoftBlock() {
}

CSoftBlock* CSoftBlock::create() {
	CSoftBlock* pRet = new CSoftBlock();
	if (pRet && pRet->init()){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CSoftBlock::init() {
	if (!cocos2d::Sprite::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	setAnchorPoint(Vec2(.5, .5));

	this->mSprite = Sprite::createWithSpriteFrameName("WeakWall.png");
	this->mSprite->setAnchorPoint(Vec2(.5, .5));
	addChild(this->mSprite);

	this->mId = 10;
	this->aStarBlocker = true;
	this->stopsExplosion = false;
	this->mSpriteClone = Sprite::createWithSpriteFrameName("WeakWall.png");
	this->mSpriteClone->setAnchorPoint(Vec2(.5, .5)),
	this->mSpriteClone->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	this->mSpriteClone->setOpacity(0);
	addChild(this->mSpriteClone);

	return true;
}

CSoftBlock::~CSoftBlock() {
	int t1 = 4343;
}

void CSoftBlock::onDestroy() {
	this->isDead = true;
	auto that = this;
	this->mSpriteClone->runAction(FadeTo::create(.3f, 255));
	this->mSprite->runAction(Sequence::create(	EaseBackIn::create(ScaleTo::create(.6f, .6f, .6f)),
												nullptr));
	this->mSpriteClone->runAction(Sequence::create(	EaseBackIn::create(ScaleTo::create(.6f, .4f, .4f)),
													nullptr));
	this->mSprite->runAction(Sequence::create(	DelayTime::create(.5f),
												FadeTo::create(.2f, 0), 
												nullptr));
	this->mSpriteClone->runAction(Sequence::create(	DelayTime::create(.5f),
													FadeTo::create(.2f, 0),
													CallFunc::create([that]() {
														that->onReact();
													}),
													nullptr));

	/* original
	TweenLite.to(this.spriteClone, .3, {
		alpha: 1
	}),
	TweenLite.to(this.sprite.scale, .6, {
		x: .6,
		y : .6,
		ease : Back.easeIn
	}),
	TweenLite.to(this.spriteClone.scale, .6, {
		x: .4,
		y : .4,
		ease : Back.easeIn
	}),
	TweenLite.to(this.sprite, .2, {
		alpha: 0,
		delay : .5
	}),
	TweenLite.to(this.spriteClone, .2, {
		alpha: 0,
		delay : .5,
		onComplete : function() {
			that.onReact()
		}
	})
	*/
}

#ifndef __STARTBLOCK_H__
#define __STARTBLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CStartBlock
	: public CPiriPiriGameObject
{
	public:
		virtual ~CStartBlock();
		static CStartBlock* create();
		virtual bool init();

	private:
		CStartBlock();
};

#endif // __STARTBLOCK_H__
#ifndef __UPDOWNBLOCK_H__
#define __UPDOWNBLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CUpDownBlock
	: public CPiriPiriGameObject
{
	public:
		virtual ~CUpDownBlock();
		static CUpDownBlock* create(std::vector<std::string>& params);
		virtual bool init(std::vector<std::string>& params);
		void setDown();
		void updateState();
		void toggleState();
		void setProperties();

	protected:
		CUpDownBlock();
		Sprite* mUp;
		Sprite* mDown;
		std::vector<unsigned int> mPattern;
		unsigned int mPatternPosition;
		unsigned int mState;
		float mCountdown;
		float mCountdownTimer;
		bool mIsActive;
};

#endif // __UPDOWNBLOCK_H__
#ifndef __BRICKWALL_H__
#define __BRICKWALL_H__

#include "cocos2d.h"
#include "blocks/CSoftBlock.h"

class CDestructable
	: public CSoftBlock
{
	public:
		static CDestructable* create();
		virtual bool init();
		virtual ~CDestructable();

	protected:
		CDestructable();
};

#endif // __BRICKWALL_H__
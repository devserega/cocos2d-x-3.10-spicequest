#ifndef __ENDBLOCK_H__
#define __ENDBLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CEndBlock
	: public CPiriPiriGameObject
{
	public:
		virtual ~CEndBlock();
		static CEndBlock* create();
		virtual bool init();

	private:
		CEndBlock();
};

#endif // __ENDBLOCK_H__

#ifndef __BARREL_H__
#define __BARREL_H__

#include "cocos2d.h"
#include "CBomb.h"
#include "Constants.h"

using namespace cocos2d;

class CBarrel
	: public CBomb
{
	public:
		static CBarrel* create(std::vector<std::string>& params, CBombManager* bombManager);
		virtual bool init(std::vector<std::string>& params, CBombManager* bombManager);
		virtual ~CBarrel();

		void update();
		void onReact();
		void remove();

		//private:
		std::string mExplosionSound;
		//	this.onExplode = new Signal,
		std::string mSpriteName;
		Sprite* mFlashLight;

	protected:
		CBarrel();
		Node* mContainer;
};

#endif // __BARREL_H__

#include "CStartBlock.h"

CStartBlock::CStartBlock(){
}

CStartBlock::~CStartBlock() {
}

CStartBlock* CStartBlock::create() {
	CStartBlock* pRet = new CStartBlock();
	if (pRet && pRet->init()){
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CStartBlock::init() {
	if (!cocos2d::Sprite::initWithSpriteFrameName("StartSquare.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setAnchorPoint(Vec2(.5, .5));

	this->mId = 7;

	return true;
}
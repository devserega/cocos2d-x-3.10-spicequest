#ifndef __DOUBLE_BLOCK_H__
#define __DOUBLE_BLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CDoubleBlock
	: public CPiriPiriGameObject
{
	public:
		static CDoubleBlock* create();
		virtual bool init();
		virtual ~CDoubleBlock();
		void takeLife();
		void onDestroy();
		bool isAlive();

	protected:
		CDoubleBlock();
};

#endif // __DOUBLE_BLOCK_H__
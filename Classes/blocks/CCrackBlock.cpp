#include "CCrackBlock.h"

CCrackBlock::CCrackBlock() {
}

CCrackBlock::~CCrackBlock() {
}

CCrackBlock* CCrackBlock::create() {
	CCrackBlock* pRet = new CCrackBlock();
	if (pRet && pRet->init()){
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CCrackBlock::init() {
	if (!cocos2d::Sprite::initWithSpriteFrameName("FloorCrack.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setAnchorPoint(Vec2(.5, .5));
	this->mId = 5;
	this->isDestroyable = false;
	this->stopsExplosion = false;
	this->showBlast = true;

	return true;
}
#include "CBomb.h"
#include "CTicker.h"
#include "CBombManager.h"
#include "actions/CShakeBomb.h"
#include "media/CSoundManager.h"

static int bombCounter = 1;

CBomb::CBomb() 
	: CPiriPiriGameObject()
	, mExplosionSound("explodeBarrel")
	, mCountdown(60.0f)
	, mCountdownTimer(0.0f)
	, mIsActive(false)
	, mRatio(0.0f)
	, mEnded(false)
	, mFlashLight(nullptr)
	, mClone(nullptr)
	, mBombId(bombCounter)
	, startUpdate(false)
{
	CCLOG("bomb created %d", mBombId);
	bombCounter++;
}

CBomb::~CBomb()
{
	CCLOG("bomb destroyed %d", mBombId);
}

void CBomb::explode() {
	CSoundManager::getInstance()->sfx->playGroup(this->mExplosionSound); 	// SoundManager.sfx.playGroup(this.explosionSound), 
	//if(mBombManager)
	//	mBombManager->onExplode(this);//this.onExplode.dispatch(this)
	if(mOnExplodeCallbackWithBombParam)
		mOnExplodeCallbackWithBombParam(this);
}

void CBomb::update() {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	BlendFunc tBlendFunc = { GL_SRC_ALPHA, GL_ONE };

	//CCLOG("countdownTimer=%f", this->mCountdownTimer);
	if (!this->mEnded) {
		auto that = this;
		this->mCountdownTimer -= CTicker::getInstance()->getDeltaTime();
		if (this->mRatio > 2)
			this->mRatio = 2;
		else
			this->mRatio += 0.01f;
		float pivot_x = 2 * sinf(this->mCountdownTimer) * this->mRatio;

		setAnchorPoint(Vec2(0.5 + pivot_x / 15, 0.5f)); //50
		//setAnchorPoint(Vec2(pivot_x, 0.5f)); //50

		// serega CCLOG("mCountdownTimer=%f ratio=%f %f delta=%f", this->mCountdownTimer, this->mRatio, pivot_x, CTicker::getInstance()->getDeltaTime());

		if (this->mCountdownTimer <= 0) {
			this->mEnded = true;
			mClone = Sprite::createWithSpriteFrameName("Barrel.png");
			mClone->setAnchorPoint(Vec2(0, 0));
			mClone->setBlendFunc(tBlendFunc);
			mClone->setPosition(.5 * -mClone->getContentSize().width, .5 * -mClone->getContentSize().height);
			mClone->setOpacity(0);
			mClone->setPositionX(0); //my
			mClone->setPositionY(0); //my
			addChild(mClone, 1);

			mFlashLight = Sprite::createWithSpriteFrameName("Light.png");
			mFlashLight->setOpacity(0);
			mFlashLight->setScale(.2);
			mFlashLight->setAnchorPoint(Vec2(0.5, 0.5));
			mFlashLight->setBlendFunc(tBlendFunc);
			mFlashLight->setPositionX(this->getContentSize().width / 2);
			mFlashLight->setPositionY(this->getContentSize().height / 2);
			this->addChild(mFlashLight, 2);

			//mFlashLight->runAction(FadeTo::create(0.5f, 255));
			//mFlashLight->runAction(ScaleTo::create(0.5f, 2));

			auto mySpawn = Spawn::createWithTwoActions(FadeTo::create(.2f, 255), ScaleTo::create(.5f, 2));
			mFlashLight->runAction(mySpawn);

			mClone->runAction(Sequence::create(	FadeTo::create(.2f, 204),
												CallFunc::create([this]() {
													mFlashLight->removeFromParent();
													mClone->removeFromParent();
													this->explode();
													this->mCountdownTimer = this->mCountdown;
												}),
												nullptr));
			this->mEnded = true;
		}
	}
}

void CBomb::reset() {
	this->mCountdownTimer = this->mCountdown;
	this->setScale(0);
	this->mIsActive = true;
	this->mEnded = false;
	this->mRatio = 0;
	this->mExplosionSize = BOMB_BLAST_RADIUS;
}

void CBomb::show() {
	runAction(Sequence::create(EaseElasticOut::create(ScaleTo::create(0.3f, 1.0f), 0.3f), nullptr));
	/* old
	auto mySpawn = Spawn::createWithTwoActions(	EaseElasticOut::create(ScaleTo::create(0.5f, 1.0f), 0.3f),
												CShakeBomb::create(0.5f, -360));
	runAction(Sequence::create(	mySpawn,
								CallFunc::create([this]() {
									this->mEnded = true;
									BlendFunc tBlendFunc = { GL_SRC_ALPHA, GL_ONE };
									mClone = Sprite::create("game/Barrel.png");
									mClone->setAnchorPoint(Vec2(0, 0));
									mClone->setBlendFunc(tBlendFunc);
									mClone->setPosition(.5 * -mClone->getContentSize().width, .5 * -mClone->getContentSize().height);
									mClone->setOpacity(0);
									mClone->setPositionX(0); //my
									mClone->setPositionY(0); //my
									addChild(mClone, 1);

									mFlashLight = Sprite::create("game/Light.png");
									mFlashLight->setOpacity(0);
									mFlashLight->setScale(0.2);
									mFlashLight->setAnchorPoint(Vec2(0.5, 0.5));
									mFlashLight->setBlendFunc(tBlendFunc);
									mFlashLight->setPositionX(this->getContentSize().width / 2);
									mFlashLight->setPositionY(this->getContentSize().height / 2);
									this->addChild(mFlashLight, 2);

									auto mySpawn = Spawn::createWithTwoActions(FadeTo::create(0.5f, 255), ScaleTo::create(0.5f, 2));
									mFlashLight->runAction(mySpawn);

									mClone->runAction(Sequence::create(	FadeTo::create(0.5f, 204),
																		CallFunc::create([this]() {
																			mFlashLight->removeFromParent();
																			mClone->removeFromParent();
																			this->explode();
																			this->mCountdownTimer = this->mCountdown;
																		}),
																		nullptr));
								}),
								nullptr));
	*/
}

void CBomb::onReact() {

}

CBomb* CBomb::create(CBombManager* bombManager) {
	CBomb* pRet = new CBomb();
	if (pRet && pRet->init(bombManager)){
		pRet->retain();
		return pRet;
	}
	else{ 
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CBomb::init(CBombManager* bombManager) {
	if (!cocos2d::Sprite::initWithSpriteFrameName("Barrel.png")) {
		return false;
	}

	mBombManager = bombManager;
	this->mCountdown = 60;
	mId = 4;
	this->mIdent=std::string("Barrel");

	/*
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setAnchorPoint(Vec2(.5, .5));
	// ...

	id = 2;
	isDestroyable = false;
	aStarBlocker = true;
	*/

	return true;
}

void CBomb::onExplode_addOnce(TOnExplodeCallbackWithBombParam onExplodeCallbackWithBombParam) {
	mOnExplodeCallbackWithBombParam = onExplodeCallbackWithBombParam;
}

//void CBomb::remove() {
//	this->removeFromParent();
//}
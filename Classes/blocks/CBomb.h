#ifndef __BOMB_H__
#define __BOMB_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

using namespace cocos2d;

class CBomb;
typedef std::function<void(CBomb*)> TOnExplodeCallbackWithBombParam;
class CBombManager;
class CBomb
	: public CPiriPiriGameObject
{
	public:
		virtual ~CBomb();
		static CBomb* create(CBombManager* bombManager);
		virtual bool init(CBombManager* bombManager);

		void explode();
		virtual void update();
		void reset();
		void show();
		void onReact();
		void onExplode_addOnce(TOnExplodeCallbackWithBombParam onExplodeCallbackWithBombParam);
		//void remove();

	//private:
		CBomb();

		std::string mExplosionSound;
		//	this.onExplode = new Signal,
		float mCountdown;
		float mCountdownTimer;
		int mExplosionSize;
		bool mIsActive;
		float mRatio;
		bool mEnded;
		int mBombId; // my

		bool startUpdate;
		CBombManager* mBombManager;

	private:
		Sprite* mFlashLight;
		Sprite* mClone;
		TOnExplodeCallbackWithBombParam mOnExplodeCallbackWithBombParam;
};

#endif // __BOMB_H__

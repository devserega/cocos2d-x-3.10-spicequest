#ifndef __EMPTY_BLOCK_H__
#define __EMPTY_BLOCK_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"

class CEmptyBlock
	: public CPiriPiriGameObject
{
	public:
		static CEmptyBlock* create();
		virtual bool init();
		virtual ~CEmptyBlock();

	protected:
		CEmptyBlock();
};

#endif // __EMPTY_BLOCK_H__
#include "CFootstepLeft.h"
#include "gridworld/CGridCell.h"

CFootstepLeft* CFootstepLeft::create(CGridCell* stepInfo) {
	CFootstepLeft* pRet = new(std::nothrow) CFootstepLeft();
	if (pRet && pRet->init(stepInfo)) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CFootstepLeft::init(CGridCell* stepInfo) {
	if (!CStep::init("FootPrintLeft.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	float x = stepInfo->centerX + OFFSETS::x;
	float y = stepInfo->centerY - OFFSETS::y;
	setPosition(x, y);

	return true;
}

CFootstepLeft::~CFootstepLeft() {

}


CFootstepLeft::CFootstepLeft() 
	:  CStep(){

}
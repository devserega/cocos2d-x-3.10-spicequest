#include "CFootstepRight.h"
#include "gridworld/CGridCell.h"

CFootstepRight* CFootstepRight::create(CGridCell* stepInfo) {
	CFootstepRight* pRet = new(std::nothrow) CFootstepRight();
	if (pRet && pRet->init(stepInfo)) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CFootstepRight::init(CGridCell* stepInfo) {
	if (!CStep::init("FootPrintRight.png")) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	float x = stepInfo->centerX + OFFSETS::x;
	float y = stepInfo->centerY - OFFSETS::y;
	setPosition(x,y);

	return true;
}

CFootstepRight::~CFootstepRight() {

}


CFootstepRight::CFootstepRight()
	: CStep() {

}
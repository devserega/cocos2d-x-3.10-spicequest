#include "CStep.h"

bool CStep::init(const std::string& fileName) {
	if (!cocos2d::Sprite::initWithSpriteFrameName(fileName)) {
		this->setCascadeOpacityEnabled(true);
		return false;
	}

	setAnchorPoint(Vec2(.5, .5));

	return true;
}

CStep::~CStep() {
}

void CStep::show(float delay) {
	/* original
	       TweenLite.to(this.view, .2, {
                alpha: 1,
                delay: delay
            }), 
			TweenLite.to(this.view.scale, .2, {
                x: 1,
                y: 1,
                delay: delay,
                ease: Elastic.easeOut
            })
			
			*/

	//CCLOG("delay=%f", delay);
	auto mySpawn = Spawn::createWithTwoActions(	FadeTo::create(0.5f, 255), 
												EaseElasticOut::create(ScaleTo::create(0.5f, 1), 0.3f));
	runAction(Sequence::create(	DelayTime::create(delay),
								mySpawn,
								nullptr));
}

void CStep::reset(float rotation) {
	this->setScale(0);
	this->setOpacity(0); 
	this->setRotation(57.2958*rotation); //	this.view.rotation = rotation; 
	//this->prevX = 0;
	//this->prevY = 0;
}

CStep::CStep() {

}
#include "CFootsteps.h"
#include "CPiriPiriBlastGame.h"
#include "CStep.h"
#include "gridworld/CGridCell.h"
#include "CFootstepLeft.h"
#include "CFootstepRight.h"

CFootsteps::CFootsteps(){
}

CFootsteps::~CFootsteps() {
}

bool CFootsteps::init() {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	this->setCascadeOpacityEnabled(true);
	mTarget = nullptr;
	mFunctionSelector = nullptr;
	prevX = 0;
	prevY = 0;

	return true;
}

CFootsteps* CFootsteps::create(CPiriPiriBlastGame* game) {
	CFootsteps* pRet = new(std::nothrow) CFootsteps();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		pRet->mGame = game;
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

void CFootsteps::build(std::vector<CGridCell*> path, CGridCell* startCell, CGridCell* endCell) {
	float delay = 0;
	float rotation = 0;
	for (unsigned int i = 0; i < path.size() - 1; i++) {
		CGridCell* stepInfo = path[i];
		CGridCell* nextStepInfo = path[i + 1];// || !1;
		rotation = 0;

		nextStepInfo &&
		stepInfo->realY > this->prevY && nextStepInfo->realX > stepInfo->realX ? rotation = 0 == i ? 270 * (PI / 180) : 0 * (PI / 180) :
		stepInfo->realX > this->prevX ? rotation = 270 * (PI / 180) :
		stepInfo->realX < this->prevX ? rotation = -270 * (PI / 180) :
		stepInfo->realY > this->prevY ? rotation = 0 * (PI / 180) :
		stepInfo->realY < this->prevY && (rotation = 180 * (PI / 180));
	
		this->prevX = stepInfo->realX;
		this->prevY = stepInfo->realY;
		CFootstepRight* stepRight = CFootstepRight::create(stepInfo);
		stepRight->reset(rotation);
		stepRight->show(delay);
		CFootstepLeft* stepLeft = CFootstepLeft::create(stepInfo);
		stepLeft->reset(rotation);
		stepLeft->show(delay + .03);
		this->addChild(stepLeft);
		this->mSteps.push_back(stepLeft);
		this->addChild(stepRight);
		this->mSteps.push_back(stepRight);
		delay += .1;
	}

	// original this.onStepsComplete.dispatch(path, startCell, endCell, delay)
	if (mTarget != nullptr && mFunctionSelector != nullptr) {
		(mTarget->*mFunctionSelector)(path, startCell, endCell, delay); 
	}
}

void CFootsteps::reset() {
	for (unsigned int i = 0; i < this->mSteps.size(); i++)
		this->mSteps[i]->removeFromParent();
	this->mSteps.clear();
}

void CFootsteps::update() {
}

void CFootsteps::onStepsComplete(SEL_CallFunc_onStepsComplete functionSelector, cocos2d::Ref* target) {
	mTarget = target;
	mFunctionSelector = functionSelector;
}
#ifndef __STEP_H__
#define __STEP_H__

#include "cocos2d.h"
//#include "CPiriPiriGameObject.h"
#include "Constants.h"

using namespace cocos2d;

class CGridCell;
class CStep
	: public Sprite
{
	public:
		virtual bool init(const std::string& fileName);
		virtual ~CStep();

		virtual void show(float delay);
		virtual void reset(float rotation);

	protected:
		CStep();
		float mRotation;
};

#endif // __STEP_H__
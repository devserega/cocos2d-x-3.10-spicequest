#ifndef __FOOTSTEPS_H__
#define __FOOTSTEPS_H__

#include "cocos2d.h"
#include "astar/CAStar.h"
#include "CPiriPiriGameObject.h"
#include "Constants.h"

class CPiriPiriBlastGame;
class CStep;
class CGridCell;
class CFootsteps
	: public cocos2d::Layer 
{
	public:
		virtual bool init();
		static CFootsteps* create(CPiriPiriBlastGame* game);
		virtual ~CFootsteps();
		void build(std::vector<CGridCell*> path, CGridCell* startCell, CGridCell* endCell);
		void onStepsComplete(SEL_CallFunc_onStepsComplete functionSelector, cocos2d::Ref* target);
		void reset();
		void update();

	protected:
		CFootsteps();


		CPiriPiriBlastGame* mGame;
		std::vector<CStep*> mSteps;

		// Pointers we need
		cocos2d::Ref* mTarget;
		SEL_CallFunc_onStepsComplete mFunctionSelector;

		int prevX;
		int prevY;

		/*
		CGridCell* getCell(unsigned int x, unsigned int y);
		void setCell(CPiriPiriGameObject* cellType, unsigned int xId, unsigned int yId);
		void setCellFromPoint(CPiriPiriGameObject* cellType, unsigned int x, unsigned int y);
		void removeObject(CPiriPiriGameObject* piriPiriObject);

		//protected:
		unsigned int cellSize;
		std::vector<CGridCell*> cells;
		unsigned int cellWidth;
		unsigned int cellHeight;
		unsigned int totalCells;
		//this.view = new PIXI.DisplayObjectContainer,
		CAStar* astar;
		*/
};

#endif // __FOOTSTEPS_H__

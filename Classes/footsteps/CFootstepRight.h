#ifndef __FOOTSTEP_RIGHT_H__
#define __FOOTSTEP_RIGHT_H__

#include "cocos2d.h"
#include "CStep.h"

using namespace cocos2d;

class CFootstepRight
	: public CStep
{
	public:
		static CFootstepRight* create(CGridCell* stepInfo);
		virtual bool init(CGridCell* stepInfo);
		virtual ~CFootstepRight();

	protected:
		CFootstepRight();
};

#endif // __FOOTSTEP_RIGHT_H__


#ifndef __FOOTSTEP_LEFT_H__
#define __FOOTSTEP_LEFT_H__

#include "cocos2d.h"
#include "CStep.h"

using namespace cocos2d;

class CFootstepLeft
	: public CStep
{
	public:
		static CFootstepLeft* create(CGridCell* stepInfo);
		virtual bool init(CGridCell* stepInfo);
		virtual ~CFootstepLeft();

	protected:
		CFootstepLeft();
};

#endif // __FOOTSTEP_LEFT_H__

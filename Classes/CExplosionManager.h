#ifndef __EXPLOSIONMANAGER_H__
#define __EXPLOSIONMANAGER_H__

#include "cocos2d.h"

class CPiriPiriBlastGame;
class CBombExplosion;
class CBomb;
class CGridCell;
class CFragment;
class CExplosionManager
	: public cocos2d::Ref
{
	public:
		static CExplosionManager* create(CPiriPiriBlastGame* game);
		virtual bool init();

		void add(CBomb* bomb);
		void reset();
		void update();
		void checkComplete();
		void addFragment(CGridCell* gridCell, std::string bombIdent = "");
		void onExplosionFinish(CBombExplosion* explosion);

	protected:
		CExplosionManager();
		virtual ~CExplosionManager();

		CPiriPiriBlastGame* mGame;
		std::vector<CBombExplosion*> mExplosions;
		std::vector<CFragment*> mFragments;
		unsigned int mBombCount;
};

#endif // __EXPLOSIONMANAGER_H__
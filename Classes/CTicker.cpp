#include "CTicker.h"
#include <time.h>
#include "CPiriPiriBlastGame.h"
#include "CTickerListener.h"

bool CTicker::instanceFlag = false;
CTicker* CTicker::single = nullptr;

CTicker::CTicker() {
}

bool CTicker::init() {
	if (!cocos2d::Node::init()) {
		return false;
	}

	mActive = false;
	mDeltaTime = 1;
	mTimeElapsed = 0;
	mLastTime = 0;
	mSpeed = 1;

	return true;
}

//Ticker.instance.add

void CTicker::start(){
	if (!this->mActive) {
		this->mActive = true;
		//requestAnimationFrame(this.updateBind))
		//this->schedule(schedule_selector(CTicker::update), 0.15f);
		this->schedule(static_cast<cocos2d::SEL_SCHEDULE>(&CTicker::update), 0.0467f); // 0.0167f
	}
}

void CTicker::stop() {
	if (this->mActive) {
		this->mActive = false;
		this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CTicker::update));
	}
}

void CTicker::update(float dt) {
	if (this->mActive) {
		//CCLOG("TIME %lu", getCurrentTime());

		//requestAnimationFrame(this.updateBind);
		long long mCurrentTime = getCurrentTime();
		mTimeElapsed = mCurrentTime - this->mLastTime;
		if (mTimeElapsed > 100) {
			mTimeElapsed = 10;
		}
		this->mDeltaTime = .06 * mTimeElapsed;
		this->mDeltaTime *= this->mSpeed;
		//this.onUpdate.dispatch(this.deltaTime);
		this->mLastTime = mCurrentTime;

		// my ask misha
		/*
		for (std::vector<SEL_SCHEDULE>::iterator it = mListeners.begin(); it != mListeners.end(); it++) {
			SEL_SCHEDULE sel = *it;
			//(*sel)(0.0f);

			//it(0.0f);

			//scheduleOnce(sel, 1.0f);

			//&sel(0.0f);
			int t1 = 212;
			//it(0.0f);
			//SEL_SCHEDULE pCallback = (SEL_SCHEDULE)(&CTicker::update);
			//pCallback(0.0f);

			//it(0.0f);
		}
		*/

		//mListeners2.push_back(std::bind(&CPiriPiriBlastGame::update, pClassAObj2, std::placeholders::_1));

		for (std::vector<CTickerListener*>::iterator it = mListeners.begin(); it != mListeners.end(); it++) {
			(*it)->update(0.0f);
		} 
	}
}

void CTicker::add(CTickerListener* listener){
//void CTicker::add(SEL_SCHEDULE listener/*, scope*/) {
//void CTicker::add(std::function<void(float val)> listener/*, scope*/) {
	//this.onUpdate.add(listener, scope)
	//mListeners.push_back(listener);
	mListeners.push_back(listener);
}

void CTicker::remove(CTickerListener* listener){
//void CTicker::remove(SEL_SCHEDULE listener/*, scope*/) {
//void CTicker::remove(std::function<void(float val)> listener/*, scope*/) {
	//this.onUpdate.remove(listener, scope)

	/*
	for (std::vector<SEL_SCHEDULE>::iterator it = mListeners.begin(); it != mListeners.end(); it++) {
		if (*it == listener) {
			mListeners.erase(it, it + 1);
			break;
		}
	}*/

	/*
	for (std::vector<std::function<void(float val)>>::iterator it = mListeners.begin(); it != mListeners.end(); it++) {
		if (*it == listener) {
			mListeners.erase(it, it + 1);
			break;
		}
	}*/

	for (std::vector<CTickerListener*>::iterator it = mListeners.begin(); it != mListeners.end(); it++) {
		if (*it == listener) {
			mListeners.erase(it, it + 1);
			break;
		}
	}
}

long long CTicker::getCurrentTime(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (long long) tp.tv_sec * 1000L + tp.tv_usec / 1000; //get current timestamp in milliseconds
}

double CTicker::getDeltaTime() {
	return this->mDeltaTime;
}

void CTicker::setSpeed(int speed) {
	mSpeed = speed;
}
#ifndef __SPICE_EXPLODE_H__
#define __SPICE_EXPLODE_H__

#include "cocos2d.h"
#include "CPiriPiriGameObject.h"
#include "Constants.h"
//#include "CBombExplosion.h"

using namespace cocos2d;

class CSpiceExplode;
typedef std::function<void(CSpiceExplode*)> TOnCompleteCallbackWithSEParam;
class CSpiceExplode
	: public cocos2d::Sprite
{
	public:
		virtual ~CSpiceExplode();
		static CSpiceExplode* create();
		virtual bool init();
		void goBoom();
		void update();
		void onComplete_addOnce(TOnCompleteCallbackWithSEParam onCompleteCallbackWithSEParam);

		CGameObject* mDisc;
		double mDepthOffset;

	protected:
		CSpiceExplode();
		TOnCompleteCallbackWithSEParam mOnCompleteCallbackWithSEParam;

		/*
		std::string mExplosionSound;
		std::string mIdent;
		//	this.onExplode = new Signal,
		float mCountdown;
		float mCountdownTimer;
		int mExplosionSize;
		bool mIsActive;
		float mRatio;
		bool mEnded;
		int mBombId; // my

		bool startUpdate;
		*/

		//Sprite* mFlashLight;
		//Sprite* mClone;
		std::vector<CGameObject*> mParticles;
		unsigned int mTotal;
		unsigned int mRatio;
		float mLife;
		Vec2 mStartPoint;
		Vec2 mTargetPoint;

		//this.startPoint = new PIXI.Point,
		//this.total = 3,
		//this.targetPoint = new PIXI.Point,
};

#endif // __SPICE_EXPLODE_H__
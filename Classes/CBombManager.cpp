#include "CBombManager.h"
#include "CPiriPiriBlastGame.h"
#include "blocks/CBomb.h"
#include "blocks/CBarrel.h"
#include "media/CSoundManager.h"

CBombManager* CBombManager::create(CPiriPiriBlastGame* game) {
	CBombManager* pRet = new(std::nothrow) CBombManager();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->mGame = game;
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

bool CBombManager::init() {
	//if (!cocos2d::Ref::init()) {
	//	return false;
	//}

	// ...
	mBombCount = 0;

	return true;
}

CBombManager::CBombManager() {
}

CBombManager::~CBombManager() {
}

void CBombManager::add(unsigned int x, unsigned int y) {
	CSoundManager::getInstance()->sfx->play("placeBarrel");
	CBomb* bomb = CBomb::create(this);
	bomb->reset();
	mGame->mGridWorld->setCellFromPoint(bomb, x, y);
	bomb->onExplode_addOnce((TOnExplodeCallbackWithBombParam) std::bind(&CBombManager::onExplode, this, std::placeholders::_1));
	this->mBombs.push_back(bomb);
	bomb->show();
	mBombCount++;
	mGame->takeBomb();
}

void CBombManager::reset() {
	this->clearBombs();
}

void CBombManager::clearBombs() {
	for (unsigned int i = this->mBombs.size(); i--;) {
		CBomb* bomb = this->mBombs[i];
		//bomb->onExplode->removeAll();
		if (bomb->mId == BLOCKS::BARREL) {
			this->mGame->mGridWorld->removeObject(bomb);
			//bomb->release();
			//bomb->autorelease();
			//this.bombPool.returnObject(bomb);
		}
	}
	this->mBombs.clear();
	this->mBombCount = 0;
}

void CBombManager::addBarrel(CBarrel* barrel, unsigned int x, unsigned int y) {
	this->mGame->mGridWorld->setCell(barrel, x, y);
	//barrel.onExplode.addOnce(this.onExplodeBarrel, this)
	barrel->onExplode_addOnce((TOnExplodeCallbackWithBombParam)std::bind(&CBombManager::onExplodeBarrel, this, std::placeholders::_1));
	this->mBombs.push_back(barrel);
}

void CBombManager::update() {
	//CCLOG("size=%d", this->mBombs.size());
	for (unsigned int i = this->mBombs.size(); i--;) {
		if(!this->mBombs[i]->mEnded)
			this->mBombs[i]->update();
	}
}

bool CBombManager::isActive() {
	for (unsigned int i = this->mBombs.size(); i--;)
		if (this->mBombs[i]->mIsActive == true) return true;
	return false;
}

void CBombManager::onExplode(CBomb* bomb) {
	if (bomb->cell!=nullptr) {
		bomb->runAction(EaseElasticOut::create(ScaleTo::create(0.2f, 2.0f), 0.3f));
		this->mGame->mExplosionManager->add(bomb);
		this->mGame->mGridWorld->removeObject(bomb);

		std::vector<CBomb*>::iterator foundElement = std::find_if(	mBombs.begin(), mBombs.end(), 
																	[bomb](CBomb* vecElement) {return vecElement == bomb;});
		mBombs.erase(foundElement); // original this.bombs.splice(this.bombs.indexOf(bomb), 1);
		//this.bombPool.returnObject(bomb);
		this->mBombCount--;
	}
}

void CBombManager::onExplodeBarrel(CBomb* barrel) {
	this->mGame->mExplosionManager->add(barrel);
	this->mGame->mGridWorld->removeObject(barrel);

	std::vector<CBomb*>::iterator foundElement = std::find_if(	mBombs.begin(), mBombs.end(),
																[barrel](CBomb* vecElement) {return vecElement == barrel; });
	mBombs.erase(foundElement); // this.bombs.splice(this.bombs.indexOf(barrel), 1);
}
#ifndef __FLASH_H__
#define __FLASH_H__

#include "cocos2d.h"

using namespace cocos2d;

class CFlash
	: public cocos2d::Layer
{
	public:
		static CFlash* create(unsigned int f1, unsigned int f2);
		virtual ~CFlash();

	protected:
		CFlash();
		virtual bool init();
};

#endif // __FLASH_H__
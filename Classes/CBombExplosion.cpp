#include "CBombExplosion.h"
#include "CTicker.h"
#include "CExplosionManager.h"
#include "actions/CShakeBomb.h"
#include "blocks/CBomb.h"
#include "gridworld/CGridCell.h"

CBombExplosion::CBombExplosion()
	: CPiriPiriGameObject()
	/*
	, mExplosionSound("explodeBarrel")
	, mIdent("Barrel")
	, mCountdown(60.0f)
	, mCountdownTimer(0.0f)
	, mIsActive(false)
	, mRatio(0.0f)
	, mEnded(false)
	, mFlashLight(nullptr)
	, mClone(nullptr)
	, mBombId(bombCounter)
	, startUpdate(false)
	*/
{
	//CCLOG("bomb created %d", mBombId);
	//bombCounter++;
}

CBombExplosion::~CBombExplosion()
{
	//CCLOG("bomb destroyed %d", mBombId);
}

void CBombExplosion::update() {
	this->mCountdownTimer -= CTicker::getInstance()->getDeltaTime();
	if (this->mCountdownTimer <= 0) {
		this->mCountdownTimer = this->mCountdown; 
		this->mExplosionLevel++;
		if (this->mExplosionLevel <= this->mStrength) {
			for (unsigned int nToProcess = mToProcess.size(); nToProcess--;) {
				std::string sDirection = mToProcess[nToProcess];
				CGridCell* source = mSDirection[sDirection];
				if (mSDirection[sDirection] != nullptr) {
					CPiriPiriGameObject* item = source->item;
					bool hasExploded = false;
					if (item && item->isDead == false) {
						if (item->isDestroyable && item->showBlast == true) {
							this->mExplosionManager->addFragment(source, this->mIdent);
							item->onDestroy(); // SEREGA1
							hasExploded = true;
						}

						if (item->stopsExplosion) {
							mSDirection[sDirection] = nullptr;
							if (hasExploded == false) item->onReact(); // SEREGA1
						}
						else {
							if (hasExploded == false && item->showBlast == true) {
								this->mExplosionManager->addFragment(source, this->mIdent);
								item->onReact();
							}
							mSDirection[sDirection] = mSDirection[sDirection]->mSDirection[sDirection];
							// original this[sDirection] = this[sDirection][sDirection];
						}
					}
					else {
						this->mExplosionManager->addFragment(source, this->mIdent);
						mSDirection[sDirection] = mSDirection[sDirection]->mSDirection[sDirection];
						// original this[sDirection] = this[sDirection][sDirection];
					}
				}
			}
		}
		else {
			mExplosionManager->onExplosionFinish(this); //	original this.onComplete.dispatch(this);
		}
	}

	if (mSDirection["up"] == nullptr && 
		mSDirection["down"] == nullptr && 
		mSDirection["left"] == nullptr && 
		mSDirection["right"] == nullptr) {
		mExplosionManager->onExplosionFinish(this); // original this.onComplete.dispatch(this);
	}
}

void CBombExplosion::complete() {

}

void CBombExplosion::reset(CPiriPiriGridWorld* gridWorld, CBomb* bomb, CExplosionManager* explosionManager) {
	this->mGridWorld = gridWorld;
	this->mStartCell = bomb->cell;
	this->mStrength = bomb->mExplosionSize;
	this->mExplosionLevel = 0;
	//this->mBomb = bomb;
	this->mExplosionManager = explosionManager;
	this->mIdent = bomb->mIdent;
}

void CBombExplosion::start() {
	CGridCell* cell = this->mStartCell;
	this->mSDirection["up"] = cell->mSDirection["up"];
	this->mSDirection["down"] = cell->mSDirection["down"];// cell->down
	this->mSDirection["left"] = cell->mSDirection["left"];
	this->mSDirection["right"] = cell->mSDirection["right"];
	this->mExplosionManager->addFragment(cell);
}

CBombExplosion* CBombExplosion::create(CExplosionManager* explosionManager) {
	CBombExplosion* pRet = new CBombExplosion();
	if (pRet && pRet->init(explosionManager)){
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CBombExplosion::init(CExplosionManager* explosionManager) {
	//if (!cocos2d::Sprite::initWithFile("game/Barrel.png")) {
	//	return false;
	//}

	
	this->mStrength = 0;
	this->mExplosionLevel = 0;
	//this.gridWorld,
	this->mExplosionManager = explosionManager;
	//this.bomb = !1,
	this->mCountdown = 2;
	this->mCountdownTimer = this->mCountdown;
	this->mToProcess = { "up", "down", "left", "right"};
	this->mSDirection["left"] = nullptr;
	this->mSDirection["right"] = nullptr;
	this->mSDirection["up"] = nullptr;
	this->mSDirection["down"] = nullptr;
	//this.onComplete = new Signal
	this->mIdent = "";

	return true;
}
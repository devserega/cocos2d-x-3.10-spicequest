#ifndef __LEVELS_H__
#define __LEVELS_H__

#include "cocos2d.h"

using namespace cocos2d;

typedef std::vector<std::string> TMap;

class CLevel 
	: public cocos2d::Ref {
	public:
		CLevel();
		virtual ~CLevel();
		int id;
		std::string name;
		unsigned int bombs;
		TMap map;
		std::string sprite;
};

class CLevels 
	: public cocos2d::Ref
{
	public:
		static CLevels* getInstance() {
			if (!instanceFlag) {
				single = new CLevels();
				if (single && single->init()) {
					instanceFlag = true;
					return single;
				}
				CC_SAFE_DELETE(single);
				return nullptr;
			}
			else {
				return single;
			}
		}

		virtual ~CLevels() {
			CCLOG("destroy CLevels singleton");
			mLevels.clear();
			instanceFlag = false;
			single = NULL;
		}

		CLevel* get(unsigned int index);
		unsigned long length();

	private:
		CLevels();
		virtual bool init();

		static bool instanceFlag;
		static CLevels* single;

		std::vector<CLevel*> mLevels;
};

#endif // __LEVELS_H__
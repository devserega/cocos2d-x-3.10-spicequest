#include "CPiriPiriBlastGame.h"
#include "CTicker.h"
#include "blocks/CUpDownBlock.h"
//#include "footsteps/CFootsteps.h"
//#include "CPointAndClickController.h"
#include "screens/CGameScreen.h"
#include "CPiriPiriBlastApp.h"
#include <functional>

CPiriPiriBlastGame* CPiriPiriBlastGame::create() {
	CPiriPiriBlastGame *pRet = new(std::nothrow) CPiriPiriBlastGame();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

CPiriPiriBlastGame::CPiriPiriBlastGame() {
	//this->schedule(schedule_selector(CPiriPiriBlastGame::update), 1.0f);
}

CPiriPiriBlastGame::~CPiriPiriBlastGame() {
	//this->unschedule(static_cast<cocos2d::SEL_SCHEDULE>(&CPiriPiriBlastGame::update));
}

bool CPiriPiriBlastGame::init() {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setCascadeOpacityEnabled(true);

	mfloor = Sprite::create("/game/WireframeGrid.jpg");
	mfloor->setAnchorPoint(Vec2(0,0));
	//bg->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	mfloor->setPosition(Vec2(0+FLOOROFFSET_x, visibleSize.height- mfloor->getContentSize().height-FLOOROFFSET_y));

	mExplosionLayer = Layer::create();
	mExplosionLayer->setCascadeOpacityEnabled(true);
	mGridWorld = CPiriPiriGridWorld::create(this);
	mBombManager = CBombManager::create(this);
	mHud = CHud::create(this);
	mController = CPointAndClickController::create(this);
	mExplosionManager = CExplosionManager::create(this);
	mEffectsManager = CEffectsManager::create(this);
	mFootsteps = CFootsteps::create(this);

	mSunBurst1 = Sprite::create("game/SunburstFG.png");
	mSunBurst1->setPosition(Vec2(50, visibleSize.height+180));
	mSunBurst1->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	mSunBurst1->setScale(5);
	mSunBurst1->setOpacity(76);
	mSunBurst1->runAction(RepeatForever::create(RotateBy::create(120.0f, 360)));

	mSunBurst2 = Sprite::create("game/SunburstFG.png");
	mSunBurst2->setPosition(Vec2(50, visibleSize.height + 180));
	mSunBurst2->setBlendFunc({ GL_SRC_ALPHA, GL_ONE });
	mSunBurst2->setScale(5);
	mSunBurst2->setOpacity(76);
	mSunBurst2->runAction(RepeatForever::create(RotateBy::create(60.0f, 360)));

	this->isGameover = false;
	this->paused = true;
	this->bombs = 10;
	this->score = 0;
	this->gems = 0;
	this->gemsToGet = 0;
	this->levelState = 0;
	mGridWorld->build();
	this->scoreMultiplyer = 1;
	mFootsteps->onStepsComplete(callfunc_onStepsComplete_selector(CPiriPiriBlastGame::onStepsComplete), this);

	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan = CC_CALLBACK_2(CPiriPiriBlastGame::onTouchesBegan, this);
	listener->onTouchesMoved = CC_CALLBACK_2(CPiriPiriBlastGame::onTouchesMoved, this);
	listener->onTouchesEnded = CC_CALLBACK_2(CPiriPiriBlastGame::onTouchesEnded, this);
	getEventDispatcher()->addEventListenerWithFixedPriority(listener, 1);
	// ...

	addChild(mfloor, 0);
	addChild(mGridWorld, 1);
	addChild(mEffectsManager, 2);
	addChild(mFootsteps, 3);
	addChild(mExplosionLayer, 4);
	addChild(mSunBurst1, 5);
	addChild(mSunBurst2, 6);
	addChild(mHud, 7);

	return true;
}

void CPiriPiriBlastGame::reset() { // config
	CGameScreen* gameScreen = (CGameScreen*) this->getParent();
	this->mLevel = CLevels::getInstance()->get(gameScreen->mApp->mSession.level); //temp Levels[app.session.level],
	this->mController->enable();
	this->mBombManager->reset();
	this->mExplosionManager->reset();
	this->mEffectsManager->reset();
	this->mFootsteps->reset();
	this->mHud->reset();
	this->gems = 0;
	this->gemsToGet = 0;
	this->mGridWorld->loadMap(this->mLevel->map);
	this->bombs = this->mLevel->bombs;
	this->mHud->updateBombs();
	this->mHud->updateGems();
	this->isGameover = false;
	this->levelState = 0;
	/*
	config = config || this.config,
	this.config = config,*/
	this->baseSpeed = 1;
	this->gameTime = 0;
	CTicker::getInstance()->setSpeed(this->baseSpeed);
	this->score = 0;
	this->mHud->showStartAndFinish();
}

void CPiriPiriBlastGame::addToScore(/*points, target*/) {
	//var newScore = points * this.scoreMultiplyer | 0;
	//this.score += newScore, this.hud.updateScore(newScore, target)
}

void CPiriPiriBlastGame::start() {//config
	this->reset();
	this->resume();
}

void CPiriPiriBlastGame::pause() {
	if (!this->paused) {
		this->paused = true;
		//this.view.interactive = !1;
		CTicker::getInstance()->remove(this);
	}
}

void CPiriPiriBlastGame::resume() {
	if (this->paused) {
		this->paused = false;
		//this.view.interactive = !0;
		CTicker::getInstance()->add(this);
	}
}

void CPiriPiriBlastGame::update(float dt) {
	this->gameTime += CTicker::getInstance()->getDeltaTime();
	this->mBombManager->update();
	this->mExplosionManager->update();
	this->mEffectsManager->update();
	this->mGridWorld->update();
	//this->world.view.children.sort(depthCompare);
	//this->world.update();
	this->mHud->update();
}

void CPiriPiriBlastGame::takeBomb() {
	this->bombs--;
	this->mHud->updateBombs();
}

void CPiriPiriBlastGame::addGem() {
	this->gems++;
	this->mHud->updateGems();
}

void CPiriPiriBlastGame::onStepsComplete(std::vector<CGridCell*> result, CGridCell* startCell, CGridCell* endCell, float delay) {
	for (unsigned int j = 0; j < this->mGridWorld->cells.size(); j++) 
		if(this->mGridWorld->cells[j]!=nullptr)
			this->mGridWorld->cells[j]->aStarBlocker = true;
	for (unsigned int i = 0; i < result.size(); i++) {
		if (result[i]->hasGem == true)
			this->mHud->clearGem(result[i]->centerX, result[i]->centerY);
	}
	this->levelState = this->gems == this->gemsToGet ? LEVEL_STATES::COMPLETE_WITH_GEMS : LEVEL_STATES::COMPLETE;
  
	CPiriPiriBlastGame* that = this;
	/*
            TweenLite.delayedCall(delay, function() {
                SoundManager.sfx.play("levelClear"), SoundManager.sfx.stop("bootsWalk")
            }), 
			TweenLite.to({
                foo: 0
            }, 
			delay + .5, {
                foo: 100,
                onComplete: function() {
                    that.levelComplete()
                }
            })
	*/
	runAction(Sequence::create(	DelayTime::create(delay),
								CallFunc::create([this]() {
									auto sm = CSoundManager::getInstance();
									sm->sfx->play("levelClear");
									sm->sfx->stop("bootsWalk");
								}),
								nullptr));

	runAction(Sequence::create(	DelayTime::create(delay + 0.5f),
								CallFunc::create([this]() {
									levelComplete();
								}),
								nullptr));
}

void CPiriPiriBlastGame::gameBoardCleared() {
	runAction(Sequence::create(	DelayTime::create(0.2f),
								CallFunc::create([this]() {
									this->runGameBoardClear();
								}),
								nullptr));
}

void CPiriPiriBlastGame::runGameBoardClear() {
	auto startCell = this->mGridWorld->getStartCell();
	auto endCell = this->mGridWorld->getEndCell();
	std::vector<CGridCell*> result = this->mGridWorld->astar->search(this->mGridWorld, startCell, endCell);

	if (result.size() > 0) {
		for (unsigned int j = 0; j < this->mGridWorld->cells.size(); j++)
			if (this->mGridWorld->cells[j]!=nullptr) { // my_test
				if (this->mGridWorld->cells[j]->item && this->mGridWorld->cells[j]->item->mId == BLOCKS::UP_DOWN) {
					CUpDownBlock* upDownBlock = dynamic_cast<CUpDownBlock*>(this->mGridWorld->cells[j]->item);
					if (upDownBlock) upDownBlock->setDown();
				}
			}
		this->mController->disable();
		this->mFootsteps->build(result, startCell, endCell);
		CSoundManager::getInstance()->sfx->play("bootsWalk");
	}
	else if(0 == this->bombs && this->mBombManager->isActive() == false) {
		this->levelState = LEVEL_STATES::UNLOCKED;
		this->gameover();
	}
}

void CPiriPiriBlastGame::levelComplete() {
	this->isGameover = true; 
	this->pause();
	this->mHud->showLevelComplete();
}

void CPiriPiriBlastGame::onLevelComplete() {
	CGameScreen* gameScreen = dynamic_cast<CGameScreen*>(this->getParent());
	if (gameScreen)
		gameScreen->onGameover();
	// original this.onGameover.dispatch()
}

void CPiriPiriBlastGame::gameover() {
	if (!this->isGameover) {
		this->isGameover = true;
		this->pause();
		this->mHud->showGameover();
	}
	// original this.isGameover || (this.isGameover = !0, this.pause(), this.hud.showGameover())
}

void CPiriPiriBlastGame::onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto s = Director::getInstance()->getWinSize();
	auto touchLocation = touches[0]->getLocation();
	CCLOG("touchLocation.x=%f touchLocation.y=%f", touchLocation.x, s.height - touchLocation.y);
}

void CPiriPiriBlastGame::onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto touchLocation = touches[0]->getLocation();
}

void CPiriPiriBlastGame::onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) {
	auto touchLocation = touches[0]->getLocation();
}
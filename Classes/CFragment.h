#ifndef __FRAGMENT_H__
#define __FRAGMENT_H__

#include "cocos2d.h"

using namespace cocos2d;

class CFragment
	: public Sprite
{
	public:
		virtual ~CFragment();
		static CFragment* create();
		bool init();
		void reset(std::string& ident);
		void update();

		bool mIsDead;
		std::string  mIdent;

	private:
		CFragment();
		float mRotationRatio;
		float mRotation;
		float mAlpha;
		float mScaleX;
		float mScaleY;
};

#endif // __FRAGMENT_H__

#include "CCloseButton.h"

bool CCloseButton::init() {
	if (!cocos2d::Node::init()) {
		return false;
	}

	this->setCascadeOpacityEnabled(true);
	this->mBg = Sprite::createWithSpriteFrameName("CloseUp.png");
	this->mBg->setAnchorPoint(Vec2(.5,.5));
	addChild(this->mBg, 1);
	addEventListener();

	return true;
}

CCloseButton* CCloseButton::create() {
	CCloseButton* pRet = new(std::nothrow) CCloseButton();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

CCloseButton::CCloseButton() {
}

CCloseButton::~CCloseButton() {
	removeEventListener();
}

void CCloseButton::addEventListener() {
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	mListener = cocos2d::EventListenerTouchOneByOne::create();
	mListener->setSwallowTouches(true);

	mListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
		auto target = static_cast<Sprite*>(e->getCurrentTarget());
		cocos2d::Vec2 p = target->convertToNodeSpace(touch->getLocation());
		cocos2d:Rect rect = Rect(0, 0, this->mBg->getContentSize().width, this->mBg->getContentSize().height);

		if (myContainsPoint(rect, p)) {
			this->mBg->setSpriteFrame("CloseClick.png");
			return true; // to indicate that we have consumed it.
		}

		return false; // we did not consume this event, pass throw.
	};

	mListener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* e) {
		this->mBg->setSpriteFrame("CloseUp.png");
		if (mOnPressCloseCallback != nullptr)
			mOnPressCloseCallback();
	};
	dispatcher->addEventListenerWithSceneGraphPriority(mListener, this->mBg);
}

void CCloseButton::removeEventListener() {
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	dispatcher->removeEventListener(mListener);
}

void CCloseButton::onPress(TOnPressCallback onPressCloseCallback) {
	mOnPressCloseCallback = onPressCloseCallback;
}

bool CCloseButton::myContainsPoint(const cocos2d::Rect rect, const cocos2d::Vec2& point) {
	bool bRet = false;

	if (point.x >= rect.origin.x && point.x <= rect.size.width &&
		point.y >= rect.origin.y && point.y <= rect.size.height) {
		bRet = true;
	}

	return bRet;
}
#include "CTopMenu.h"
#include "CPiriPiriBlastApp.h"
#include "media/CSoundManager.h"

CTopMenu::CTopMenu()
	: mApp(nullptr)
	, mMuteButton(nullptr)
	, mPauseButton(nullptr)
	, mHelpButton(nullptr)
	, mIsMobile(true){

}

CTopMenu::~CTopMenu() {
}

CTopMenu* CTopMenu::create(CPiriPiriBlastApp* app) {
	CTopMenu* pRet = new CTopMenu();
	if (pRet && pRet->init(app)) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool CTopMenu::init(CPiriPiriBlastApp* app) {
	if (!cocos2d::Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->mApp = app;

	this->mMuteButton = CPiriPiriButton::create("SoundOn.png");
	this->addChild(this->mMuteButton, 1);
	this->mMuteButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CTopMenu::onButtonPressed, this, std::placeholders::_1));
	this->mMuteButton->setPositionX(1086);
	this->mMuteButton->setPositionY(visibleSize.height - 56);

	this->mPauseButton = CPiriPiriButton::create("Pause.png"); 
	this->addChild(this->mPauseButton, 1);
	this->mPauseButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CTopMenu::onButtonPressed, this, std::placeholders::_1));
	this->mPauseButton->setPositionX(996);
	this->mPauseButton->setPositionY(visibleSize.height - 56);

	this->mHelpButton = CPiriPiriButton::create("Help.png"); 
	this->addChild(this->mHelpButton, 1);
	this->mHelpButton->onPress((TOnPressCallbackWithButtonParam) std::bind(&CTopMenu::onButtonPressed, this, std::placeholders::_1));
	this->mHelpButton->setPositionX(996);
	this->mHelpButton->setPositionY(visibleSize.height - 56);
	this->mPauseButton->setVisible(false);
	CSoundManager::getInstance()->sfx->onMuteToggle((TOnCallback) std::bind(&CTopMenu::onMuteToggle, this));
	CSoundManager::getInstance()->music->onMuteToggle((TOnCallback)std::bind(&CTopMenu::onMuteToggle, this));
	this->mIsMobile = mApp->mIsMobile;

	return true;
}

void CTopMenu::gameMode() {
	this->mPauseButton->setVisible(true);
	this->mMuteButton->setVisible(true);
	this->mHelpButton->setVisible(false);
}

void CTopMenu::normalMode() {
	this->mIsMobile ? (this->mPauseButton->setVisible(false), this->mMuteButton->setVisible(true), this->mHelpButton->setVisible(true)) : (this->mPauseButton->setVisible(false), this->mHelpButton->setVisible(true));
}

void CTopMenu::onButtonPressed(CPiriPiriButton* bt) {
	if (bt == this->mMuteButton ) {
		if (CSoundManager::getInstance()->sfx->mIsMuted && CSoundManager::getInstance()->music->mIsMuted) {
			CSoundManager::getInstance()->sfx->unmute();
			CSoundManager::getInstance()->music->unmute();
		}
		else {
			CSoundManager::getInstance()->sfx->mute();
			CSoundManager::getInstance()->music->mute();
		}
	}
	else if (bt == this->mHelpButton) {
		this->mApp->mOverlayManager->show(std::string("help"));
	}
	else if (bt == this->mPauseButton) {
		this->mApp->mOverlayManager->show(std::string("pause"));
	}
}

void CTopMenu::onMuteToggle() {
	auto sm = CSoundManager::getInstance();
	this->mMuteButton->setIcon((sm->sfx->mIsMuted && sm->music->mIsMuted) ? "SoundOff.png" : "SoundOn.png");
}

void CTopMenu::onShown() {
	//this->game.start()
}

void CTopMenu::resize() {
}
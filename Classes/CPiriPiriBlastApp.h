#ifndef __CPIRIPIRIBLASTAPP_H__
#define __CPIRIPIRIBLASTAPP_H__

#include "cocos2d.h"
#include "screens/CLoaderScreen.h"
#include "screens/CTitleScreen.h"
#include "screens/CGameScreen.h"
#include "screens/CLevelScreen.h"
#include "screens/COverlayManager.h"
#include "CTopMenu.h"
#include "CTracking.h"
#include "media/CSoundManager.h"

#define ASSET_URL "assets/"

using namespace cocos2d;
using namespace std;

struct FilmClubSession {
	unsigned int character;
	unsigned int level;
	FilmClubSession() : character(0), level(0){}
};

class CPiriPiriBlastApp 
	: public cocos2d::Scene
{
	public:
		static CPiriPiriBlastApp* createScene();
		CPiriPiriBlastApp();
		virtual ~CPiriPiriBlastApp();
		void onAssetsLoaded();
		void initSound();
		void resize(unsigned int w, unsigned int h);

		FilmClubSession mSession;

	//private:
		static CPiriPiriBlastApp* mThis;
		CLoaderScreen* mLoaderScreen;
		CTitleScreen* mTitleScreen;
		CLevelScreen* mLevelScreen;
		CGameScreen* mGameScreen;
		CScreenManager* mScreenManager;
		Sprite* mBg;
		Sprite* mLogo;
		CTopMenu* mTopMenu;
		COverlayManager* mOverlayManager;
		bool mIsMobile;
};

#endif // __CPIRIPIRIBLASTAPP_H__

#include "CFlash.h"

CFlash* CFlash::create(unsigned int f1, unsigned int f2) {
	CFlash* pRet = new CFlash();
	if (pRet && pRet->init()) {
		//pRet->autorelease();
		pRet->retain();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

CFlash::~CFlash() {

}

CFlash::CFlash() {

}

bool CFlash::init() {

	return true;
}
#include "CPlayButton.h"

bool CPlayButton::init() {
	if (!CPiriPiriButton::init("Play.png",false)) { //interfaceSprites/
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	setCascadeOpacityEnabled(true);

	this->mTScale = 1.2;
	this->mRScale = 1;
	//this->mOffset = 10;
	//this.down = !1,
	this->mIcon->setScale(1, 1);

	//mPlaySprite = Sprite::create("game/Play.png");
	//mPlaySprite->setPosition(getContentSize().width / 2, getContentSize().height / 2);
	//addChild(mPlaySprite, 2);

	return true;
}

CPlayButton* CPlayButton::create() {
	CPlayButton* pRet = new(std::nothrow) CPlayButton();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

void CPlayButton::onTouchBegan() {
	//setTexture("game/LargeSquareButtonClick.png");
	//Size contentSize = getContentSize();
	//mPlaySprite->setPosition(contentSize.width / 2, contentSize.height / 1.7);
}

void CPlayButton::onTouchEnded() {
	//setTexture("game/LargeSquareButtonUp.png");
	//Size contentSize = getContentSize();
	//mPlaySprite->setPosition(contentSize.width / 2, contentSize.height / 2.0);
}

void CPlayButton::onMouseOver() {
	//runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1, 1), 0.3f));
	/*
	TweenLite.to(this.view, .5, {
	ratio: 1,
		   ease : Power0.easeNone //Elastic.easeOut
	})*/
}

void CPlayButton::onMouseOut() {
	//runAction(EaseElasticOut::create(ScaleTo::create(0.5, 0, 0), 0.3f));
	/*
	int t5 = 5545;
	TweenLite.to(this.view, .5, {
	ratio: 0,
		   ease : Elastic.easeOut
	})*/
}
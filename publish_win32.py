#!/usr/bin/python
# coding=utf-8
"""****************************************************************************
Copyright (c) 2016 serega
****************************************************************************"""

import os
import glob
import shutil
import sys
import errno
import zipfile

dstDir = 'publish/win32'
srcDir = 'proj.win32/Debug.win32'

def copyFiles(srcDir_, mask_): 
	for file in glob.glob(srcDir_+mask_):
		print file                                                                                                                                        
		shutil.copy(file, dstDir)
		
def copyFolder(srcPath, dstPath, ignorePatterns=None):
	# Copies the files and folders from a specfied folder to the renamed folder
	try:
		# This copies directories only
		shutil.copytree(srcPath, dstPath, ignore=ignorePatterns)
	except OSError as e:
		# Here we check if the error was a result of the source not being a directory
		if e.errno == errno.ENOTDIR:
			shutil.copy(srcPath, dstPath)
		else:
			print "Copy unsuccessfull!!"
			print str(e)

if not os.path.exists(dstDir):
	os.makedirs(dstDir)
	
copyFiles(srcDir, r'/*.dll')
copyFiles(srcDir, r'/*.exe')
copyFolder('proj.win32/Debug.win32/snd', dstDir+'/snd', shutil.ignore_patterns('*.mp3', 'specificfile.file'))
copyFolder('proj.win32/Debug.win32/res', dstDir+'/res')

# create zip archive
#shutil.make_archive('win32', 'zip', dstDir)
	
os.system("pause")  


